#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ActionListViewController.h"
#import "UpdateActionProgressViewController.h"
#import "MeetingDetailsViewController.h"
#import "AddStackHoldersViewController.h"
#import "UpdateStakeHolderViewController.h"
#import "ContactDirectoryViewController.h"
#import "DashBoardPieChartViewController.h"
#import "UpdateMeetingStatusViewController.h"
#import "MeetingCalenderViewController.h"
#import "ConductMeetingViewController.h"
#import "ApproveActionsViewController.h"
#import "UpdatePassVC.h"
#import "SettingsVC.h"
#import "MyMeetingViewController.h"
#import "BarChartViewController.h"
#import "DocumentsRetrievalVC.h"
#import "DocumentUploadVC.h"
#import "MOMApprovalVC.h"
#import "AlertVC.h"
#import "invoiceApprovalVC.h"
#import "InvoiceClosureApprovalVC.h"
#import "InvoiceClosureAlertVC.h"
#import "PaymentRecieptListVC.h"
#import "InvoiceCreatedListVC.h"
#import "InvoiceDashboardVC.h"
#import "InvoiceExternalVC.h"
#import "LoginQueryViewController.h"
#import "AccessQueryViewController.h"
#import "ActivityQueryViewController.h"
#import "AccessGroupTableViewController.h"
#import "MyAccessDataQueryViewController.h"
#import "MyActivityDataQueryViewController.h"

@interface SWUITableViewCell : UITableViewCell
@end

@interface MenuViewController : UITableViewController
@property (nonatomic, strong) ActionListViewController *ActionListViewController;
@property (nonatomic, strong) UpdateActionProgressViewController *UpdateActionProgressViewController;
@property (nonatomic, strong) MeetingDetailsViewController *MeetingDetailsViewController;
@property (nonatomic, strong) AddStackHoldersViewController *AddStackHoldersViewController;
@property (nonatomic, strong) UpdateStakeHolderViewController *UpdateStakeHolderViewController;
@property (nonatomic, strong) ContactDirectoryViewController *ContactDirectoryViewController;
@property (nonatomic, strong) DashBoardPieChartViewController *DashBoardPieChartViewController;
@property (nonatomic, strong) UpdateMeetingStatusViewController *UpdateMeetingStatusViewController;
@property (nonatomic, strong) MeetingCalenderViewController *MeetingCalenderViewController;
@property (nonatomic, strong) ConductMeetingViewController *ConductMeetingViewController;
@property (nonatomic, strong) ApproveActionsViewController *ApproveActionsViewController;
@property (nonatomic, strong) UpdatePassVC *UpdatePassVC;
@property (nonatomic, strong) SettingsVC *SettingsVC;
@property (nonatomic, strong) MyMeetingViewController *MyMeetingViewController;
@property (nonatomic, strong) BarChartViewController *BarChartViewController;
@property (nonatomic, strong) DocumentsRetrievalVC *DocumentsRetrievalVC;
@property (nonatomic, strong) DocumentUploadVC *DocumentUploadVC;
@property (nonatomic, strong) MOMApprovalVC *MOMApprovalVC;
@property (nonatomic, strong) AlertVC *AlertVC;
@property (nonatomic, strong) invoiceApprovalVC *invoiceApprovalVC;
@property (nonatomic, strong) InvoiceClosureApprovalVC *InvoiceClosureApprovalVC;
@property (nonatomic, strong) InvoiceClosureAlertVC *InvoiceClosureAlertVC;
@property (nonatomic, strong) PaymentRecieptListVC *PaymentRecieptListVC;
@property (nonatomic, strong) InvoiceCreatedListVC *InvoiceCreatedListVC;
@property (nonatomic, strong) InvoiceDashboardVC *InvoiceDashboardVC;
@property (nonatomic, strong) InvoiceExternalVC *InvoiceExternalVC;
@property (nonatomic, strong) LoginQueryViewController *loginQueryVC;
@property (nonatomic, strong) AccessQueryViewController *accessQueryVC;
@property (nonatomic, strong) ActivityQueryViewController *activityQueryVC;
@property (nonatomic, strong) AccessGroupTableViewController *accessGroupVC;
@property (nonatomic, strong) MyAccessDataQueryViewController *myAccessQueryVC;
@property (nonatomic, strong) MyActivityDataQueryViewController *myActivityQueryVC;

@end

