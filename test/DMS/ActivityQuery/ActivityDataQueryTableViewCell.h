//
//  ActivityDataQueryTableViewCell.h
//  test
//
//  Created by ceazeles on 24/12/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ActivityDataQueryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *activityHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *pathNameHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *folderHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *fileHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *dateTimeHeaderText;

@property (weak, nonatomic) IBOutlet UITextField *activityDataText;
@property (weak, nonatomic) IBOutlet UITextField *pathNameDataText;
@property (weak, nonatomic) IBOutlet UITextField *folderDataText;
@property (weak, nonatomic) IBOutlet UITextField *fileDataText;
@property (weak, nonatomic) IBOutlet UITextField *dateTimeDataText;
@end

NS_ASSUME_NONNULL_END
