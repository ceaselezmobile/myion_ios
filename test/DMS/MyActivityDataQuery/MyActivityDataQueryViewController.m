//
//  MyActivityDataQueryViewController.m
//  test
//
//  Created by ceazeles on 09/01/19.
//  Copyright © 2019 ceaselez. All rights reserved.
//

#import "MyActivityDataQueryViewController.h"

@interface MyActivityDataQueryViewController ()<THDatePickerDelegate>

@end

@implementation MyActivityDataQueryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    formatter = [[NSDateFormatter alloc] init];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearAllData) name:@"clear_data" object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
}


-(void)clearAllData
{
    NSIndexPath *indexPath00 = [NSIndexPath indexPathForRow:0 inSection:0];
    MyActivityDataQueryTableViewCell *cell00 = [self.tableView cellForRowAtIndexPath:indexPath00];
    
    NSIndexPath *indexPath10 = [NSIndexPath indexPathForRow:0 inSection:1];
    MyActivityDataQueryTableViewCell *cell10 = [self.tableView cellForRowAtIndexPath:indexPath10];
    
    cell00.fromDateText.text = @"";
    cell10.toDateText.text = @"";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyActivityDataQueryTableViewCell *cell;
    
    if (indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"fromDateCell" forIndexPath:indexPath];
        [cell.fromDateText setLeftPadding:10.0f];
    }
    if (indexPath.section == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"toDateCell" forIndexPath:indexPath];
        [cell.toDateText setLeftPadding:10.0f];
    }
    return cell;
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSIndexPath *indexPath00 = [NSIndexPath indexPathForRow:0 inSection:0];
    MyActivityDataQueryTableViewCell *cell00 = [self.tableView cellForRowAtIndexPath:indexPath00];
    
    NSIndexPath *indexPath10 = [NSIndexPath indexPathForRow:0 inSection:1];
    MyActivityDataQueryTableViewCell *cell10 = [self.tableView cellForRowAtIndexPath:indexPath10];
    
    
    
    if(textField == cell00.fromDateText)
    {
        [self showDatePicker:@"fromDate"];
        dateType = @"fromDate";
        
    }
    else if(textField == cell10.toDateText)
    {if([cell00.fromDateText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please Select From Date" withTitle:@"Alert" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else{
        [self showDatePicker:@"toDate"];
        dateType = @"toDate";
    }
    }
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)showDatePicker:(NSString *)dateType
{
    if(!datePicker)
        datePicker = [THDatePickerViewController datePicker];
    datePicker.date = [NSDate date];
    datePicker.delegate = self;
    [datePicker setAllowClearDate:NO];
    [datePicker setClearAsToday:YES];
    [datePicker setAutoCloseOnSelectDate:NO];
    [datePicker setAllowSelectionOfSelectedDate:YES];
    [datePicker setDisableYearSwitch:YES];
    
    [datePicker setSelectedBackgroundColor:[UIColor colorWithRed:125/255.0 green:208/255.0 blue:0/255.0 alpha:1.0]];
    [datePicker setCurrentDateColor:[UIColor colorWithRed:242/255.0 green:121/255.0 blue:53/255.0 alpha:1.0]];
    [datePicker setCurrentDateColorSelected:[UIColor yellowColor]];
    
    [datePicker setDateHasItemsCallback:^BOOL(NSDate *date) {
        int tmp = (arc4random() % 30)+1;
        return (tmp % 5 == 0);
    }];
    [self presentSemiViewController:datePicker withOptions:@{
                                                             KNSemiModalOptionKeys.pushParentBack    : @(NO),
                                                             KNSemiModalOptionKeys.animationDuration : @(0.2),
                                                             KNSemiModalOptionKeys.shadowOpacity     : @(0.2),
                                                             }];
}

- (void)datePickerDonePressed:(THDatePickerViewController *)datePicker{
    
    NSIndexPath *indexPath00 = [NSIndexPath indexPathForRow:0 inSection:0];
    MyActivityDataQueryTableViewCell *cell00 = [self.tableView cellForRowAtIndexPath:indexPath00];
    
    NSIndexPath *indexPath10 = [NSIndexPath indexPathForRow:0 inSection:1];
    MyActivityDataQueryTableViewCell *cell10 = [self.tableView cellForRowAtIndexPath:indexPath10];
    //curDate = datePicker.date;
    
    NSDate *startDate = [NSDate date];
    NSDate *endDate = [NSDate date];
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    if (components.day <=0) {
        [formatter setDateFormat:@"yyyy-MM-dd"];
        if([dateType isEqualToString:@"fromDate"])
        {
            cell00.fromDateText.text = [formatter stringFromDate:datePicker.date];
        }
        else
        {
            NSDate *dt1 = [[NSDate alloc] init];
            
            NSDate *dt2 = [[NSDate alloc] init];
            
            dt1 = [formatter dateFromString:cell00.fromDateText.text];
            
            dt2 = datePicker.date;
            
            NSComparisonResult result = [dt1 compare:dt2];
            
            switch (result)
            {
                    
                case NSOrderedAscending:
                    NSLog(@"%@ is greater than %@", dt2, dt1);
                    
                    cell10.toDateText.text = [formatter stringFromDate:datePicker.date];
                    break;
                    
                case NSOrderedDescending:
                    [[Utitlity sharedInstance] showAlertViewWithMessage:@"To-Date must be greater than From-Date" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
                     {
                         
                     }];
                    NSLog(@"%@ is less %@", dt2, dt1);
                    break;
                    
                case NSOrderedSame: NSLog(@"%@ is equal to %@", dt2, dt1);
                    cell10.toDateText.text = [formatter stringFromDate:datePicker.date];
                    break;
                    
                default: NSLog(@"erorr dates %@, %@", dt2, dt1); break;
                    
            }
            
        }
    }
    else{
        //[self showMsgAlert:@"Enter the proper time duration"];
    }
    [self dismissSemiModalView];
}


- (void)datePickerCancelPressed:(THDatePickerViewController *)datePicker{
    [self dismissSemiModalView];
}

- (void)datePicker:(THDatePickerViewController *)datePicker selectedDate:(NSDate *)selectedDate {
    NSLog(@"Date selected: %@",[formatter stringFromDate:selectedDate]);
}

- (IBAction)viewReportButtonClick:(id)sender {
    
    NSIndexPath *indexPath00 = [NSIndexPath indexPathForRow:0 inSection:0];
    MyActivityDataQueryTableViewCell *cell00 = [self.tableView cellForRowAtIndexPath:indexPath00];
    
    NSIndexPath *indexPath10 = [NSIndexPath indexPathForRow:0 inSection:1];
    MyActivityDataQueryTableViewCell *cell10 = [self.tableView cellForRowAtIndexPath:indexPath10];
    
    if([cell00.fromDateText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please fill from date" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else if([cell10.toDateText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please fill to date" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else{
        
        MyActivityQueryTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyActivityQueryTableViewController"];
        vc.theURL = [NSString stringWithFormat:@"GetUserActivityDataQuery?userid=%@&fromdate=%@&todate=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"],cell00.fromDateText.text,cell10.toDateText.text];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (IBAction)clearAllButtonClick:(id)sender {
    [self clearAllData];
}


@end

