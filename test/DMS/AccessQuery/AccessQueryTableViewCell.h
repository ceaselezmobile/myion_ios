//
//  AccessQueryTableViewCell.h
//  test
//
//  Created by ceazeles on 20/12/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AccessQueryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *stakeHolderText;
@property (weak, nonatomic) IBOutlet UITextField *categoryText;
@property (weak, nonatomic) IBOutlet UITextField *companyText;
@property (weak, nonatomic) IBOutlet UITextField *nameText;
@property (weak, nonatomic) IBOutlet UITextField *fromDateText;
@property (weak, nonatomic) IBOutlet UITextField *toDateText;

@end

NS_ASSUME_NONNULL_END
