//
//  AccessQueryViewController.m
//  test
//
//  Created by ceazeles on 20/12/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "AccessQueryViewController.h"

@interface AccessQueryViewController ()<UITextFieldDelegate,THDatePickerDelegate,SearchDelegate>

@end

@implementation AccessQueryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    formatter = [[NSDateFormatter alloc] init];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [self.menuButton addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    count = @"0";
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clearAllData) name:@"clear_data" object:nil];

}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
}

-(void)clearAllData
{
    NSIndexPath *indexPath00 = [NSIndexPath indexPathForRow:0 inSection:0];
    AccessQueryTableViewCell *cell00 = [self.tableView cellForRowAtIndexPath:indexPath00];
    
    NSIndexPath *indexPath10 = [NSIndexPath indexPathForRow:0 inSection:1];
    AccessQueryTableViewCell *cell10 = [self.tableView cellForRowAtIndexPath:indexPath10];
    
    NSIndexPath *indexPath20 = [NSIndexPath indexPathForRow:0 inSection:2];
    AccessQueryTableViewCell *cell20 = [self.tableView cellForRowAtIndexPath:indexPath20];
    
    NSIndexPath *indexPath30 = [NSIndexPath indexPathForRow:0 inSection:3];
    AccessQueryTableViewCell *cell30 = [self.tableView cellForRowAtIndexPath:indexPath30];
    
    NSIndexPath *indexPath40 = [NSIndexPath indexPathForRow:0 inSection:4];
    AccessQueryTableViewCell *cell40 = [self.tableView cellForRowAtIndexPath:indexPath40];
    
    NSIndexPath *indexPath50 = [NSIndexPath indexPathForRow:0 inSection:5];
    AccessQueryTableViewCell *cell50 = [self.tableView cellForRowAtIndexPath:indexPath50];
    
    cell00.stakeHolderText.text = @"";
    cell10.categoryText.text = @"";
    cell20.companyText.text = @"";
    cell30.nameText.text = @"";
    cell40.fromDateText.text = @"";
    cell50.toDateText.text = @"";
    
    count = @"0";
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if([count isEqualToString:@"1"])
    {
        if(section == 1 || section == 2)
        {
            return 0;
        }
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AccessQueryTableViewCell *cell;
    
    if (indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"stakeholderCell" forIndexPath:indexPath];
        [cell.stakeHolderText setLeftPadding:10.0f];
        
    }
    if (indexPath.section == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"categoryCell" forIndexPath:indexPath];
        [cell.categoryText setLeftPadding:10.0f];
    }
    if (indexPath.section == 2) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"companyCell" forIndexPath:indexPath];
        [cell.companyText setLeftPadding:10.0f];
    }
    if (indexPath.section == 3) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"nameCell" forIndexPath:indexPath];
        [cell.nameText setLeftPadding:10.0f];
    }
    if (indexPath.section == 4) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"fromDateCell" forIndexPath:indexPath];
        [cell.fromDateText setLeftPadding:10.0f];
    }
    if (indexPath.section == 5) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"toDateCell" forIndexPath:indexPath];
        [cell.toDateText setLeftPadding:10.0f];
    }
    return cell;
    
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSIndexPath *indexPath00 = [NSIndexPath indexPathForRow:0 inSection:0];
    AccessQueryTableViewCell *cell00 = [self.tableView cellForRowAtIndexPath:indexPath00];
    
    NSIndexPath *indexPath10 = [NSIndexPath indexPathForRow:0 inSection:1];
    AccessQueryTableViewCell *cell10 = [self.tableView cellForRowAtIndexPath:indexPath10];
    
    NSIndexPath *indexPath20 = [NSIndexPath indexPathForRow:0 inSection:2];
    AccessQueryTableViewCell *cell20 = [self.tableView cellForRowAtIndexPath:indexPath20];
    
    NSIndexPath *indexPath30 = [NSIndexPath indexPathForRow:0 inSection:3];
    AccessQueryTableViewCell *cell30 = [self.tableView cellForRowAtIndexPath:indexPath30];
    
    NSIndexPath *indexPath40 = [NSIndexPath indexPathForRow:0 inSection:4];
    AccessQueryTableViewCell *cell40 = [self.tableView cellForRowAtIndexPath:indexPath40];
    
    NSIndexPath *indexPath50 = [NSIndexPath indexPathForRow:0 inSection:5];
    AccessQueryTableViewCell *cell50 = [self.tableView cellForRowAtIndexPath:indexPath50];
    
    
    SearchDataQueryViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchDataQueryViewController"];
    vc.delegate = self;
    if(textField == cell00.stakeHolderText)
    {
        [self showAlert];
    }
    if(textField == cell10.categoryText)
    {
        if([cell00.stakeHolderText.text isEqualToString:@""])
        {
            [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please Select Stakeholder Type" withTitle:@"Alert" forController:self withCallback:^(BOOL onClickOk)
             {
                 
             }];
        }
        else{
            vc.searchFlag = @"Category";
            vc.searchScreen = @"Common";
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    else if(textField == cell20.companyText)
    {
        if([cell10.categoryText.text isEqualToString:@""])
        {
            [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please Select Category" withTitle:@"Alert" forController:self withCallback:^(BOOL onClickOk)
             {
                 
             }];
        }
        else{
            vc.searchFlag = @"Company";
            vc.searchScreen = @"Common";
            vc.theId = categoryId;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    else if(textField == cell30.nameText)
    {
        if([cell20.companyText.text isEqualToString:@""])
        {
            [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please Select Company" withTitle:@"Alert" forController:self withCallback:^(BOOL onClickOk)
             {
                 
             }];
        }
        else{
            vc.searchFlag = @"Name";
            
            if([cell00.stakeHolderText.text isEqualToString:@"Internal"])
            {
                vc.searchScreen = @"Common";
                vc.theURL = @"GetEmployee";
            }
            else if([cell00.stakeHolderText.text isEqualToString:@"External"])
            {
                vc.searchScreen = @"External";
                vc.theURL = [NSString stringWithFormat:@"/GetExternalStakeholderbyid?id=%@",externalStakeHolderId];
            }
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    else if(textField == cell40.fromDateText)
    {if([cell30.nameText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please Select Name" withTitle:@"Alert" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else{
        [self showDatePicker:@"fromDate"];
        dateType = @"fromDate";
    }
    }
    else if(textField == cell50.toDateText)
    {if([cell40.fromDateText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please Select From Date" withTitle:@"Alert" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else{
        [self showDatePicker:@"toDate"];
        dateType = @"toDate";
    }
    }
    [textField resignFirstResponder];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)showAlert
{
    NSIndexPath *indexPath00 = [NSIndexPath indexPathForRow:0 inSection:0];
    AccessQueryTableViewCell *cell00 = [self.tableView cellForRowAtIndexPath:indexPath00];
    
    NSIndexPath *indexPath10 = [NSIndexPath indexPathForRow:0 inSection:1];
    AccessQueryTableViewCell *cell10 = [self.tableView cellForRowAtIndexPath:indexPath10];
    
    NSIndexPath *indexPath20 = [NSIndexPath indexPathForRow:0 inSection:2];
    AccessQueryTableViewCell *cell20 = [self.tableView cellForRowAtIndexPath:indexPath20];
    
    NSIndexPath *indexPath30 = [NSIndexPath indexPathForRow:0 inSection:3];
    AccessQueryTableViewCell *cell30 = [self.tableView cellForRowAtIndexPath:indexPath30];
    
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Internal" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        cell00.stakeHolderText.text = @"Internal";
        cell10.categoryText.text = @"";
        cell20.companyText.text = @"";
        cell30.nameText.text = @"";
        count = @"1";
        [self.tableView reloadData];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"External" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        count = @"0";
        [self.tableView reloadData];
        cell00.stakeHolderText.text = @"External";
        cell10.categoryText.text = @"";
        cell20.companyText.text = @"";
        cell30.nameText.text = @"";
        
    }];
    
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void) selectedString:(NSString*)string selectedID:(NSString *)theId anotherId:(NSString *)otherId  selectFlag:(NSString *)theFlag
{
    NSIndexPath *indexPath10 = [NSIndexPath indexPathForRow:0 inSection:1];
    AccessQueryTableViewCell *cell10 = [self.tableView cellForRowAtIndexPath:indexPath10];
    
    NSIndexPath *indexPath20 = [NSIndexPath indexPathForRow:0 inSection:2];
    AccessQueryTableViewCell *cell20 = [self.tableView cellForRowAtIndexPath:indexPath20];
    
    NSIndexPath *indexPath30 = [NSIndexPath indexPathForRow:0 inSection:3];
    AccessQueryTableViewCell *cell30 = [self.tableView cellForRowAtIndexPath:indexPath30];
    
    if([theFlag isEqualToString:@"Category"])
    {
        cell10.categoryText.text = string;
        categoryId = theId;
    }
    else if([theFlag isEqualToString:@"Company"])
    {
        cell20.companyText.text = string;
        companyId = theId;
        externalStakeHolderId = otherId;
    }
    else if([theFlag isEqualToString:@"Name"])
    {
        cell30.nameText.text = string;
        nameId = theId;
    }
}


-(void)showDatePicker:(NSString *)dateType
{
    if(!datePicker)
        datePicker = [THDatePickerViewController datePicker];
    datePicker.date = [NSDate date];
    datePicker.delegate = self;
    [datePicker setAllowClearDate:NO];
    [datePicker setClearAsToday:YES];
    [datePicker setAutoCloseOnSelectDate:NO];
    [datePicker setAllowSelectionOfSelectedDate:YES];
    [datePicker setDisableYearSwitch:YES];
    
    [datePicker setSelectedBackgroundColor:[UIColor colorWithRed:125/255.0 green:208/255.0 blue:0/255.0 alpha:1.0]];
    [datePicker setCurrentDateColor:[UIColor colorWithRed:242/255.0 green:121/255.0 blue:53/255.0 alpha:1.0]];
    [datePicker setCurrentDateColorSelected:[UIColor yellowColor]];
    
    [datePicker setDateHasItemsCallback:^BOOL(NSDate *date) {
        int tmp = (arc4random() % 30)+1;
        return (tmp % 5 == 0);
    }];
    [self presentSemiViewController:datePicker withOptions:@{
                                                             KNSemiModalOptionKeys.pushParentBack    : @(NO),
                                                             KNSemiModalOptionKeys.animationDuration : @(0.2),
                                                             KNSemiModalOptionKeys.shadowOpacity     : @(0.2),
                                                             }];
}

- (void)datePickerDonePressed:(THDatePickerViewController *)datePicker{
    
    NSIndexPath *indexPath40 = [NSIndexPath indexPathForRow:0 inSection:4];
    AccessQueryTableViewCell *cell40 = [self.tableView cellForRowAtIndexPath:indexPath40];
    
    NSIndexPath *indexPath50 = [NSIndexPath indexPathForRow:0 inSection:5];
    AccessQueryTableViewCell *cell50 = [self.tableView cellForRowAtIndexPath:indexPath50];
    //curDate = datePicker.date;
    
    NSDate *startDate = [NSDate date];
    NSDate *endDate = [NSDate date];
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    if (components.day <=0) {
        [formatter setDateFormat:@"yyyy-MM-dd"];
        if([dateType isEqualToString:@"fromDate"])
        {
            cell40.fromDateText.text = [formatter stringFromDate:datePicker.date];
        }
        else
        {
            NSDate *dt1 = [[NSDate alloc] init];
            
            NSDate *dt2 = [[NSDate alloc] init];
            
            dt1 = [formatter dateFromString:cell40.fromDateText.text];
            
            dt2 = datePicker.date;
            
            NSComparisonResult result = [dt1 compare:dt2];
            
            switch (result)
            {
                    
                case NSOrderedAscending:
                    NSLog(@"%@ is greater than %@", dt2, dt1);
                    
                    cell50.toDateText.text = [formatter stringFromDate:datePicker.date];
                    break;
                    
                case NSOrderedDescending:
                    [[Utitlity sharedInstance] showAlertViewWithMessage:@"To-Date must be greater than From-Date" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
                     {
                         
                     }];
                    NSLog(@"%@ is less %@", dt2, dt1);
                    break;
                    
                case NSOrderedSame: NSLog(@"%@ is equal to %@", dt2, dt1);
                    cell50.toDateText.text = [formatter stringFromDate:datePicker.date];
                    break;
                    
                default: NSLog(@"erorr dates %@, %@", dt2, dt1); break;
                    
            }
            
        }
    }
    else{
        //[self showMsgAlert:@"Enter the proper time duration"];
    }
    [self dismissSemiModalView];
}


- (void)datePickerCancelPressed:(THDatePickerViewController *)datePicker {
    [self dismissSemiModalView];
}

- (void)datePicker:(THDatePickerViewController *)datePicker selectedDate:(NSDate *)selectedDate {
    NSLog(@"Date selected: %@",[formatter stringFromDate:selectedDate]);
}

- (IBAction)viewReportButtonClick:(id)sender {
    NSIndexPath *indexPath00 = [NSIndexPath indexPathForRow:0 inSection:0];
    AccessQueryTableViewCell *cell00 = [self.tableView cellForRowAtIndexPath:indexPath00];
    
    NSIndexPath *indexPath10 = [NSIndexPath indexPathForRow:0 inSection:1];
    AccessQueryTableViewCell *cell10 = [self.tableView cellForRowAtIndexPath:indexPath10];
    
    NSIndexPath *indexPath20 = [NSIndexPath indexPathForRow:0 inSection:2];
    AccessQueryTableViewCell *cell20 = [self.tableView cellForRowAtIndexPath:indexPath20];
    
    NSIndexPath *indexPath30 = [NSIndexPath indexPathForRow:0 inSection:3];
    AccessQueryTableViewCell *cell30 = [self.tableView cellForRowAtIndexPath:indexPath30];
    
    NSIndexPath *indexPath40 = [NSIndexPath indexPathForRow:0 inSection:4];
    AccessQueryTableViewCell *cell40 = [self.tableView cellForRowAtIndexPath:indexPath40];
    
    NSIndexPath *indexPath50 = [NSIndexPath indexPathForRow:0 inSection:5];
    AccessQueryTableViewCell *cell50 = [self.tableView cellForRowAtIndexPath:indexPath50];
    
    if([cell00.stakeHolderText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please fill stakeholder type" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else if([cell10.categoryText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please fill category" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else if([cell20.toDateText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please fill company" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else if([cell30.nameText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please fill name" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else if([cell40.fromDateText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please fill from date" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else if([cell50.toDateText.text isEqualToString:@""])
    {
        [[Utitlity sharedInstance] showAlertViewWithMessage:@"Please fill to date" withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
    else{
        AccessQueryTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AccessQueryTableViewController"];
        vc.theURL = [NSString stringWithFormat:@"GetUserAccessDataQuery?userid=%@&fromdate=%@&todate=%@",nameId,cell40.fromDateText.text,cell50.toDateText.text];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (IBAction)clearAllButtonClick:(id)sender {
    [self clearAllData];
}

@end
