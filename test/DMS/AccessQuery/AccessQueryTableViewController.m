//
//  AccessQueryTableViewController.m
//  test
//
//  Created by ceazeles on 24/12/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "AccessQueryTableViewController.h"

@interface AccessQueryTableViewController ()

@end

@implementation AccessQueryTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height/2,self.view.frame.size.width ,50)];
    
    
    [self getDelegate:self.theURL];
    
    lengthPath = 0;
    lengthFile = 0;
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    if (@available(iOS 10.0, *)) {
        self.tableView.refreshControl = refreshControl;
    } else {
        [self.tableView addSubview:refreshControl];
    }
    
}

-(void)refreshTable{
    [self getDelegate:self.theURL];
    [refreshControl endRefreshing];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 1)
    {
        return array.count;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AccessDataQueryTableViewCell *cell;
    
    if (indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell" forIndexPath:indexPath];
       // cell.pathHeaderWidth.constant = 8 * lengthPath;
       // cell.fileHeaderWidth.constant = 8 * lengthFile;
        
        cell.pathNameHeaderText.text = @"Path Name";
        cell.folderNameHeaderText.text = @"Folder Name";
        cell.folderDateTimeHeaderText.text = @"Folder Access Date";
        cell.fileHeaderText.text = @"Files";
        cell.dateTimeHeaderText.text = @"Date & Time";
    }
    if (indexPath.section == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"dataCell" forIndexPath:indexPath];
        if(array.count > 0)
        {
           // cell.pathDataWidth.constant = 8 * lengthPath;
           // cell.fileDataWidth.constant = 8 * lengthFile;
            
            NSDictionary *dict = array[indexPath.row];
            cell.pathNameDataText.text = dict[@"Path"];
            cell.folderNameDataText.text = dict[@"CategoryName"];
            cell.folderDateTimeDataText.text = dict[@"FolderAccessDate"];
            cell.fileDataText.text = dict[@"FileName"];
           // cell.dateTimeText.text = dict[@"FileDate"];
        }
        
    }
    return cell;
}
-(void)getDelegate :(NSString *)url
{
    if([Utitlity isConnectedTointernet]){
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[ServiceManager sharedInstance] getWithParameter:nil withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            array = [NSMutableArray array];
            [self->array addObjectsFromArray:responseObject];
            if(self->array.count > 0)
            {
                for (int i = 0; i<array.count; i++) {
                    long len1 = [[[array objectAtIndex:i] objectForKey:@"Path"] length];
                    long len2 = [[[array objectAtIndex:i] objectForKey:@"FileName"] length];
                    if (len1 > lengthPath) {
                        lengthPath = len1;
                        NSLog(@"index-----%ld",14 * lengthPath);
                    }
                    
                    if(len1 > lengthFile)
                    {
                        lengthFile = len2;
                        NSLog(@"index-----%ld",14 * lengthPath);
                    }
                }
                
               // self.tableViewWidth.constant = 1331+ 4*lengthPath + 4*lengthFile;
                [self removeNoDataFound:self.view];
                [self.tableView reloadData];
            }
            else{
                [self showNoDataFound:self.view];
            }
            
            NSLog(@"Search Response  %@",responseObject);
        });
    }];
    }
    else{
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
}

-(void)showNoDataFound :(UIView *)view
{
    if(![noDataLabel isDescendantOfView:view])
    {
        [view addSubview:noDataLabel];
    }
    [noDataLabel setFont:[UIFont systemFontOfSize:20]];
    noDataLabel.textAlignment=NSTextAlignmentCenter;
    noDataLabel.layer.masksToBounds  = YES;
    noDataLabel.layer.shadowOpacity  = 2.5;
    noDataLabel.layer.shadowColor    = [[UIColor grayColor] CGColor];
    noDataLabel.layer.shadowOffset   = CGSizeMake(0, 1);
    noDataLabel.layer.shadowRadius   = 2;
    noDataLabel.text=@"No Data Found";
}

-(void)removeNoDataFound :(UIView *)view
{
    [noDataLabel removeFromSuperview];
}


- (IBAction)backButtonClick:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
