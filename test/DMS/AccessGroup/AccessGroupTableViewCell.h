//
//  AccessGroupTableViewCell.h
//  test
//
//  Created by ceazeles on 27/12/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AccessGroupTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *groupNameHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *stakeHolderTypeHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *nameHeaderText;

@property (weak, nonatomic) IBOutlet UITextField *groupNameDataText;
@property (weak, nonatomic) IBOutlet UITextField *stakeHolderTypeText;
@property (weak, nonatomic) IBOutlet UITextField *nameDataText;

@end

NS_ASSUME_NONNULL_END
