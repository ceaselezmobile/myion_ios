//
//  LoginQueryTableViewController.h
//  test
//
//  Created by ceazeles on 24/12/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginDataQueryTableViewCell.h"
#import "MBProgressHUD.h"
#import "ServiceManager.h"
#import "GlobalURL.h"
#import "Utitlity.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginQueryTableViewController : UIViewController
{
        NSMutableArray *array;
        UILabel *noDataLabel;
       UIRefreshControl *refreshControl;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(strong,nonatomic)NSString *theURL;

@end

NS_ASSUME_NONNULL_END
