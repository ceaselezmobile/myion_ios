//
//  SearchDataQueryViewController.h
//  test
//
//  Created by ceazeles on 26/12/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ServiceManager.h"
#import "GlobalURL.h"
#import "Utitlity.h"

NS_ASSUME_NONNULL_BEGIN

@protocol SearchDelegate <NSObject>
- (void) selectedString:(NSString*)string selectedID:(NSString *)theId anotherId:(NSString *)otherId selectFlag:(NSString *)theFlag;
@end

@interface SearchDataQueryViewController : UIViewController
{
    NSMutableArray *searchArray;
    NSMutableArray *searchResultArray;
    UILabel *noDataLabel;
}

@property (assign)BOOL isALL;
@property (assign)BOOL searchBarActive;
@property(strong,nonatomic)NSString *theId;
@property(strong,nonatomic)NSString *theURL;
@property(strong,nonatomic)NSString *searchFlag;
@property(strong,nonatomic)NSString *searchScreen;
@property (nonatomic, assign) id<SearchDelegate>delegate;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

NS_ASSUME_NONNULL_END
