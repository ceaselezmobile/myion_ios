//
//  MyAccessQueryTableViewCell.h
//  test
//
//  Created by ceazeles on 08/01/19.
//  Copyright © 2019 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyAccessQueryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *folderNameHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *pathNameHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *folderDateTimeHeaderText;
@property (weak, nonatomic) IBOutlet UITextField *fileHeaderText;

@property (weak, nonatomic) IBOutlet UITextField *folderNameDataText;
@property (weak, nonatomic) IBOutlet UITextField *pathNameDataText;
@property (weak, nonatomic) IBOutlet UITextField *folderDateTimeDataText;
@property (weak, nonatomic) IBOutlet UITextField *fileDataText;

@end

NS_ASSUME_NONNULL_END
