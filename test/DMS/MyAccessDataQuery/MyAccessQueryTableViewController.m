//
//  MyAccessQueryTableViewController.m
//  test
//
//  Created by ceazeles on 08/01/19.
//  Copyright © 2019 ceaselez. All rights reserved.
//

#import "MyAccessQueryTableViewController.h"

@interface MyAccessQueryTableViewController ()

@end

@implementation MyAccessQueryTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    searchArray = [NSMutableArray array];
    
    noDataLabel = [[UILabel alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height/2,self.view.frame.size.width ,50)];
    
    
    [self getDelegate:self.theURL];
    
    lengthPath = 0;
    lengthFile = 0;
    
    refreshControl = [[UIRefreshControl alloc]init];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    if (@available(iOS 10.0, *)) {
        self.tableView.refreshControl = refreshControl;
    } else {
        [self.tableView addSubview:refreshControl];
    }
    
    [self.searchBar setValue:[UIColor whiteColor] forKeyPath:@"_searchField._placeholderLabel.textColor"];
}

-(void)refreshTable{
    [self getDelegate:self.theURL];
    [refreshControl endRefreshing];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 1)
    {
        if(self.searchBarActive)
        {
            return searchArray.count;
        }
        else
        {
            return array.count;
        }
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyAccessQueryTableViewCell *cell;
    
    if (indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell" forIndexPath:indexPath];
        // cell.pathHeaderWidth.constant = 8 * lengthPath;
        // cell.fileHeaderWidth.constant = 8 * lengthFile;
        
        cell.pathNameHeaderText.text = @"Folder Name";
        cell.folderNameHeaderText.text = @"Path Name";
        cell.folderDateTimeHeaderText.text = @"Folder Access Date";
        cell.fileHeaderText.text = @"File Name";
        
    }
    if (indexPath.section == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"dataCell" forIndexPath:indexPath];
        NSArray *arr = [NSArray array];
        if(self.searchBarActive)
        {
            arr = [searchArray mutableCopy];
        }
        else
        {
            arr = [array mutableCopy];
        }
        if(arr.count > 0)
        {
            // cell.pathDataWidth.constant = 8 * lengthPath;
            // cell.fileDataWidth.constant = 8 * lengthFile;
            
            NSDictionary *dict = arr[indexPath.row];
            cell.pathNameDataText.text = dict[@"CategoryName"];
            cell.folderNameDataText.text = dict[@"Path"];
            cell.folderDateTimeDataText.text = dict[@"FolderAccessDate"];
            cell.fileDataText.text = dict[@"FileName"];
            
        }
        
    }
    return cell;
}
-(void)getDelegate :(NSString *)url
{
    if([Utitlity isConnectedTointernet]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[ServiceManager sharedInstance] getWithParameter:nil withUrl:[NSString stringWithFormat:@"%@",url] withHandler:^(id responseObject, NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                array = [NSMutableArray array];
                [self->array addObjectsFromArray:responseObject];
                if(self->array.count > 0)
                {
                    for (int i = 0; i<array.count; i++) {
                        long len1 = [[[array objectAtIndex:i] objectForKey:@"Path"] length];
                        long len2 = [[[array objectAtIndex:i] objectForKey:@"FileName"] length];
                        if (len1 > lengthPath) {
                            lengthPath = len1;
                            NSLog(@"index-----%ld",14 * lengthPath);
                        }
                        
                        if(len1 > lengthFile)
                        {
                            lengthFile = len2;
                            NSLog(@"index-----%ld",14 * lengthPath);
                        }
                    }
                    
                    // self.tableViewWidth.constant = 1331+ 4*lengthPath + 4*lengthFile;
                    [self removeNoDataFound:self.view];
                    [self.tableView reloadData];
                }
                else{
                    [self showNoDataFound:self.view];
                }
                
                NSLog(@"Search Response  %@",responseObject);
            });
        }];
    }
    else{
        [[Utitlity sharedInstance] showAlertViewWithMessage:NOInternetMessage withTitle:@"" forController:self withCallback:^(BOOL onClickOk)
         {
             
         }];
    }
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [searchArray removeAllObjects];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"CategoryName contains[c] %@", searchText];
   
    searchArray  = [NSMutableArray arrayWithArray:[array filteredArrayUsingPredicate:resultPredicate]];
    if(searchArray.count > 0)
    {
        self.tableView.hidden = NO;
        [self.tableView reloadData];
        [self removeNoDataFound:self.view];
    }
    else{
        self.tableView.hidden = YES;
        [self showNoDataFound:self.view];
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length>0)
    {
        self.searchBarActive = YES;
        [self filterContentForSearchText:searchText scope:[[self.searchBar scopeButtonTitles] objectAtIndex:[self.searchBar selectedScopeButtonIndex]]];
        
    }else{
        [self removeNoDataFound:self.view];
        self.searchBarActive = NO;
        self.tableView.hidden = NO;
        [self.tableView reloadData];
    }
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self cancelSearching];
    [self.tableView reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    self.searchBarActive = YES;
    [self.view endEditing:YES];
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self.searchBar setShowsCancelButton:NO animated:YES];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    self.searchBarActive = NO;
    [self.searchBar setShowsCancelButton:NO animated:YES];
}
-(void)cancelSearching
{
    self.searchBarActive = NO;
    [self.searchBar resignFirstResponder];
    self.searchBar.text  = @"";
}

-(void)showNoDataFound :(UIView *)view
{
    if(![noDataLabel isDescendantOfView:view])
    {
        [view addSubview:noDataLabel];
    }
    [noDataLabel setFont:[UIFont systemFontOfSize:20]];
    noDataLabel.textAlignment=NSTextAlignmentCenter;
    noDataLabel.layer.masksToBounds  = YES;
    noDataLabel.layer.shadowOpacity  = 2.5;
    noDataLabel.layer.shadowColor    = [[UIColor grayColor] CGColor];
    noDataLabel.layer.shadowOffset   = CGSizeMake(0, 1);
    noDataLabel.layer.shadowRadius   = 2;
    noDataLabel.text=@"No Data Found";
}

-(void)removeNoDataFound :(UIView *)view
{
    [noDataLabel removeFromSuperview];
}
- (IBAction)backButtonClick:(id)sender {
    
   
    [self.navigationController popViewControllerAnimated:YES];
}

@end
