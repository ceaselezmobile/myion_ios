//
//  MyMeetingViewController.m
//  test
//
//  Created by ceaselez on 26/04/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "MyMeetingViewController.h"
#import "MyMeetingTableViewCell.h"
#import "MySharedManager.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
@interface MyMeetingViewController ()
@property (weak, nonatomic) IBOutlet UIButton *daysBtton;
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewWidth;
@end

@implementation MyMeetingViewController
{
    NSMutableArray *dataArray;
    long tableIndex;
    int textFieldNO;
    long lenght;
    int range;
    __weak IBOutlet UIButton *menuBtn;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.navigationController setNavigationBarHidden:YES animated:YES];

    dataArray = [[NSMutableArray alloc] init];
    lenght = 0;
    range = 0;
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    //    _dataTableView.transform = CGAffineTransformMakeRotation(-M_PI_2);
    
    [self apiCall];
}
-(void)viewWillAppear:(BOOL)animated{
    [self viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self apiCall];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
        if (dataArray.count == 0) {
            return 1;
        }
    return dataArray.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (dataArray.count == 0) {
        UITableViewCell *cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SimpleTableItem"];
        cell1.textLabel.text = @"Meetings are not found";
        cell1.backgroundColor = [UIColor clearColor];
        cell1.textLabel.textColor = [UIColor blackColor];
        cell1.userInteractionEnabled  = NO;
        return cell1;
    }
    static NSString *simpleTableIdentifier = @"dataCell";
    
    MyMeetingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.dataWidth.constant = 8*lenght;
    
    cell.data1.text = [NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:indexPath.row] objectForKey:@"Group"]];
    cell.data2.text = [NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:indexPath.row] objectForKey:@"Description"]];
    cell.data3.text = [NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:indexPath.row] objectForKey:@"Date"]];
    cell.data4.text = [NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:indexPath.row] objectForKey:@"FromTime"]];
    cell.data5.text = [NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:indexPath.row] objectForKey:@"ToTime"]];
    if ([NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:indexPath.row] objectForKey:@"Place"]]) {
        cell.data6.text = [NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:indexPath.row] objectForKey:@"Place"]];
    }
    cell.data7.text = [NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:indexPath.row] objectForKey:@"Secretary"]];
    cell.data8.text = [NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:indexPath.row] objectForKey:@"Participants"]];
    
    return  cell;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *simpleTableIdentifier = @"headerCell";
    MyMeetingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.headerWidth.constant = 8 * lenght;
    
    cell.header1.text = @"Group";
    cell.header2.text = @"Description";
    cell.header3.text = @"Date";
    cell.header4.text = @"From Time";
    cell.header5.text = @"To Time";
    cell.header6.text = @"Place";
    cell.header7.text = @"Secretary";
    cell.header8.text = @"Participants";
    return  cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (dataArray.count == 0) {
        return 0;
    }
    return 44;
}

-(void)apiCall{
    if([Utitlity isConnectedTointernet]){
        
        [self showProgress];
    NSMutableURLRequest *urlRequest;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@Mymeeting?userid=%@&Range=%d",BASEURL,[defaults objectForKey:@"UserID"],range]]];
    NSLog(@"urlRequest-----%@",urlRequest);
    
    //create the Method "GET"
    [urlRequest setHTTPMethod:@"GET"];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                          if(httpResponse.statusCode == 200)
                                          {
                                              NSError *parseError = nil;
                                              dataArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                                              NSLog(@"dataArray---%@",dataArray);
                                              for (int i = 0; i<dataArray.count; i++) {
                                                  long len = [[[dataArray objectAtIndex:i] objectForKey:@"Description"] length];
                                                  if (len > lenght) {
                                                      lenght = len;
                                                      NSLog(@"index-----%ld",14*lenght);
                                                  }
                                              }
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [self hideProgress];
                                                  [_dataTableView reloadData];
                                                  _tableViewWidth.constant = 1168+8*lenght;
                                                  
                                              });
                                          }
                                          else
                                          {
                                              NSLog(@"Error");
                                          }
                                      }];
    [dataTask resume];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }

}
-(void)showProgress{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}

-(void)hideProgress{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)daysButton:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select" message:nil preferredStyle:UIAlertControllerStyleAlert];
    // create the actions handled by each button
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"All" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        range = 0;
        [self.daysBtton setTitle:@"All" forState:UIControlStateNormal];
        [self apiCall];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Tomorrow" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.daysBtton setTitle:@"Tomorrow" forState:UIControlStateNormal];
        range = 1;
        [self apiCall];
    }];
    
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"Next 7 Days" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.daysBtton setTitle:@"Next 7 Days" forState:UIControlStateNormal];
        range = 7;
        [self apiCall];
    }];
    UIAlertAction *action4 = [UIAlertAction actionWithTitle:@"This Month" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.daysBtton setTitle:@"This Month" forState:UIControlStateNormal];
        range = 30;
        [self apiCall];
    }];
    UIAlertAction *action5 = [UIAlertAction actionWithTitle:@"Next Month" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.daysBtton setTitle:@"Next Month" forState:UIControlStateNormal];
        range = 60;
        [self apiCall];
    }];
    
    
    // add actions to our sheet
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    [actionSheet addAction:action3];
    [actionSheet addAction:action4];
    [actionSheet addAction:action5];

    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        // Called when user taps outside
    }]];
    // bring up the action sheet
    [self presentViewController:actionSheet animated:YES completion:nil];
}

@end
