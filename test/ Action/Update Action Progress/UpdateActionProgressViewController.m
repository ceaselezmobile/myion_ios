
#import "UpdateActionProgressViewController.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "CustomTableViewCell.h"
#import "MySharedManager.h"
#import "ActionPlannerDetailViewController.h"
#import "THDatePickerViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/ALAssetRepresentation.h>

#import "AFHTTPSessionManager.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFHTTPRequestOperation.h"

@interface UpdateActionProgressViewController ()<THDatePickerDelegate,UIDocumentPickerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *internalBtn;
@property (weak, nonatomic) IBOutlet UIButton *externalBtn;
@property (weak, nonatomic) IBOutlet UIView *internalBtnView;
@property (weak, nonatomic) IBOutlet UIView *externalBtnView;
@property (weak, nonatomic) IBOutlet UILabel *tittleLabel;
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;
@property (weak, nonatomic) IBOutlet UIView *searchBarView;
@property (nonatomic, strong) NSArray *searchResult;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIView *popOverView;
@property (weak, nonatomic) IBOutlet UITableView *popOverTableView;

@end

@implementation UpdateActionProgressViewController
{
    BOOL type;
    NSMutableArray *actionProgressDict;
    MySharedManager *sharedManager;
    UIRefreshControl *refreshControl;
    NSString *actionStatus;
    
    __weak IBOutlet UIButton *menuBtn;
    BOOL searchEnabled;
    __weak IBOutlet UIButton *actionStatusBtn;
    THDatePickerViewController * datePicker1;
    NSDate * curDate;
    NSDateFormatter * formatter;
    NSString *textViewText;
    NSString *filetxtview;
    
    __weak IBOutlet NSLayoutConstraint *popOverTableViewHeight;
    long editButtonIndex;
    UILabel *depenlbl;
    UIButton *donebtn;
    UILabel *Docname;
    UIView *DocView;
    UIButton *delete,*Completiondatebtn,*uploadbtn;
    UITextView *remarkstxtfld;
    CustomTableViewCell *cell;
    NSString *alertMessage;
    NSURL *filepath;
    NSData *fileData;
    UIImagePickerController *idPicker;
    NSMutableArray *filesArray;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    textViewText = @"";
    filetxtview=@"";
    _popOverView.hidden = YES;
    idPicker = [[UIImagePickerController alloc] init];
    filesArray = [[NSMutableArray alloc] init];
    idPicker.delegate = self;
    sharedManager = [MySharedManager sharedManager];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    actionProgressDict = [[NSMutableArray alloc] init];
    _internalBtnView.hidden = NO;
    _externalBtnView.hidden = YES;
    _searchBarView.hidden = YES;
    _tittleLabel.text = @"Self Planner List";
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [_dataTableView addGestureRecognizer:swipeLeft];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self  action:@selector(didSwipe:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [_dataTableView addGestureRecognizer:swipeRight];
    actionStatus = @"i";
    //    [self loadDataFromApi];
    refreshControl = [[UIRefreshControl alloc]init];
    [self.dataTableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(loadDataFromApi) forControlEvents:UIControlEventValueChanged];
    curDate = [NSDate date];
    formatter = [[NSDateFormatter alloc] init];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"IsExternal"] isEqualToString:@"Yes"]) {
        _internalBtnView.hidden = YES;
        _externalBtnView.hidden = YES;
        _internalBtn.hidden = YES;
        _externalBtn.hidden = YES;
        //        type = YES;
        
    }
}
-(void) viewWillAppear:(BOOL)animated
{
    [formatter setDateFormat:@"dd-MM-yyyy"];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        actionStatus = @"i";
        _popOverView.hidden = YES;
        [filesArray removeAllObjects];
    }
    [self loadDataFromApi];
}
-(void)viewDidAppear:(BOOL)animated{
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        sharedManager.slideMenuSlected = @"no";
    }
}
- (void)didSwipe:(UISwipeGestureRecognizer*)swipe{
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        [self externalBtnClicked];
        
    } else if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
        [self internalBtnClicked];
        
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == _popOverTableView) {
        if (_popOverView.hidden) {
            return 0;
        }
        if(filesArray.count == 0)
        {
            return 4;
        }
        else
            return 5+filesArray.count;
    }
    else{
        if (searchEnabled) {
            if (_searchResult.count == 0) {
                return 1;
            }
            return [self.searchResult count];
        }
        return actionProgressDict.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == _popOverTableView) {
        
        if (indexPath.row == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Completion Date" forIndexPath:indexPath];
            cell.updateCompletionDateTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            cell.updateCompletionDateTF.text = [formatter stringFromDate:curDate];
            Completiondatebtn=[cell viewWithTag:40];
            if([[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"DependentMemberActionStatus"] isEqualToString:@"NA"]||[[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"DependentMemberActionStatus"] isEqual:[NSNull null]] || [[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"DependentMemberActionStatus"] isEqual:@"Completed"])
            {
                [Completiondatebtn setEnabled:YES];
            }
            else
            {
                [Completiondatebtn setEnabled:NO];
                cell.updateCompletionDateTF.userInteractionEnabled=NO;
            }
        }
        else if (indexPath.row == 1) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Remarks" forIndexPath:indexPath];
            
            cell.updateRemaksTV.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            cell.updateRemaksTV.text = textViewText;
            remarkstxtfld=[cell viewWithTag:50];
            if([[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"DependentMemberActionStatus"] isEqualToString:@"NA"]||[[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"DependentMemberActionStatus"] isEqual:[NSNull null]] || [[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"DependentMemberActionStatus"] isEqual:@"Completed"])
            {
                remarkstxtfld.userInteractionEnabled=YES;
            }
            else
            {
                remarkstxtfld.userInteractionEnabled=NO;
            }
        }
        else if (filesArray.count == 0 &&indexPath.row == 2) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Document" forIndexPath:indexPath];
            
            Docname=[cell viewWithTag:20];
            delete=[cell viewWithTag:10];
            uploadbtn=[cell viewWithTag:30];
            DocView=[cell viewWithTag:40];
            if([[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"DependentMemberActionStatus"] isEqualToString:@"NA"]||[[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"DependentMemberActionStatus"] isEqual:[NSNull null]] || [[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"DependentMemberActionStatus"] isEqual:@"Completed"])
            {
                uploadbtn.userInteractionEnabled=YES;
                DocView.hidden = YES;
            }
            else
            {
                uploadbtn.userInteractionEnabled=NO;
                DocView.hidden = YES;
            }
        }
        else if ( filesArray.count != 0 && indexPath.row < 4+filesArray.count) {
            if(indexPath.row==2)
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"File" forIndexPath:indexPath];
                cell.FileDescTV.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                cell.FileDescTV.text = filetxtview;
                
            }
            else if (indexPath.row == 3) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Document" forIndexPath:indexPath];
                
                Docname=[cell viewWithTag:20];
                delete=[cell viewWithTag:10];
                uploadbtn=[cell viewWithTag:30];
                DocView=[cell viewWithTag:40];
                if([[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"DependentMemberActionStatus"] isEqualToString:@"NA"]||[[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"DependentMemberActionStatus"] isEqual:[NSNull null]] || [[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"DependentMemberActionStatus"] isEqual:@"Completed"])
                {
                    uploadbtn.userInteractionEnabled=YES;
                    DocView.hidden = YES;
                }
                else
                {
                    uploadbtn.userInteractionEnabled=NO;
                    DocView.hidden = YES;
                }
            }
            if (indexPath.row >3) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"Files" forIndexPath:indexPath];
                Docname=[cell viewWithTag:20];
                Docname.text = [[filesArray objectAtIndex:indexPath.row-4] objectForKey:@"FileName"];
            }
        }
        
        else {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Button" forIndexPath:indexPath];
            
            depenlbl=[cell viewWithTag:1];
            donebtn=[cell viewWithTag:2];
            NSLog(@"DependentMemberActionStatus-------%@",[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"DependentMemberActionStatus"]);
            if([[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"DependentMemberActionStatus"] isEqualToString:@"NA"]||[[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"DependentMemberActionStatus"] isEqual:[NSNull null]] || [[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"DependentMemberActionStatus"] isEqual:@"Completed"])
            {
                depenlbl.hidden=YES;
                [donebtn setEnabled:YES];
            }
            else
            {
                depenlbl.hidden=NO;
                [donebtn setEnabled:NO];
            }
        }
        
    }
    else{
        static NSString *simpleTableIdentifier;
        
        if (type)  {
            simpleTableIdentifier = @"cell11";
        }
        else{
            simpleTableIdentifier = @"cell1";
        }
        cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (searchEnabled) {
            if (_searchResult.count == 0) {
                UITableViewCell *cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SimpleTableItem"];
                cell1.textLabel.text = @"Actions are not found";
                cell1.backgroundColor = [UIColor clearColor];
                cell1.textLabel.textColor = [UIColor blackColor];
                cell1.userInteractionEnabled = YES;
                return cell1;
            }
            cell.actionListDate.text = [[_searchResult objectAtIndex:indexPath.row] objectForKey:@"StartDate"];
            cell.actionListTargetDate.text = [[_searchResult objectAtIndex:indexPath.row] objectForKey:@"TargetDate"];
            if (type) {
                cell.actionListAssignee.text = [[_searchResult objectAtIndex:indexPath.row] objectForKey:@"Owner"];
            }
            else{
                cell.actionListAssignee.text = [[_searchResult objectAtIndex:indexPath.row] objectForKey:@"Assignee"];
            }
            cell.actionListDescripition.text = [[_searchResult objectAtIndex:indexPath.row] objectForKey:@"ActionDescription"];
            if ([[_searchResult objectAtIndex:indexPath.row] objectForKey:@"EmployeeType"]) {
                cell.actionListEmployeeTypeLabel.text = [[_searchResult objectAtIndex:indexPath.row] objectForKey:@"EmployeeType"];
            }
            
        }
        else{
            cell.actionListDate.text = [[actionProgressDict objectAtIndex:indexPath.row] objectForKey:@"StartDate"];
            cell.actionListTargetDate.text = [[actionProgressDict objectAtIndex:indexPath.row] objectForKey:@"TargetDate"];
            if (type) {
                cell.actionListAssignee.text = [[actionProgressDict objectAtIndex:indexPath.row] objectForKey:@"Owner"];
            }
            else{
                cell.actionListAssignee.text = [[actionProgressDict objectAtIndex:indexPath.row] objectForKey:@"Assignee"];
            }
            cell.actionListDescripition.text = [[actionProgressDict objectAtIndex:indexPath.row] objectForKey:@"ActionDescription"];
            if ([[actionProgressDict objectAtIndex:indexPath.row] objectForKey:@"EmployeeType"]) {
                cell.actionListEmployeeTypeLabel.text = [[actionProgressDict objectAtIndex:indexPath.row] objectForKey:@"EmployeeType"];
            }
        }
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _popOverTableView) {
        if (indexPath.row == 0) {
            return 70;
        }
        if (indexPath.row == 1) {
            return 60;
        }
        if (filesArray.count == 0) {
            popOverTableViewHeight.constant = 230;
            if (indexPath.row == 2) {
                return 30;
            }
            return 70;
        }
        else{
            popOverTableViewHeight.constant = 320;
            
            if (indexPath.row == 2) {
                return 60;
            }
            if (indexPath.row <4+filesArray.count) {
                return 40;
            }
            return 70;
        }
    }
    if (type) {
        return 200;
    }
    return 160;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _dataTableView) {
        if (searchEnabled) {
            sharedManager.passingId = [[_searchResult objectAtIndex:indexPath.row] objectForKey:@"ActionPlannerID"];
        }
        else{
            sharedManager.passingId = [[actionProgressDict objectAtIndex:indexPath.row] objectForKey:@"ActionPlannerID"];
        }
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ActionPlannerDetailViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"ActionPlannerDetailViewController"];
        [self presentViewController:myNavController animated:YES completion:nil];
    }
}
- (IBAction)internalBtn:(id)sender
{
    [self internalBtnClicked];
}
- (IBAction)externalBtn:(id)sender
{
    [self externalBtnClicked];
}
-(void)internalBtnClicked
{
    if (type) {
        type = NO;
        _tittleLabel.text = @"Self Details";
        _internalBtnView.hidden = NO;
        _externalBtnView.hidden = YES;
        [self loadDataFromApi];
        [self searchBarCancelButtonClicked:_searchBar];
    }
}
-(void)externalBtnClicked
{
    if (!type) {
        _tittleLabel.text = @"Others Details";
        _internalBtnView.hidden = YES;
        _externalBtnView.hidden = NO;
        type = YES;
        [self loadDataFromApi];
        [self searchBarCancelButtonClicked:_searchBar];
    }
}
-(void)loadDataFromApi
{
    if([Utitlity isConnectedTointernet]){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [self showProgress];
        int actionType;
        if (type) {
            actionType = 1;
        }
        else{
            actionType = 0;
        }
        //        [actionProgressDict removeAllObjects];
        NSString *targetUrl = [NSString stringWithFormat:@"%@ActionPlannerList?userid=%@&status=%@&emptype=%d", BASEURL,[defaults objectForKey:@"UserID"],actionStatus,actionType];
        NSLog(@"%@",targetUrl);
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPMethod:@"GET"];
        [request setURL:[NSURL URLWithString:targetUrl]];
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
          ^(NSData * _Nullable data,
            NSURLResponse * _Nullable response,
            NSError * _Nullable error) {
              if(error == nil)
              {
                  actionProgressDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                  NSLog(@"%@",actionProgressDict);
                  alertMessage=@"";
                  dispatch_async(dispatch_get_main_queue(), ^{
                      [self hideProgress];
                      if (actionProgressDict.count == 0) {
                          _dataTableView.hidden = YES;
                      }
                      else{
                          _dataTableView.hidden = NO;
                      }
                      
                      [_dataTableView reloadData];
                      [refreshControl endRefreshing];
                  });
              }
              else{
                  [self showMsgAlert:NOInternetMessage];
              }
          }] resume];
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
-(void)showProgress
{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:_dataTableView animated:YES];
}
-(void)hideProgress
{
    [MBProgressHUD hideHUDForView:_dataTableView animated:YES];
}
-(void)showMsgAlert:(NSString *)msg
{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Action planner"
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)searchBtn:(id)sender {
    
    _searchBarView.hidden = NO;
    [_searchBar  becomeFirstResponder];
    
}
- (IBAction)hideSearchBarView:(id)sender {
    [self searchBarCancelButtonClicked:_searchBar];
    [_dataTableView reloadData];
    _searchBarView.hidden = YES;
}
- (void)filterContentForSearchText:(NSString*)searchText
{
    _searchResult = [actionProgressDict filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(ActionDescription contains[c] %@) OR (Owner contains[c] %@)  OR (TargetDate contains[c] %@) OR (EmployeeType contains[c] %@)", searchText, searchText, searchText, searchText]];
    [_dataTableView reloadData];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[searchBar.text stringByTrimmingCharactersInSet: set] length] == 0) {
        searchEnabled = NO;
        [_dataTableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[searchBar.text stringByTrimmingCharactersInSet: set] length] == 0) {
        searchEnabled = NO;
        [self.dataTableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    searchEnabled = NO;
    _searchBarView.hidden = YES;
}
- (IBAction)actionStatusBtn:(id)sender
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select Action Status :" message:nil preferredStyle:UIAlertControllerStyleAlert];
    // create the actions handled by each button
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"In Progress" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        actionStatus = @"i";
        [actionStatusBtn setTitle:@"In Progress" forState:UIControlStateNormal];
        [self loadDataFromApi];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Over Due" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        actionStatus = @"o";
        [actionStatusBtn setTitle:@"Over Due" forState:UIControlStateNormal];
        [self loadDataFromApi];
    }];
    
    //    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"Completed" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    //        actionStatus = @"c";
    //        [actionStatusBtn setTitle:@"Completed" forState:UIControlStateNormal];
    //        [self loadDataFromApi];
    //    }];
    // add actions to our sheet
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    //    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"IsExternal"] isEqualToString:@"Yes"]) {
    //     [actionSheet addAction:action3];
    //    }
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        // Called when user taps outside
    }]];
    // bring up the action sheet
    [self presentViewController:actionSheet animated:YES completion:nil];
}
- (IBAction)editBtn:(id)sender
{
    _popOverView.hidden = NO;
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.dataTableView];
    NSIndexPath *indexPath = [self.dataTableView indexPathForRowAtPoint:buttonPosition];
    _popOverTableView.scrollEnabled=YES;
    editButtonIndex = indexPath.row;
    textViewText = @"";
    filetxtview=@"";
    curDate = nil;
    alertMessage=@"";
    [_popOverTableView reloadData];
}
- (IBAction)popCancelBtn:(id)sender
{
    _popOverView.hidden = YES;
    alertMessage=@"";
    [filesArray removeAllObjects];
}
- (IBAction)popDoneBtn:(id)sender
{
    if([Utitlity isConnectedTointernet]){
        [formatter setDateFormat:@"dd MMM yyyy"];
        NSDate *startDate= [formatter dateFromString:[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"StartDate"]];
        NSLog(@"startDate-------%@",startDate);
        NSDate *endDate;
        NSCalendar *gregorianCalendar;
        NSDateComponents *components;
        if(curDate != nil){
            endDate = curDate;
            gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            components = [gregorianCalendar components:NSCalendarUnitDay
                                              fromDate:startDate
                                                toDate:endDate
                                               options:0];
        }
        
        if(curDate == nil){
            [self showMsgAlert:@"Please select completion date"];
        }
        
        else if (components.day <0 ) {
            [self showMsgAlert:@"Completion Date should be greater then from start date"];
        }
        
        else
        {
            if([Utitlity isConnectedTointernet]){
                [self showProgress];
                [self imageupload];
                [self hideProgress];
            }
            else{
                [self showMsgAlert:NOInternetMessage];
            }
        }
    }
}
- (IBAction)completionDateBtn:(id)sender
{
    
    if(!datePicker1)
        datePicker1 = [THDatePickerViewController datePicker];
    datePicker1.date = [NSDate date];
    datePicker1.delegate = self;
    [datePicker1 setAllowClearDate:NO];
    [datePicker1 setClearAsToday:YES];
    [datePicker1 setAutoCloseOnSelectDate:NO];
    [datePicker1 setAllowSelectionOfSelectedDate:YES];
    [datePicker1 setDisableYearSwitch:YES];
    //[self.datePicker setDisableFutureSelection:NO];
    //    [datePicker1 setDaysInHistorySelection:1];
    //    [datePicker1 setDaysInFutureSelection:0];
    [datePicker1 setSelectedBackgroundColor:[UIColor colorWithRed:125/255.0 green:208/255.0 blue:0/255.0 alpha:1.0]];
    [datePicker1 setCurrentDateColor:[UIColor colorWithRed:242/255.0 green:121/255.0 blue:53/255.0 alpha:1.0]];
    [datePicker1 setCurrentDateColorSelected:[UIColor yellowColor]];
    [datePicker1 setDateHasItemsCallback:^BOOL(NSDate *date) {
        int tmp = (arc4random() % 30)+1;
        return (tmp % 5 == 0);
    }];
    //[self.datePicker slideUpInView:self.view withModalColor:[UIColor lightGrayColor]];
    [self presentSemiViewController:datePicker1 withOptions:@{
                                                              KNSemiModalOptionKeys.pushParentBack    : @(NO),
                                                              KNSemiModalOptionKeys.animationDuration : @(0.2),
                                                              KNSemiModalOptionKeys.shadowOpacity     : @(0.2),
                                                              }];
}

- (void)datePickerDonePressed:(THDatePickerViewController *)datePicker
{
    curDate = datePicker1.date;
    NSIndexPath *ip = [NSIndexPath indexPathForRow:0 inSection:0];
    CustomTableViewCell *cell = [_popOverTableView cellForRowAtIndexPath:ip];
    cell.updateCompletionDateTF.text = [formatter stringFromDate:curDate];
    [cell.updateCompletionDateTF resignFirstResponder];
    [self dismissSemiModalView];
    
}

- (void)datePickerCancelPressed:(THDatePickerViewController *)datePicker
{
    [self dismissSemiModalView];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    NSIndexPath *ip ;
    NSIndexPath *ip1 ;
    ip = [NSIndexPath indexPathForRow:1 inSection:0];
    ip1 = [NSIndexPath indexPathForRow:2 inSection:0];
    CustomTableViewCell *cell = [_popOverTableView cellForRowAtIndexPath:ip];
    CustomTableViewCell *cell1 = [_popOverTableView cellForRowAtIndexPath:ip1]
    ;
    if (textView ==cell.updateRemaksTV) {
        textViewText = newString;
    }
    else if (textView ==cell1.FileDescTV) {
        filetxtview = newString;
    }
    //        [self updateTextLabelsWithText: newString];
    
    return YES;
}

-(CGFloat)heightForText:(NSString*)text withFont:(UIFont *)font andWidth:(CGFloat)width
{
    CGSize constrainedSize = CGSizeMake(width, MAXFLOAT);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:text attributes:attributesDictionary];
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    if (requiredHeight.size.width > width) {
        requiredHeight = CGRectMake(0,0,width, requiredHeight.size.height);
    }
    return requiredHeight.size.height;
}
- (void)textViewDidChange:(UITextView *)textView
{
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    [_popOverTableView beginUpdates];
    textView.frame=newFrame;
    [_popOverTableView endUpdates];
}
-(void)sendMail
{
    if([Utitlity isConnectedTointernet]){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
        [params setObject:[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"ActionAsignee"] forKey:@"Assignee"];
        //        if (type) {
        [params setObject:[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"ActionOwner"] forKey:@"Owner"];
        //        }
        //        else
        //        {
        //            [params setObject:[defaults objectForKey:@"UserID"] forKey:@"Owner"];
        //        }
        [self showProgress];
        NSString* JsonString = [Utitlity JSONStringConv: params];
        NSLog(@"json----%@",JsonString);
        [[WebServices sharedInstance]apiAuthwithJSON:SendActionStatusCompleteMail HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse)
         {
             NSString *error=[json valueForKey:@"error"];
             [self hideProgress];
             if(error.length>0)
             {
                 [self showMsgAlert:[json valueForKey:@"error_description"]];
                 return ;
             }else
             {
                 [self showMsgAlert:@"Updated successfully"];
                 _popOverView.hidden = YES;
             }
         }];
    }
    else
    {
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)popUploadDoc:(UIButton *)sender
{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Take a photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        idPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:idPicker animated:YES completion:nil];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Choose from Libary" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSArray* types = @[(NSString*)kUTTypeImage,(NSString*)kUTTypeSpreadsheet,(NSString*)kUTTypePresentation,(NSString*)kUTTypeDatabase,(NSString*)kUTTypeFolder,(NSString*)kUTTypeZipArchive,(NSString*)kUTTypeVideo,(NSString*)kUTTypePDF,(__bridge NSString* ) kUTTypeContent,(__bridge NSString *) kUTTypeData,(__bridge NSString* ) kUTTypePackage,(__bridge NSString *) kUTTypeDiskImage,(NSString* )kUTTypeCompositeContent,@"com.apple.iwork.pages.pages",@"com.apple.iwork.numbers.numbers",@"com.apple.iwork.keynote.key"];
        UIDocumentPickerViewController *docPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:types inMode:UIDocumentPickerModeImport];
        docPicker.delegate = self;
        [self presentViewController:docPicker animated:YES completion:nil];
        
    }];
    
    
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}
- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url
{
    if (controller.documentPickerMode == UIDocumentPickerModeImport)
    {
        fileData = [NSData dataWithContentsOfURL:url];
        filepath=url;
        alertMessage = [NSString stringWithFormat:@"%@", [url lastPathComponent]];
        NSDictionary *dic1 = @{@"FileName":alertMessage,@"fileData":fileData,@"filepath":filepath};
        [filesArray addObject:dic1];
        [_popOverTableView reloadData];
        _popOverView.hidden = NO;
    }
    else  if (controller.documentPickerMode == UIDocumentPickerModeExportToService)
    {
        // Called when user uploaded the file - Display success alert
        dispatch_async(dispatch_get_main_queue(), ^{
            alertMessage = [NSString stringWithFormat:@"Successfully uploaded file %@", [url lastPathComponent]];
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"UIDocumentView"
                                                  message:alertMessage
                                                  preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertController animated:YES completion:nil];
        });
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        
        UIImage* cameraImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        fileData = UIImageJPEGRepresentation(cameraImage, 0);
        NSURL *path = [info valueForKey:UIImagePickerControllerReferenceURL];
        
        ALAssetsLibrary* assetsLibrary = [[ALAssetsLibrary alloc] init];
        
        [assetsLibrary writeImageToSavedPhotosAlbum:cameraImage.CGImage
                                           metadata:[info objectForKey:UIImagePickerControllerMediaMetadata]
                                    completionBlock:^(NSURL *assetURL, NSError *error) {
                                        
                                        if (!error) {
                                            
                                            NSLog(@"checkkk %@",[[NSString stringWithFormat:@"%@",assetURL] substringWithRange:NSMakeRange(77,3)]);
                                            NSLog(@"File======%@",assetURL.path);
                                            
                                            NSURL *Checkurl = [NSURL URLWithString:[[NSString stringWithFormat:@"file:///private/var/mobile/Containers/Data/Application/04E12545-4E21-47C3-ABE1-4CFE577CE6F1/tmp/%@.jpeg",[[NSString stringWithFormat:@"%@",assetURL] substringWithRange:NSMakeRange(36, 36)]]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                                            NSLog(@"assetURL---------%@",assetURL);
                                            NSLog(@"assetURL test-------------%@",Checkurl);
                                            
                                            
                                            filepath=assetURL;
                                            alertMessage = [NSString stringWithFormat:@"%@", [Checkurl lastPathComponent]];
                                            NSDictionary *dic1 = @{@"FileName":alertMessage,@"fileData":fileData,@"filepath":filepath};
                                            [filesArray addObject:dic1];
                                            
                                            [_popOverTableView reloadData];
                                        }
                                    }];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)popDeleteDoc:(UIButton *)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure do you want to delete the file" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                         {
                             
                             NSIndexPath *indexPath = [_popOverTableView indexPathForCell:(UITableViewCell *)sender.superview.superview];
                             [filesArray removeObjectAtIndex:indexPath.row];
                             [_popOverTableView reloadData];
                         }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)imageupload
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[actionProgressDict objectAtIndex:editButtonIndex] objectForKey:@"ActionPlannerID"] forKey:@"ActionplannerID"];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [params setObject:[formatter stringFromDate:curDate] forKey:@"CompletionDate"];
    [params setObject:textViewText forKey:@"Remark"];
    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"] forKey:@"CreatedBy"];
    [params setObject:@"INS" forKey:@"ActionMode"];
    [params setObject:filetxtview forKey:@"FileDescription"];
    
    
    NSString* JsonString = [Utitlity JSONStringConv: params];
    NSLog(@"dict-------%@",JsonString); 
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"contentType"];
    AFHTTPRequestOperation *op =[manager POST:[NSString stringWithFormat:@"%@POSTUPDActionProgressAsMultipart",BASEURL] parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        for (int i = 0; i<filesArray.count; i++) {
            [formData appendPartWithFileData:[[filesArray objectAtIndex:i] objectForKey:@"fileData"] name:@"files" fileName:[[filesArray objectAtIndex:i] objectForKey:@"FileName"] mimeType:[[[filesArray objectAtIndex:i] objectForKey:@"FileName"] pathExtension]];
        }
        
    }
                                      success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {
                                          NSLog(@"test----%@",[NSString stringWithFormat:@"%@PostUpdateActionProgressAsMultipart",BASEURL]);
                                          NSLog(@"%@",responseObject);
                                          [filesArray removeAllObjects];
                                          [self loadDataFromApi];
                                          [self sendMail];
                                      } failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
                                          NSString *ErrorResponse = [[NSString alloc] initWithData:(NSData *)error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
                                          
                                          NSData *data1 = [ErrorResponse dataUsingEncoding:NSUTF8StringEncoding];
                                          id json = [NSJSONSerialization JSONObjectWithData:data1 options:0 error:nil];
                                          
                                          NSLog(@"%@",json);
                                      }];
    [op start];
}

@end

