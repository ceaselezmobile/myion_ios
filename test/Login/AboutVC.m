

#import "AboutVC.h"
#import "MBProgressHUD.h"

@interface AboutVC ()

@end

@implementation AboutVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *htmlFile;
    self.headertxtlbl.text=self.str;
    if([self.str isEqualToString:@"About Us"])
    {
        htmlFile = [[NSBundle mainBundle] pathForResource:@"about" ofType:@"html"];
    }
    else if ([self.str isEqualToString:@"Privacy Policy"])
    {
        htmlFile = [[NSBundle mainBundle] pathForResource:@"PrivacyandPolicy" ofType:@"html"];
    }
    else if ([self.str isEqualToString:@"Terms & Conditions"])
    {
        htmlFile = [[NSBundle mainBundle] pathForResource:@"TandC" ofType:@"html"];
    }
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [self showProgress];
    [_webview loadHTMLString:htmlString baseURL: [[NSBundle mainBundle] bundleURL]];

    
}
-(void)viewWillAppear:(BOOL)animated
{
//    [self hideProgress];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
    [self hideProgress];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)backbtn:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)showProgress
{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:_webview animated:YES];
}

-(void)hideProgress
{
    [MBProgressHUD hideHUDForView:_webview animated:YES];
}
@end
