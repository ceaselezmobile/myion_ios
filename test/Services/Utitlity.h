
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@protocol alertViewBtnHandle <NSObject>
@required
-(void)alertbtn: (NSString*)btnHeading;
@end

@interface Utitlity : NSObject
typedef void(^AlertCallBack)(BOOL onClickOk);
+(Utitlity *)sharedInstance;
+ (BOOL)isConnectedTointernet;

- (UIAlertController*)dynamicAlert:(NSString*)title :(NSString*) message :(NSArray*)dynamicBtn;
@property (nonatomic, weak)id<alertViewBtnHandle> delegate;
+ (NSString *)JSONStringConv:(NSDictionary *)dict;
+ (NSString *)timeandDateConv_timeZone:(NSString *)givenFormat input:(NSString *)inputValue reqFormat:(NSString *)reqFormat;
+ (NSString *)deviceIPAddress;
//+ (BOOL)isValidEmail:(NSString*)checkString;
+ (NSString *)nullstring:(NSString *)nullstring;
+ (BOOL)isStringNumeric:(NSString *)text;
+ (BOOL) validatePanCardNumber: (NSString *) cardNumber;
+ (BOOL)validatePhone:(NSString *)phoneNumber;
+ (NSString*)currencyFormate:(int)amount;

-(void) showAlertViewWithMessage:(NSString *)message withTitle:(NSString *)title forController:(UIViewController *)controller withCallback:(AlertCallBack)callBack ;
-(void) displayAlertViewWithMessage:(NSString *)message withTitle:(NSString *)title forController:(UIViewController *)controller withCallback:(AlertCallBack)callBack ;


@end
