//
//  MeetingDetailViewController.m
//  test
//
//  Created by ceaselez on 11/01/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "MeetingDetailViewController.h"
#import "ConductMeetingTableViewCell.h"
#import "AddDetailsViewController.h"
#import "MySharedManager.h"

@interface MeetingDetailViewController ()
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;


@end

@implementation MeetingDetailViewController
{
    NSArray *subTitleArray;
    NSArray *contentArray;
    NSDateFormatter *dateFormatter;
    NSDate *startDate;
    NSDate *endDate;
    NSDateFormatter *timeFormat;
    UIDatePicker *startTimePicker;
    UIDatePicker *endTimePicker;
    NSString *mode;
//    NSTimer *timer;
//    NSTimer *timer1;
    MySharedManager *sharedManager;

    long currMinute;
    long currSeconds;
    long currHours;
    BOOL decrement;
    BOOL mins;
    BOOL realTimeBack;
    NSString *MeetingStartTime;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    sharedManager = [MySharedManager sharedManager];

    dateFormatter=[[NSDateFormatter alloc] init];
    if(sharedManager.realTime){
        mode = @"online";
          }
    else{
        mode = @"Offline";
    }
    NSLog(@"%@",_meetingDict);
    subTitleArray = [NSArray arrayWithObjects:@"Date :",@"Meeting Description :",@"From Time :",@"To Time :",@"Place :",@"Time Zone :",@"Duration (Mins) :", nil];
    [dateFormatter setDateFormat:@"mm/dd/yyyy hh:mm:ss a"];
    startDate =[dateFormatter dateFromString:[_meetingDict objectForKey:@"FromTime"]];
    endDate =[dateFormatter dateFromString:[_meetingDict objectForKey:@"ToTime"]];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSTimeInterval diff = [endDate timeIntervalSinceDate:startDate];
    int difference = diff/60;
    contentArray = [NSArray arrayWithObjects:[_meetingDict objectForKey:@"MeetingDate"],[_meetingDict objectForKey:@"MeetingDescription"],[dateFormatter stringFromDate:startDate],[dateFormatter stringFromDate:endDate],[_meetingDict objectForKey:@"MeetingPlace"],[_meetingDict objectForKey:@"TimeZone"],[NSString stringWithFormat:@"%d Mins",difference] ,nil];
    
    timeFormat= [[NSDateFormatter alloc]init];
    [timeFormat setDateFormat:@"hh:mm a"];
    startTimePicker=[[UIDatePicker alloc]init];
    startTimePicker.datePickerMode=UIDatePickerModeTime;
    endTimePicker=[[UIDatePicker alloc]init];
    endTimePicker.datePickerMode=UIDatePickerModeTime;
    realTimeBack = NO;
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];

}
-(void)viewWillAppear:(BOOL)animated{
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        sharedManager.slideMenuSlected = @"no";
    }
    else{
        realTimeBack = YES;
    }

    if (sharedManager.realTime) {
        decrement = sharedManager.decrement;
        currHours = sharedManager.currHour;
        currSeconds = sharedManager.currSecs;
        currMinute =  sharedManager.currmins ;
    }
    else{
//        timeLabel.hidden = YES;
//        _timeLabelConstrain.constant = 20;
    }
    [_dataTableView reloadData];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([mode isEqualToString:@"online"]) {
        return 8;

    }
       return 10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ConductMeetingTableViewCell *cell;
    if (indexPath.row == 0) {
    cell= [tableView dequeueReusableCellWithIdentifier:@"start" forIndexPath:indexPath];
        NSDate *now = [NSDate date];
        static NSDateFormatter *dateFormatter;
        if (!dateFormatter) {
            dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"E MMM dd yyyy hh:mm:ss a";
        }
        if (realTimeBack) {
            cell.stratBtnHeightContraint.constant = 30;
            cell.startBtn.layer.cornerRadius = 0;
            [cell.startBtn setTitle:@"Next" forState:UIControlStateNormal];

        }
        if ([mode isEqualToString:@"online"]) {
            if (realTimeBack) {
                cell.timeLable.text = [NSString stringWithFormat:@" %@ \nGMT+05:30(India Standard Time) %@",[dateFormatter stringFromDate:now],[NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds]];
            }
            else{
                cell.timeLable.text = [NSString stringWithFormat:@" %@ \nGMT+05:30(India Standard Time)",[dateFormatter stringFromDate:now]];
                
            }        }
        else{
            cell.timeLable.hidden= YES;
        }
    }
    else{
    if ([mode isEqualToString:@"online"]) {
        cell= [tableView dequeueReusableCellWithIdentifier:@"content" forIndexPath:indexPath];
        cell.detailSubtitleLabel.text = [subTitleArray objectAtIndex:indexPath.row - 1];
        cell.detailDetailsLabel.text = [contentArray objectAtIndex:indexPath.row - 1];
    }
    else{
    if (indexPath.row  == 1) {
        cell= [tableView dequeueReusableCellWithIdentifier:@"timeContent" forIndexPath:indexPath];
        cell.detailTimeSubtitleLabel.text  = @"Actual Start Time :";
        cell.detailTimeTF.text = [dateFormatter stringFromDate:startDate];
        cell.detailTimeTF.inputView = startTimePicker;
        UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        [toolBar setTintColor:[UIColor grayColor]];
//        startTimePicker.maximumDate = endDate;
        UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(startTimeDone)];
        UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
        [cell.detailTimeTF setInputAccessoryView:toolBar];

    }
    else if (indexPath.row  == 2) {
        cell= [tableView dequeueReusableCellWithIdentifier:@"timeContent" forIndexPath:indexPath];
        cell.detailTimeSubtitleLabel.text  = @"Actual End Time :";
        cell.detailTimeTF.text = [dateFormatter stringFromDate:endDate];
        cell.detailTimeTF.inputView = endTimePicker;
        UIToolbar *toolBar=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        [toolBar setTintColor:[UIColor grayColor]];
        endTimePicker.minimumDate = startDate;
        UIBarButtonItem *doneBtn=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(endTimeDone)];
        UIBarButtonItem *space=[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        [toolBar setItems:[NSArray arrayWithObjects:space,doneBtn, nil]];
        [cell.detailTimeTF setInputAccessoryView:toolBar];
    }
    else{
    cell= [tableView dequeueReusableCellWithIdentifier:@"content" forIndexPath:indexPath];
    cell.detailSubtitleLabel.text = [subTitleArray objectAtIndex:indexPath.row - 3];
        cell.detailDetailsLabel.text = [contentArray objectAtIndex:indexPath.row - 3];
    }
    }
    }
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0){
        if ([mode isEqualToString:@"online"]) {
            if (realTimeBack) {
                return 150;
            }
            return 150;
        }
        else{
            if (realTimeBack) {
                return 60;
            }
            return 110;
        }
    }
    return 44;
}
- (void)startTimeDone
{
    startDate = startTimePicker.date;
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    [_dataTableView reloadData];
}
- (void)endTimeDone
{
    endDate = endTimePicker.date;
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    [_dataTableView reloadData];
}
- (IBAction)startBtn:(id)sender {
   
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"mm/dd/yyyy hh:mm:ss a"];
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AddDetailsViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"AddDetailsViewController"];
    NSTimeInterval diff = [endDate timeIntervalSinceDate:startDate];
    int difference = diff/60;

    if (realTimeBack) {
        [dateFormatter setDateFormat:@"hh:mm a"];
        NSString *MeetingDescription = [[_meetingDict objectForKey:@"MeetingDescription"] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        myNavController.meetingDetails = @{@"meetingID" : [_meetingDict objectForKey:@"MeetingTypeID"] , @"meetingDate" : [_meetingDict objectForKey:@"MeetingDate"] , @"meetingActualTime" : [NSString stringWithFormat:@"%@ to %@ (%d Mins)",[dateFormatter stringFromDate:startDate],[dateFormatter stringFromDate:endDate],difference] ,@"ActualStartTime" : [dateFormatter stringFromDate:startDate],@"ActualEndTime" : [dateFormatter stringFromDate:endDate],@"Duration" : [NSString stringWithFormat:@"%d",difference],@"Mode" : mode ,@"PDFName" :MeetingDescription,@"MinutesTakenBy" : [_meetingDict objectForKey:@"MeetingDate"],@"MeetingStartTime" : MeetingStartTime};
        NSLog(@"myNavController.meetingActualTime------------%@",myNavController.meetingDetails);
        sharedManager.decrement = decrement;
        sharedManager.currmins = currMinute;
        sharedManager.currHour = currHours;
        sharedManager.currSecs = currSeconds;

        [self presentViewController:myNavController animated:YES completion:nil];
    }
    else{
        UIAlertController *alert= [UIAlertController alertControllerWithTitle:nil message:@"Are you sure ,You want start?" preferredStyle:UIAlertControllerStyleAlert];
    
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                 [dateFormatter setDateFormat:@"hh:mm a"];
                                 MeetingStartTime = [dateFormatter stringFromDate:[NSDate date]];
                                 NSString *MeetingDescription = [[_meetingDict objectForKey:@"MeetingDescription"] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                                 myNavController.meetingDetails = @{@"meetingID" : [_meetingDict objectForKey:@"MeetingTypeID"] , @"meetingDate" : [_meetingDict objectForKey:@"MeetingDate"] , @"meetingActualTime" : [NSString stringWithFormat:@"%@ to %@ (%d Mins)",[dateFormatter stringFromDate:startDate],[dateFormatter stringFromDate:endDate],difference] ,@"ActualStartTime" : [dateFormatter stringFromDate:startDate],@"ActualEndTime" : [dateFormatter stringFromDate:endDate],@"Duration" : [NSString stringWithFormat:@"%d",difference],@"Mode" : mode ,@"PDFName" :MeetingDescription,@"MinutesTakenBy" : [_meetingDict objectForKey:@"MeetingDate"],@"MeetingStartTime" : MeetingStartTime};
                                 NSLog(@"myNavController.meetingActualTime------------%@",myNavController.meetingDetails);
                                 sharedManager.currmins = 00;
                                 sharedManager.currHour = 00;
                                 sharedManager.currSecs = 00;

                                 [self presentViewController:myNavController animated:YES completion:nil];
                             }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        [alert addAction:ok];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
    }
        
}
- (IBAction)backBtn:(id)sender {
    UIAlertController *alert= [UIAlertController alertControllerWithTitle:nil message:@"Do you want to exist meeting?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                         {
                             [self dismissViewControllerAnimated:YES completion:nil];
                         }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:ok];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)timerTick:(NSTimer *)timer {
    NSDate *now = [NSDate date];
    
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"E MMM dd yyyy hh:mm:ss a";  // very simple format  "8:47:22 AM"
    }
    NSString *countDownTimer ;
    if (decrement) {
        countDownTimer =  [self  timerFired];
    }
    else{
        countDownTimer = [self timerincrementer];
    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    ConductMeetingTableViewCell *cell = (ConductMeetingTableViewCell *)[self.dataTableView cellForRowAtIndexPath:indexPath];
    if (realTimeBack) {
        cell.timeLable.text = [NSString stringWithFormat:@" %@ \nGMT+05:30(India Standard Time) %@",[dateFormatter stringFromDate:now],countDownTimer];
    }
    else{
        cell.timeLable.text = [NSString stringWithFormat:@" %@ \nGMT+05:30(India Standard Time)",[dateFormatter stringFromDate:now]];

    }
    
}
-(NSString*)timerFired
{
    if(currMinute == 0 && currSeconds == 0 && currHours == 0)
    {
        decrement = NO;
    }
    else{
        decrement = YES;

        if(currMinute == 0 && currHours>0 && currSeconds==0)
        {
            currHours-=1;
            currMinute=59;
            mins = NO;
        }
        else if(currSeconds==0 && currMinute>0 )
        {
            if (mins) {
                currMinute-=1;
            }
            currSeconds=59;
        }
        else if(currSeconds>0 )
        {
            currSeconds-=1;
            mins = YES;
        }
    }
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds];
}
-(NSString*)timerincrementer
{
    if(currSeconds<59)
    {
        currSeconds+=1;
    }
    else if(currSeconds == 59 && currMinute < 59)
    {
        currMinute+=1;
        currSeconds=0;
    }
    else  if(currMinute == 59)
    {
        currHours+=1;
        currMinute=0;
        currSeconds=0;
    }
    
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds];
}
@end
