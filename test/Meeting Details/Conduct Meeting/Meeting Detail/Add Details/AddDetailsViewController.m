

#import "AddDetailsViewController.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "MeetingDetailsTableViewCell.h"
#import "THDatePickerViewController.h"
#import "SearchViewController.h"
#import "MySharedManager.h"
#import "AddActionViewController.h"
#import "AgendaDescriptionViewController.h"

@interface AddDetailsViewController ()<UITextFieldDelegate,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *tittleLabel;
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeLabelConstraint;

@end

@implementation AddDetailsViewController
{
    __weak IBOutlet NSLayoutConstraint *memberTableViewHeight;
    __weak IBOutlet NSLayoutConstraint *agendeTableViewHeight;
    __weak IBOutlet UITableView *agendaTableView;
    __weak IBOutlet UITableView *membersPopoverTableView;
    __weak IBOutlet UIView *popOverView;
    int meetingDetailsInt;
    __weak IBOutlet UIButton *addBtn;
    NSInteger timeDifference;
    NSString *textViewText;
    MySharedManager *sharedManager;
    NSString *StakeholderCategory;
    NSString *Company;
    NSString *AssignedTo;
    NSString *Role;
    NSString *textViewTextAgenda;
    NSMutableArray *membersArray;
    NSMutableArray *agendaArray;
    NSArray *agendaDiscussionArray;
    NSString *catogeryString;
    NSString *comanyString;
    NSString *memberString;
    NSString *roleString;
    NSInteger oldDataIndex;
    BOOL companyIDBool;
    BOOL categoryIDBool;
    BOOL roleNeed;
    BOOL olddata;
    BOOL updatemember;
    int presence;
    int agendaDiscussionIndex;
    int stakeHType;
    
    NSTimer *timer;
    long currMinute;
    long currSeconds;
    long currHours;
    BOOL decrement;
    BOOL mins;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    sharedManager = [MySharedManager sharedManager];
    _dataTableView.estimatedRowHeight=44;
    agendaTableView.estimatedRowHeight=224;
    _dataTableView.rowHeight = UITableViewAutomaticDimension;
    agendaTableView.rowHeight = UITableViewAutomaticDimension;
    membersArray = [[NSMutableArray alloc] init];
    agendaArray = [[NSMutableArray alloc] init];
    
    meetingDetailsInt = 1;
    popOverView.hidden = YES;
    agendaDiscussionIndex = 0;
    _tittleLabel.text = @"Members";
    [self loadMemberDataFromApi];
    if (sharedManager.realTime) {
        if (sharedManager.currHour == 00 && sharedManager.currmins == 00 && sharedManager.currSecs == 00) {
            NSDateFormatter *dateFormatEnd = [[NSDateFormatter alloc] init];
            [dateFormatEnd setDateFormat:@"hh:mm a"];
            NSDate *endTime = [dateFormatEnd dateFromString:[_meetingDetails objectForKey:@"ActualEndTime"]];
            //        NSDate *date = [NSDate date];
            //        NSString *timeString = [dateFormatEnd stringFromDate:date];
            NSDate *realTime =[dateFormatEnd dateFromString:[_meetingDetails objectForKey:@"ActualStartTime"]];
            NSLog(@"realTime-----%@endTime------%@,",realTime,endTime);
            NSDateComponents *components;
            components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate: realTime toDate: endTime options: 0];
            currHours = [components hour];
            if ([components minute] != 0) {
                currMinute=[components minute]-1;
            }
            else{
                currMinute=[components minute];
            }
            currSeconds=00;
            decrement = YES;
        }
        else{
            decrement = sharedManager.decrement;
            currHours = sharedManager.currHour;
            currMinute=sharedManager.currmins;
            currSeconds=sharedManager.currSecs;
        }
        NSString *countDownTimer=[NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds];
        NSDate *now = [NSDate date];
        
        static NSDateFormatter *dateFormatter;
        if (!dateFormatter) {
            dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"E MMM dd yyyy hh:mm:ss a";  // very simple format  "8:47:22 AM"
        }
        _timeLabel.text = [NSString stringWithFormat:@" %@ \nGMT+05:30(India Standard Time) %@",[dateFormatter stringFromDate:now],countDownTimer];
        
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
        
    }
    else{
        _timeLabel.hidden = YES;
        _timeLabelConstraint.constant = 0;
    }
    
    
}
-(void) viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self fillTheTF];
    [_dataTableView reloadData];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == _dataTableView) {
        if(meetingDetailsInt == 1){
            return membersArray.count;
        }
        else if(meetingDetailsInt == 2){
            if (agendaArray.count == 0) {
                return  1;
            }
            return agendaArray.count;
        }
        else if(meetingDetailsInt == 3){
            return  3;
        }
    }
    else if (tableView == membersPopoverTableView){
        if(membersArray.count != 0 && oldDataIndex != 5000){
            if ( [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"update"]  ){
                memberTableViewHeight.constant = 5 *60;
                return 5;
            }
        }
        else if (roleNeed) {
            if (stakeHType == 1) {
                memberTableViewHeight.constant = 4 *60;
                return 4;
            }
            else
            {
                memberTableViewHeight.constant = 6 *60;
                return 6;
            }
        }
       
        else{
            if (stakeHType == 1) {
                memberTableViewHeight.constant = 3 *60;
                return 3;
            }
            else
            {
                memberTableViewHeight.constant = 5 *60;
                return 5;
            }
        }
    }
    else if (tableView == agendaTableView){
        return 3;
    }
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    MeetingDetailsTableViewCell *cell;
    if (meetingDetailsInt == 1 || meetingDetailsInt == 2 ){
        cell = [tableView dequeueReusableCellWithIdentifier:@"Header"];
    }
    else if (meetingDetailsInt == 3){
        cell = [tableView dequeueReusableCellWithIdentifier:@"HeaderAction"];
    }
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == _dataTableView) {
        if (meetingDetailsInt == 1 || meetingDetailsInt == 2 ) {
            addBtn.hidden = NO;
            return 0;
        }
        addBtn.hidden = YES;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MeetingDetailsTableViewCell *cell ;
    if (tableView == _dataTableView) {
        if (meetingDetailsInt == 1){
            cell = [tableView dequeueReusableCellWithIdentifier:@"membersCell" forIndexPath:indexPath];
            cell.membersNameLabel.text = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"name"];
            cell.membersRoleLabel.text = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"role"];
            if ([[[membersArray objectAtIndex:indexPath.row] objectForKey:@"Presence"] intValue] == 1) {
                cell.presenceLabel.text = @"Present";
            }
            else
                cell.presenceLabel.text = @"Apologies";
        }
        else if(meetingDetailsInt == 2){
            if (agendaArray.count == 0) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"no data" forIndexPath:indexPath];
            }
            else{
                cell = [tableView dequeueReusableCellWithIdentifier:@"agendaCell" forIndexPath:indexPath];
                if ([[agendaArray objectAtIndex:indexPath.row] objectForKey:@"descripition"] != [NSNull null]) {
                    cell.agendaDescriptionLabel.text = [self convertHTML:[[agendaArray objectAtIndex:indexPath.row] objectForKey:@"descripition"]];
                }
                cell.agendaRoleLabel.text =[NSString stringWithFormat:@"%@ (Mins)",[[agendaArray objectAtIndex:indexPath.row] objectForKey:@"duration"]];
            }
        }
        else if(meetingDetailsInt == 3){
            if (indexPath.row == 0) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"title" forIndexPath:indexPath];
                if ([[agendaDiscussionArray objectAtIndex:agendaDiscussionIndex] objectForKey:@"DiscussionTitle"]) {
                    cell.discussionTitleLabel.numberOfLines=0;
                    cell.discussionTitleLabel.lineBreakMode=NSLineBreakByWordWrapping;
                    cell.discussionTitleLabel.text = [self convertHTML:[[agendaDiscussionArray objectAtIndex:agendaDiscussionIndex] objectForKey:@"DiscussionTitle"]];
                    
                }
            }
            if (indexPath.row == 1) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"description" forIndexPath:indexPath];
                if (textViewTextAgenda) {
                    if([[[agendaDiscussionArray objectAtIndex:agendaDiscussionIndex] objectForKey:@"DiscussionDescription"] isEqual:[NSNull null]]||[[[agendaDiscussionArray objectAtIndex:agendaDiscussionIndex] objectForKey:@"DiscussionDescription"] isEqualToString:@""])
                    {
                        cell.discussionDescriptionTV.text=@"";
                    }
                    else
                    {
                        cell.discussionDescriptionTV.text = [[agendaDiscussionArray objectAtIndex:agendaDiscussionIndex] objectForKey:@"DiscussionDescription"];
                    }
                }
            }
            if (indexPath.row == 2) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"buttons" forIndexPath:indexPath];
            }
        }
    }
    if (tableView == membersPopoverTableView) {
        if(membersArray.count != 0 && oldDataIndex != 5000 ){
            if ( [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"update"]  )
            {
                updatemember = YES;
                if (indexPath.row == 0) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Members" forIndexPath:indexPath];
                    cell.memberTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.userInteractionEnabled = NO;
                    cell.memberTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"name"];
                }
                if (indexPath.row == 1) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Unit" forIndexPath:indexPath];
                    cell.unitTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.unitTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"ALevelDescription"];
                    cell.userInteractionEnabled = NO;
                }
                if (indexPath.row == 2) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Role" forIndexPath:indexPath];
                    cell.roleTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.roleTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"role"];
                    cell.userInteractionEnabled = NO;
                }
                if (indexPath.row == 3) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Presence" forIndexPath:indexPath];
                    cell.presenceTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    NSLog(@"Presence----------%@",[[membersArray objectAtIndex:oldDataIndex] objectForKey:@"Presence"]);
                    if ( presence == 1) {
                        cell.presenceTF.text = @"Present";
                    }
                    else{
                        cell.presenceTF.text = @"Apologies";
                    }
                }
                if (indexPath.row == 4) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Buttons" forIndexPath:indexPath];
                    if (oldDataIndex == 5000) {
                        [cell.memberDoneBtn setTitle:@"Done" forState:UIControlStateNormal];
                        cell.deleteMemberBtn.hidden = YES;
                    }
                    else{
                        [cell.memberDoneBtn setTitle:@"Update" forState:UIControlStateNormal];
                        cell.deleteMemberBtn.hidden = YES;
                    }
                }
            }
        }
        else if (roleNeed) {
            if (stakeHType == 1) {
                if (indexPath.row == 0) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"stackholder" forIndexPath:indexPath];
                    cell.stackHolderTypeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    if (olddata) {
                        if (stakeHType == 1) {
                            cell.stackHolderTypeTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"category"];
                        }
                        else{
                            cell.stackHolderTypeTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"category"];
                        }
                    }
                    else {
                        cell.stackHolderTypeTF.text = @"Internal StakeHolders";
                    }
                }
                if (indexPath.row == 1) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Members" forIndexPath:indexPath];
                    cell.memberTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.userInteractionEnabled = YES;
                    if (olddata) {
                        cell.memberTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"name"];
                    }
                    else{
                        cell.memberTF.text = memberString;
                    }
                }
                if (indexPath.row == 2) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Role" forIndexPath:indexPath];
                    cell.roleTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.userInteractionEnabled = YES;
                    if (olddata) {
                        cell.roleTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"role"];
                    }
                    else{
                        cell.roleTF.text = roleString;
                    }
                }
                if (indexPath.row == 3) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Buttons" forIndexPath:indexPath];
                    if (oldDataIndex == 5000) {
                        [cell.memberDoneBtn setTitle:@"Done" forState:UIControlStateNormal];
                        cell.deleteMemberBtn.hidden = YES;
                    }
                    else{
                        [cell.memberDoneBtn setTitle:@"Update" forState:UIControlStateNormal];
                        cell.deleteMemberBtn.hidden = YES;
                    }
                }
            }
            else{
                if (indexPath.row == 0) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"stackholder" forIndexPath:indexPath];
                    cell.stackHolderTypeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    if (olddata) {
                        //                    cell.stackHolderTypeTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"category"];
                    }
                    else {
                        cell.stackHolderTypeTF.text = @"External StakeHolders";
                    }
                }
                if (indexPath.row == 1) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Category" forIndexPath:indexPath];
                    cell.categoryTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    if (olddata) {
                        cell.categoryTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"category"];
                    }
                    else {
                        cell.categoryTF.text = catogeryString;
                    }
                }
                if (indexPath.row == 2) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Company" forIndexPath:indexPath];
                    cell.companyTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    if (olddata) {
                        cell.companyTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"company"];
                    }
                    else {
                        cell.companyTF.text = comanyString;
                    }
                }
                if (indexPath.row == 3) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Members" forIndexPath:indexPath];
                    cell.memberTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.userInteractionEnabled = YES;
                    if (olddata) {
                        cell.memberTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"name"];
                    }
                    else{
                        cell.memberTF.text = memberString;
                    }
                }
                if (indexPath.row == 4) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Role" forIndexPath:indexPath];
                    cell.roleTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.userInteractionEnabled = YES;
                    if (olddata) {
                        cell.roleTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"role"];
                    }
                    else{
                        cell.roleTF.text = roleString;
                    }
                }
                if (indexPath.row == 5) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Buttons" forIndexPath:indexPath];
                    if (oldDataIndex == 5000) {
                        [cell.memberDoneBtn setTitle:@"Done" forState:UIControlStateNormal];
                        cell.deleteMemberBtn.hidden = YES;
                    }
                    else{
                        [cell.memberDoneBtn setTitle:@"Update" forState:UIControlStateNormal];
                        cell.deleteMemberBtn.hidden = YES;
                    }
                }
            }
        }
        else{
            if (stakeHType == 1) {
                if (indexPath.row == 0) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"stackholder" forIndexPath:indexPath];
                    cell.stackHolderTypeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    if (olddata) {
                        //                    cell.stackHolderTypeTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"category"];
                    }
                    else {
                        cell.stackHolderTypeTF.text = @"Interenal StakeHolders";
                    }
                }
                
                if (indexPath.row == 1) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Members" forIndexPath:indexPath];
                    cell.memberTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.userInteractionEnabled = YES;
                    if (olddata) {
                        cell.memberTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"name"];
                    }
                    else{
                        cell.memberTF.text = memberString;
                    }
                }
                if (indexPath.row == 2) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Buttons" forIndexPath:indexPath];
                    if (oldDataIndex == 5000) {
                        [cell.memberDoneBtn setTitle:@"Done" forState:UIControlStateNormal];
                        cell.deleteMemberBtn.hidden = YES;
                    }
                    else {
                        [cell.memberDoneBtn setTitle:@"Update" forState:UIControlStateNormal];
                        cell.deleteMemberBtn.hidden = YES;
                    }
                }
            }
            else{
                if (indexPath.row == 0){
                    cell = [tableView dequeueReusableCellWithIdentifier:@"stackholder" forIndexPath:indexPath];
                    cell.stackHolderTypeTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    if (olddata) {
                        //                    cell.stackHolderTypeTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"category"];
                    }
                    else {
                        cell.stackHolderTypeTF.text = @"External StakeHolders";
                    }
                }
                if (indexPath.row == 1) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Category" forIndexPath:indexPath];
                    cell.categoryTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    if (olddata) {
                        cell.categoryTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"category"];
                    }
                    else{
                        cell.categoryTF.text = catogeryString;
                    }
                }
                if (indexPath.row == 2) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Company" forIndexPath:indexPath];
                    cell.companyTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    if (olddata) {
                        cell.companyTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"company"];
                    }
                    else{
                        cell.companyTF.text = comanyString;
                    }
                }
                if (indexPath.row == 3) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Members" forIndexPath:indexPath];
                    cell.memberTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
                    cell.userInteractionEnabled = YES;
                    if (olddata) {
                        cell.memberTF.text = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"name"];
                    }
                    else{
                        cell.memberTF.text = memberString;
                    }
                }
                if (indexPath.row == 4) {
                    cell = [tableView dequeueReusableCellWithIdentifier:@"Buttons" forIndexPath:indexPath];
                    if (oldDataIndex == 5000) {
                        [cell.memberDoneBtn setTitle:@"Done" forState:UIControlStateNormal];
                        cell.deleteMemberBtn.hidden = YES;
                    }
                    else {
                        [cell.memberDoneBtn setTitle:@"Update" forState:UIControlStateNormal];
                        cell.deleteMemberBtn.hidden = YES;
                    }
                }
            }
        }
    }
    
    if (tableView == agendaTableView) {
        if (indexPath.row == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Description" forIndexPath:indexPath];
            cell.descriptionTV.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            if (olddata) {
                if ([[agendaArray objectAtIndex:oldDataIndex] objectForKey:@"descripition"] != [NSNull null]) {
                    textViewTextAgenda = [self convertHTML:[[agendaArray objectAtIndex:oldDataIndex] objectForKey:@"descripition"]];
                    //                    cell.descriptionTV.scrollEnabled=NO;
                }
            }
            else{
                textViewTextAgenda = @"";
            }
            [cell.descriptionTV setText:textViewTextAgenda];
        }
        if (indexPath.row == 1) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Duration" forIndexPath:indexPath];
            cell.durationTF.layer.sublayerTransform = CATransform3DMakeTranslation(10.0f, 0.0f, 0.0f);
            
            //            cell.timeGapLabel.text = [NSString stringWithFormat:@"Remaining (%ld mins)",timeDifference-(long)sum];
            if (olddata) {
                cell.durationTF.text = [NSString stringWithFormat:@"%@",[[agendaArray objectAtIndex:oldDataIndex] objectForKey:@"duration"]];
            }
            else{
                cell.durationTF.text = @"";
            }
        }
        if (indexPath.row == 2) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Button" forIndexPath:indexPath];
            if (oldDataIndex == 5000) {
                [cell.agendaDoneBtn setTitle:@"Done" forState:UIControlStateNormal];
                cell.deleteAgendaBtn.hidden = YES;
            }
            else{
                [cell.agendaDoneBtn setTitle:@"Update" forState:UIControlStateNormal];
                cell.deleteAgendaBtn.hidden = YES;
            }
        }
    }
    return cell;
}
- (CGFloat)tableView:(UITableView* )tableView heightForRowAtIndexPath:(NSIndexPath* )indexPath
{
    if (tableView == _dataTableView) {
        if (meetingDetailsInt == 1) {
            return 120;
        }
        else if (meetingDetailsInt == 2 ) {
            return 100;
        }
        else if (meetingDetailsInt == 3 ) {
            if (indexPath.row == 1) {
                if (textViewTextAgenda) {
                    return [self heightForText:textViewTextAgenda withFont:[UIFont systemFontOfSize:14] andWidth:self.view.bounds.size.width-20]+65;
                    
                }
            }
            else if (indexPath.row==0)
            {
                _dataTableView.estimatedRowHeight=44;
                return UITableViewAutomaticDimension;
            }
            else{
                return 44;
            }
        }
        return 80;
    }
    else if (tableView == agendaTableView) {
        if (meetingDetailsInt == 2) {
            if (indexPath.row == 0) {
                if (180 + [self heightForText:textViewTextAgenda withFont:[UIFont systemFontOfSize:14] andWidth:190] < 350) {
                    agendeTableViewHeight.constant = 180 + [self heightForText:textViewTextAgenda withFont:[UIFont systemFontOfSize:14] andWidth:203];
                    return [self heightForText:textViewTextAgenda withFont:[UIFont systemFontOfSize:14] andWidth:203]+65;
                }
                else{
                    agendeTableViewHeight.constant = 350;
                    return 240;
                }
            }
        }
        
        return 60;
    }
    return 60;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _dataTableView) {
        
        if (meetingDetailsInt == 1 || meetingDetailsInt == 2) {
            if (meetingDetailsInt == 1 && indexPath.row == 0) {
                NSLog(@"test");
            }
            else if( meetingDetailsInt == 2 && agendaArray.count == 0){
                NSLog(@"test 1");
            }
            else{
                
                olddata = YES;
                oldDataIndex = indexPath.row;
                companyIDBool = YES;
                categoryIDBool = YES;
                popOverView.hidden = NO;
                if (meetingDetailsInt == 1) {
                    NSLog(@"membersArray-----%@",membersArray[indexPath.row]);
                    memberString = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"name"];
                    AssignedTo = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"nameID"];
                    roleString = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"role"];
                    Role = [[membersArray objectAtIndex:indexPath.row] objectForKey:@"roleID"];
                    presence = [[[membersArray objectAtIndex:indexPath.row] objectForKey:@"Presence"] intValue];
                    membersPopoverTableView.hidden = NO;
                    agendaTableView.hidden = YES;
                    [membersPopoverTableView reloadData];
                }
                if (meetingDetailsInt == 2 ) {
                    if ([[agendaArray objectAtIndex:oldDataIndex] objectForKey:@"descripition"] != [NSNull null]) {
                        textViewTextAgenda = [self convertHTML:[[agendaArray objectAtIndex:oldDataIndex] objectForKey:@"descripition"]];
                    }
                    
                    membersPopoverTableView.hidden = YES;
                    agendaTableView.hidden = NO;
                    [agendaTableView reloadData];
                }
            }
        }
    }
}
- (IBAction)addBtn:(id)sender {
    roleNeed = NO;
    companyIDBool = NO;
    categoryIDBool = NO;
    olddata = NO;
    updatemember = NO;
    oldDataIndex = 5000;
    if (meetingDetailsInt == 1) {
        comanyString = @"";
        catogeryString = @"";
        memberString = @"";
        
        popOverView.hidden = NO;
        membersPopoverTableView.hidden = NO;
        agendaTableView.hidden = YES;
        [membersPopoverTableView reloadData];
    }
    else if (meetingDetailsInt == 2) {
        popOverView.hidden = NO;
        membersPopoverTableView.hidden = YES;
        agendaTableView.hidden = NO;
        [agendaTableView reloadData];
        
    }
}
- (IBAction)membereCategoryBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        
        olddata = NO;
        
        NSIndexPath *ip ;
        MeetingDetailsTableViewCell *cell;
        ip = [NSIndexPath indexPathForRow:1 inSection:0];
        cell = [membersPopoverTableView cellForRowAtIndexPath:ip];
        comanyString = @"";
        
        ip = [NSIndexPath indexPathForRow:2 inSection:0];
        cell = [membersPopoverTableView cellForRowAtIndexPath:ip];
        memberString = @"";
        
        
        sharedManager.passingMode = @"category";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
        [self presentViewController:myNavController animated:YES completion:nil];
        categoryIDBool = NO;
        companyIDBool = NO;
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)membersCompanyBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        
        olddata = NO;
        
        NSIndexPath *ip ;
        MeetingDetailsTableViewCell *cell ;
        ip = [NSIndexPath indexPathForRow:0 inSection:0];
        
        cell = [membersPopoverTableView cellForRowAtIndexPath:ip];    // create an alert controller with action sheet appearance
        if ([cell.categoryTF.text isEqualToString:@""]) {
            [self showMsgAlert:@"Please select Catagory"];
        }
        else{
            sharedManager.passingMode = @"company";
            if (!categoryIDBool) {
                sharedManager.passingId = StakeholderCategory;
            }
            else{
                sharedManager.passingId = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"categoryID"];
            }
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
        }
        ip = [NSIndexPath indexPathForRow:2 inSection:0];
        cell = [membersPopoverTableView cellForRowAtIndexPath:ip];
        memberString = @"";
        
        categoryIDBool = NO;
        companyIDBool = NO;
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)membersFindBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        olddata = NO;
        
        if (stakeHType ==1) {
            sharedManager.passingMode = @"assignedToInternal";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
        }
        else  if ([comanyString isEqualToString:@""]) {
            [self showMsgAlert:@"Please select Company"];
        }
        else{
            sharedManager.passingMode = @"assignedTo";
            if (!companyIDBool) {
                sharedManager.passingId = Company;
            }
            else{
                sharedManager.passingId = [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"companyID"];
            }
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
        }
        
        roleString = @"";
        companyIDBool = NO;
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)membersRoleBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        
        olddata = NO;
        NSIndexPath *ip ;
        ip = [NSIndexPath indexPathForRow:2 inSection:0];
        
        MeetingDetailsTableViewCell *cell = [membersPopoverTableView cellForRowAtIndexPath:ip];
        if ([cell.memberTF.text isEqualToString:@""]) {
            [self showMsgAlert:@"Please select Member"];
        }
        else{
            sharedManager.passingMode = @"role";
            sharedManager.passingId = Company;
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
        }
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
-(void)fillTheTF{
    
    if (sharedManager.passingString.length != 0) {
        if ([sharedManager.passingMode isEqualToString: @"category"]) {
            catogeryString = sharedManager.passingString;
            StakeholderCategory = sharedManager.passingId;
        }
        if ([sharedManager.passingMode isEqualToString: @"company"]) {
            comanyString = sharedManager.passingString;
            Company = sharedManager.passingId;
        }
        if ([sharedManager.passingMode isEqualToString: @"assignedTo"]) {
            memberString = sharedManager.passingString;
            AssignedTo = sharedManager.passingId;
        }
        if ([sharedManager.passingMode isEqualToString: @"assignedToInternal"]) {
            memberString= sharedManager.passingString;
            AssignedTo = sharedManager.passingId;
        }
        if ([sharedManager.passingMode isEqualToString: @"role"]) {
            roleString = sharedManager.passingString;
            Role = sharedManager.passingId;
        }
        [self roleCell];
        [membersPopoverTableView reloadData];
        
    }
    
}
-(void)roleCell{
    
    if (memberString.length == 0) {
        if (roleNeed) {
            
            roleNeed = NO;
        }
    }
    else{
        
        roleNeed = YES;
    }
    
}
- (IBAction)membersDoneBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        
        NSIndexPath *ip ;
        MeetingDetailsTableViewCell *cell ;
        NSString *msg = @"";
        
        ip = [NSIndexPath indexPathForRow:3 inSection:0];
        cell = [membersPopoverTableView cellForRowAtIndexPath:ip];
        if ([cell.roleTF.text isEqualToString:@""]) {
            msg = @"Please select role";
        }
        ip = [NSIndexPath indexPathForRow:2 inSection:0];
        cell = [membersPopoverTableView cellForRowAtIndexPath:ip];
        if ([cell.memberTF.text isEqualToString:@""]) {
            msg = @"Please select member";
        }
        ip = [NSIndexPath indexPathForRow:1 inSection:0];
        cell = [membersPopoverTableView cellForRowAtIndexPath:ip];
        if ([cell.companyTF.text isEqualToString:@""]) {
            msg = @"Please select Company";
        }
        ip = [NSIndexPath indexPathForRow:0 inSection:0];
        cell = [membersPopoverTableView cellForRowAtIndexPath:ip];
        if ([cell.categoryTF.text isEqualToString:@""]) {
            msg = @"Please select Catagory";
        }
        
        if (msg.length != 0) {
            [self showMsgAlert:msg];
        }
        else{
            NSDictionary *dict;
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd/mm/yyyy"];
            NSDate *meetingDate =[dateFormatter dateFromString:[_meetingDetails objectForKey:@"meetingDate"]];
            [dateFormatter setDateFormat:@"yyyy/mm/dd"];
            
            if (updatemember && [[membersArray objectAtIndex:oldDataIndex] objectForKey:@"update"]) {
                dict = @{ @"Mode":@"UPD",@"MeetingDate" : [dateFormatter stringFromDate:meetingDate],@"MeetingTypeID" : [_meetingDetails objectForKey:@"meetingID"],@"UserID" : AssignedTo, @"RoleID" : Role,@"ALevelID" : @0 ,@"Presence" : [NSNumber numberWithInt:presence] };
                
            }
            else{
                dict= @{ @"Mode":@"INS",@"MeetingDate" : [dateFormatter stringFromDate:meetingDate],@"MeetingTypeID" : [_meetingDetails  objectForKey:@"meetingID"],@"UserID" : AssignedTo, @"RoleID" : Role,@"ALevelID" : @0 ,@"Presence" :@1  };
            }
            NSString* JsonString = [Utitlity JSONStringConv: dict];
            NSLog(@"JsonString-----%@",JsonString);
            [[WebServices sharedInstance]apiAuthwithJSON:PostAddAdhocMeetingMembers HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
                
                NSString *error=[json valueForKey:@"error"];
                [self hideProgress];
                
                if(error.length>0){
                    [self showMsgAlert:[json valueForKey:@"error_description"]];
                    return ;
                }else{
                    if(json.count==0){
                        [self showMsgAlert:@"Error , Try Again"];
                    }else{
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Conduct Meeting" message:[json valueForKey:@"AddAdhocMeetingMembersResult"] preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                             {
                                                 [self loadMemberDataFromApi];
                                                 popOverView.hidden=YES;
                                             }];
                        [alert addAction:ok];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                }
                
            }];
            
        }
    }
    else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)cancelBtn:(id)sender {
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    membersPopoverTableView.hidden = YES;
    popOverView.hidden = YES;
    [_dataTableView reloadData];
}
- (IBAction)agendaCancelBtn:(id)sender {
    agendaTableView.hidden = YES;
    popOverView.hidden = YES;
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    [_dataTableView reloadData];
}
- (IBAction)agendaDoneBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        
        NSIndexPath *ip ;
        MeetingDetailsTableViewCell *cell ;
        NSString *msg = @"";
        NSString *description;
        NSString *duration;
        ip = [NSIndexPath indexPathForRow:1 inSection:0];
        cell = [agendaTableView cellForRowAtIndexPath:ip];
        [cell.durationTF resignFirstResponder];
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        if ([[cell.durationTF.text stringByTrimmingCharactersInSet: set] length] == 0){
            msg = @"Please enter the duration";
        }
        else{
            duration = cell.durationTF.text;
        }
        ip = [NSIndexPath indexPathForRow:0 inSection:0];
        cell = [agendaTableView cellForRowAtIndexPath:ip];
        if ([cell.descriptionTV.text isEqualToString:@""]) {
            msg = @"Please enter the description";
        }
        
        else{
            description = cell.descriptionTV.text;
        }
        
        if (msg.length != 0) {
            [self showMsgAlert:msg];
        }
        else{
            NSDictionary *dict;
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd/mm/yyyy"];
            NSDate *meetingDate =[dateFormatter dateFromString:[_meetingDetails objectForKey:@"meetingDate"]];
            [dateFormatter setDateFormat:@"yyyy/mm/dd"];
            //        NSDictionary *dict = @{ @"descripition" : description,@"duration" : duration};
            
            if (oldDataIndex != 5000) {
                dict = @{ @"Mode":@"UPD",@"MeetingDate" : [dateFormatter stringFromDate:meetingDate],@"MeetingTypeID" : [_meetingDetails objectForKey:@"meetingID"],@"AgendaDescription" : description, @"AgendaDuration" : duration ,@"AgendaID":[[agendaArray objectAtIndex:oldDataIndex] objectForKey:@"AgendaID"]};
                
            }
            else{
                
                dict= @{ @"Mode":@"INS",@"MeetingDate" : [dateFormatter stringFromDate:meetingDate],@"MeetingTypeID" : [_meetingDetails objectForKey:@"meetingID"],@"AgendaDescription" : description, @"AgendaDuration" : duration ,@"AgendaID":@0  };
            }
            NSString* JsonString = [Utitlity JSONStringConv: dict];
            NSLog(@"JsonString-------%@",JsonString);
            [[WebServices sharedInstance]apiAuthwithJSON:PostAddAdhocMeetingAgendas HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
                
                NSString *error=[json valueForKey:@"error"];
                [self hideProgress];
                
                if(error.length>0){
                    [self showMsgAlert:[json valueForKey:@"error_description"]];
                    return ;
                }else{
                    if(json.count==0){
                        [self showMsgAlert:@"Error , Try Again"];
                    }else{
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Conduct Meeting" message:@"Agenda saved successfully" preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                             {
                                                 [self loadAgendaDataFromApi];
                                                 popOverView.hidden=YES;
                                             }];
                        [alert addAction:ok];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                }
                
            }];
            
        }
    }
    else{
        [self showMsgAlert:NOInternetMessage];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    [self updateTextLabelsWithText: newString];
    return YES;
}
-(void)updateTextLabelsWithText:(NSString *)string
{
    textViewTextAgenda = string;
}
-(CGFloat)heightForText:(NSString*)text withFont:(UIFont *)font andWidth:(CGFloat)width
{
    CGSize constrainedSize = CGSizeMake(width, MAXFLOAT);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:text attributes:attributesDictionary];
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    if (requiredHeight.size.width > width) {
        requiredHeight = CGRectMake(0,0,width, requiredHeight.size.height);
    }
    return requiredHeight.size.height;
}

- (void)textViewDidChange:(UITextView *)textView
{
    if ( meetingDetailsInt == 2) {
        CGFloat fixedWidth = textView.frame.size.width;
        CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
        CGRect newFrame = textView.frame;
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
        if (180 + [self heightForText:textViewTextAgenda withFont:[UIFont systemFontOfSize:14] andWidth:190] < 300) {
            [agendaTableView beginUpdates];
            textView.frame = newFrame;
            [agendaTableView endUpdates];
        }
    }
    else{
        CGFloat fixedWidth = textView.frame.size.width;
        CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
        CGRect newFrame = textView.frame;
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
        [_dataTableView beginUpdates];
        textView.frame = newFrame;
        [_dataTableView endUpdates];
    }
}
- (IBAction)presenceBtn:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Present" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        presence = 1;
        [membersPopoverTableView reloadData];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Apologies" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        presence = 2;
        [membersPopoverTableView reloadData];
    }];
    
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}
- (IBAction)backBtn:(id)sender {
    if (meetingDetailsInt == 1) {
        sharedManager.decrement = decrement;
        sharedManager.currmins = currMinute;
        sharedManager.currHour = currHours;
        sharedManager.currSecs = currSeconds;
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if (meetingDetailsInt == 2) {
        _tittleLabel.text = @"Members";
        [self loadMemberDataFromApi];
        meetingDetailsInt = 1;
        [_dataTableView reloadData];
    }
    else if (meetingDetailsInt == 3) {
        _tittleLabel.text = @"Agendas";
        [self loadAgendaDataFromApi];
        meetingDetailsInt = 2;
        [_dataTableView reloadData];
    }
    
}
- (IBAction)nextBtn:(id)sender {
    if (meetingDetailsInt == 1) {
        _tittleLabel.text = @"Agendas";
        [self loadAgendaDataFromApi];
        meetingDetailsInt = 2;
        [_dataTableView reloadData];
    }
    else if (meetingDetailsInt == 2) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        AgendaDescriptionViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"AgendaDescriptionViewController"];
        myNavController.meetingDetails = _meetingDetails;
        sharedManager.decrement = decrement;
        sharedManager.currmins = currMinute;
        sharedManager.currHour = currHours;
        sharedManager.currSecs = currSeconds;
        [self presentViewController:myNavController animated:YES completion:nil];
        
    }
}
- (IBAction)assignActionBtn:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddActionViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"AddActionViewController"];
    myNavController.update = NO;
    myNavController.meetingAction = YES;
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/mm/yyyy"];
    NSDate *meetingDate =[dateFormatter dateFromString:[_meetingDetails objectForKey:@"meetingDate"]];
    [dateFormatter setDateFormat:@"yyyy/mm/dd"];
    myNavController.meetingDate = [dateFormatter stringFromDate:meetingDate];
    NSLog(@"myNavController.meetingDate-----%@",myNavController.meetingDate);
    myNavController.meetingID = [_meetingDetails objectForKey:@"meetingID"];
    [self presentViewController:myNavController animated:YES completion:nil];
}
- (IBAction)discussionPreBtn:(id)sender {
    if (agendaDiscussionIndex >0) {
        -- agendaDiscussionIndex ;
        textViewTextAgenda =[[agendaDiscussionArray objectAtIndex:agendaDiscussionIndex] objectForKey:@"DiscussionDescription"];
        [_dataTableView reloadData];
    }
}
- (IBAction)discussionSaveBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        NSDictionary *dict;
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/mm/yyyy"];
        NSDate *meetingDate =[dateFormatter dateFromString:[_meetingDetails objectForKey:@"meetingDate"]];
        [dateFormatter setDateFormat:@"yyyy/mm/dd"];
        dict= @{ @"MeetingDate" : [dateFormatter stringFromDate:meetingDate],@"MeetingTypeID" : [_meetingDetails objectForKey:@"meetingID"],@"DiscussionID" : [[agendaDiscussionArray objectAtIndex:agendaDiscussionIndex] objectForKey:@"DiscussionID"], @"DiscussionDescription" : textViewTextAgenda  };
        NSLog(@"dict-------%@",dict);
        NSString* JsonString = [Utitlity JSONStringConv: dict];
        
        [[WebServices sharedInstance]apiAuthwithJSON:PostUpdateAdhocMeetingDiscussion HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
            
            NSString *error=[json valueForKey:@"error"];
            [self hideProgress];
            
            if(error.length>0){
                [self showMsgAlert:[json valueForKey:@"error_description"]];
                return ;
            }else{
                if(json.count==0){
                    [self showMsgAlert:@"Error , Try Again"];
                }else{
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Conduct Meeting" message:[json valueForKey:@"UpdateAdhocMeetingDiscussionResult"] preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                         {
                                             [self loadAgendaDiscussionDataFromApi];
                                         }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
            
        }];
        
    }
    else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)discussionNextBtn:(id)sender {
    if (agendaDiscussionIndex <agendaDiscussionArray.count-1) {
        ++ agendaDiscussionIndex ;
        textViewTextAgenda =[[agendaDiscussionArray objectAtIndex:agendaDiscussionIndex] objectForKey:@"DiscussionDescription"];
        [_dataTableView reloadData];
    }
}

-(void)loadMemberDataFromApi{
    if([Utitlity isConnectedTointernet]){
        [self showProgress];
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/mm/yyyy"];
        NSDate *meetingDate =[dateFormatter dateFromString:[_meetingDetails objectForKey:@"meetingDate"]];
        [dateFormatter setDateFormat:@"yyyy/mm/dd"];
        
        NSString *targetUrl = [NSString stringWithFormat:@"%@GetAdhocMeetingMembers?MeetingID=%@&MeetingDate=%@", BASEURL,[_meetingDetails objectForKey:@"meetingID"],[dateFormatter stringFromDate: meetingDate]];
        NSLog(@"targetUrl=====%@",targetUrl);
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPMethod:@"GET"];
        [request setURL:[NSURL URLWithString:targetUrl]];
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
          ^(NSData * _Nullable data,
            NSURLResponse * _Nullable response,
            NSError * _Nullable error) {
              NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
              NSLog(@"%@",dataArray);
              [membersArray removeAllObjects];
              for (int i = 0; i < dataArray.count; i++) {
                  NSDictionary *dict= @{ @"name" : [[dataArray objectAtIndex:i]  objectForKey:@"EmpName"], @"role" : [[dataArray objectAtIndex:i] objectForKey:@"RoleDescription"], @"roleID" : [[dataArray objectAtIndex:i] objectForKey:@"RoleID"],@"nameID" : [[dataArray objectAtIndex:i] objectForKey:@"UserID"],@"ALevelDescription" : [[dataArray objectAtIndex:i] objectForKey:@"ALevelDescription"] ,@"Presence" : [[[dataArray objectAtIndex:i] objectForKey:@"Presence"] stringValue],@"update":@"yes"};
                  [membersArray addObject:dict];
              }
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self hideProgress];
                  [_dataTableView reloadData];
              });
              
          }] resume];
        
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
-(void)loadAgendaDataFromApi{
    if([Utitlity isConnectedTointernet]){
        
        [self showProgress];
        
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/mm/yyyy"];
        NSDate *meetingDate =[dateFormatter dateFromString:[_meetingDetails objectForKey:@"meetingDate"]];
        [dateFormatter setDateFormat:@"yyyy/mm/dd"];
        NSString *targetUrl = [NSString stringWithFormat:@"%@GetAdhocMeetingAgendas?MeetingID=%@&MeetingDate=%@", BASEURL,[_meetingDetails objectForKey:@"meetingID"],[dateFormatter stringFromDate: meetingDate]];
        NSLog(@"targetUrl=====%@",targetUrl);
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPMethod:@"GET"];
        [request setURL:[NSURL URLWithString:targetUrl]];
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
          ^(NSData * _Nullable data,
            NSURLResponse * _Nullable response,
            NSError * _Nullable error) {
              NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
              NSLog(@"%@",dataArray);
              [agendaArray removeAllObjects];
              for (int i = 0; i < [dataArray count]; i++) {
                  NSString *discription;
                  if ([[dataArray objectAtIndex:i] objectForKey:@"AgendaDescription"] != [NSNull null]) {
                      discription = [[dataArray objectAtIndex:i] objectForKey:@"AgendaDescription"];
                  }
                  else{
                      discription= @"";
                  }
                  
                  NSDictionary *dict = @{ @"descripition" : [[dataArray objectAtIndex:i] objectForKey:@"AgendaDescription"],@"AgendaID" : [[dataArray objectAtIndex:i] objectForKey:@"AgendaID"],@"duration" : [[dataArray objectAtIndex:i] objectForKey:@"AgendaDuration"]};
                  [agendaArray addObject:dict];
              }
              NSLog(@"agendaArray------%@",agendaArray);
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self hideProgress];
                  [_dataTableView reloadData];
              });
              
          }] resume];
        
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
-(void)loadAgendaDiscussionDataFromApi{
    if([Utitlity isConnectedTointernet]){
        
        [self showProgress];
        
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/mm/yyyy"];
        NSDate *meetingDate =[dateFormatter dateFromString:[_meetingDetails objectForKey:@"meetingDate"]];
        [dateFormatter setDateFormat:@"yyyy/mm/dd"];
        NSString *targetUrl = [NSString stringWithFormat:@"%@GetAdhocMeetingDiscussions?MeetingID=%@&MeetingDate=%@", BASEURL,[_meetingDetails objectForKey:@"meetingID"],[dateFormatter stringFromDate: meetingDate]];
        NSLog(@"targetUrl=====%@",targetUrl);
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPMethod:@"GET"];
        [request setURL:[NSURL URLWithString:targetUrl]];
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
          ^(NSData * _Nullable data,
            NSURLResponse * _Nullable response,
            NSError * _Nullable error) {
              NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
              NSLog(@"%@",dataArray);
              agendaDiscussionArray = dataArray;
              if ([self convertHTML:[[agendaDiscussionArray objectAtIndex:agendaDiscussionIndex] objectForKey:@"DiscussionTitle"]]) {
                  textViewTextAgenda = [self convertHTML:[[agendaDiscussionArray objectAtIndex:agendaDiscussionIndex] objectForKey:@"DiscussionTitle"]];
              }
              else{
                  textViewTextAgenda = @"";
              }
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self hideProgress];
                  [_dataTableView reloadData];
              });
              
          }] resume];
        
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
-(void)showProgress{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:_dataTableView animated:YES];
    
}

-(void)hideProgress{
    
    [MBProgressHUD hideHUDForView:_dataTableView animated:YES];
}
-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}
-(NSString *)convertHTML:(NSString *)html {
    
    NSScanner *myScanner;
    NSString *text = nil;
    myScanner = [NSScanner scannerWithString:html];
    
    while ([myScanner isAtEnd] == NO) {
        
        [myScanner scanUpToString:@"<" intoString:NULL] ;
        
        [myScanner scanUpToString:@">" intoString:&text] ;
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    //
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return html;
}
- (void)timerTick:(NSTimer *)timer {
    NSDate *now = [NSDate date];
    
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"E MMM dd yyyy hh:mm:ss a";  // very simple format  "8:47:22 AM"
    }
    NSString *countDownTimer ;
    if (decrement) {
        countDownTimer =  [self  timerFired];
    }
    else{
        countDownTimer = [self timerincrementer];
    }
    _timeLabel.text = [NSString stringWithFormat:@" %@ \nGMT+05:30(India Standard Time) %@",[dateFormatter stringFromDate:now],countDownTimer];
}
-(NSString*)timerFired
{
    if(currMinute == 0 && currSeconds == 0 && currHours == 0)
    {
        decrement = NO;
    }
    else{
        decrement = YES;
        
        if(currMinute == 0 && currHours>0 && currSeconds==0)
        {
            currHours-=1;
            currMinute=59;
            mins = NO;
        }
        else if(currSeconds==0 && currMinute>0 )
        {
            if (mins) {
                currMinute-=1;
            }
            currSeconds=59;
        }
        else if(currSeconds>0 )
        {
            currSeconds-=1;
            mins = YES;
        }
    }
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds];
}
-(NSString*)timerincrementer
{
    if(currSeconds<59)
    {
        currSeconds+=1;
    }
    else if(currSeconds == 59 && currMinute < 59)
    {
        currMinute+=1;
        currSeconds=0;
    }
    else  if(currMinute == 59)
    {
        currHours+=1;
        currMinute=0;
        currSeconds=0;
    }
    
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds];
}
- (IBAction)StakeHType:(UIButton *)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select" message:nil preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"External StakeHolders" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        stakeHType = 2;
        StakeholderCategory = @"";
        comanyString = @"";
        memberString = @"";
        [membersPopoverTableView reloadData];
    }];
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Internal StakeHolders" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        stakeHType = 1;
        StakeholderCategory = @"";
        comanyString = @"";
        memberString = @"";
        [membersPopoverTableView reloadData];
    }];
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}
@end
