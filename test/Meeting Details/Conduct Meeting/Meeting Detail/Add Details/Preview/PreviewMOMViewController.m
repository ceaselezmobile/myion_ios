//
//  PreviewMOMViewController.m
//  test
//
//  Created by ceaselez on 24/01/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "PreviewMOMViewController.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "MySharedManager.h"
#import "MOMMembersViewController.h"

@interface PreviewMOMViewController ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *momWebView;
@property (weak, nonatomic) IBOutlet UITextField *subjectTF;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeLabelConstraint;

@end

@implementation PreviewMOMViewController
{
    MySharedManager *sharedManager;
    NSTimer *timer;
    long currMinute;
    long currSeconds;
    long currHours;
    BOOL decrement;
    BOOL mins;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    sharedManager = [MySharedManager sharedManager];
    if (sharedManager.realTime) {
        decrement = sharedManager.decrement;
        currHours = sharedManager.currHour;
        currSeconds = sharedManager.currSecs;
        currMinute =  sharedManager.currmins ;
        NSString *countDownTimer=[NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds];
        NSDate *now = [NSDate date];
        
        static NSDateFormatter *dateFormatter;
        if (!dateFormatter) {
            dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"E MMM dd yyyy hh:mm:ss a";  // very simple format  "8:47:22 AM"
        }
        _timeLabel.text = [NSString stringWithFormat:@" %@ \nGMT+05:30(India Standard Time) %@",[dateFormatter stringFromDate:now],countDownTimer];

      
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
    }
    else{
        _timeLabel.hidden = YES;
        _timeLabelConstraint.constant = 30;
    }
    NSLog(@"test -----------");
    if([Utitlity isConnectedTointernet]){
        [self showProgress];
        NSLog(@"%@",_pdfUrl);
       _pdfUrl = [_pdfUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:_pdfUrl]];
        [_momWebView loadRequest:request];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}

-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)showProgress{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}

-(void)hideProgress{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
                [self hideProgress];
}
- (IBAction)backBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)momMeberBtn:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MOMMembersViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"MOMMembersViewController"];
    sharedManager.decrement = decrement;
    sharedManager.currmins = currMinute;
    sharedManager.currHour = currHours;
    sharedManager.currSecs = currSeconds;
    [self presentViewController:myNavController animated:YES completion:nil];
}
- (IBAction)doneBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        UIAlertController *alert;
        alert = [UIAlertController alertControllerWithTitle:nil message:@"Do you want to send minutes of meeting now?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                 [self finalSubmitAPI:@1];
                             }];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                 {
                                     [self finalSubmitAPI:@0];
                                 }];
        [alert addAction:ok];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else{
        [self showMsgAlert:NOInternetMessage];
    }
}
-(void)finalSubmitAPI:(NSNumber *)MomSendStatus{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[_meetingDetails objectForKey:@"meetingID"] forKey:@"MeetingTypeId"];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/mm/yyyy"];
    NSDate *meetingDate =[dateFormatter dateFromString:[_meetingDetails objectForKey:@"meetingDate"]];
    [dateFormatter setDateFormat:@"yyyy-mm-dd"];
    [params setObject:[dateFormatter stringFromDate:meetingDate] forKey:@"MeetingDate"];
    [params setObject:[defaults objectForKey:@"EmployeeName"] forKey:@"MinutesTakenBy"];
    if (sharedManager.realTime) {
        [params setObject:[NSString stringWithFormat:@"%@ %@",[dateFormatter stringFromDate:meetingDate],_ActualStartTime] forKey:@"ActualStartTime"];
        [params setObject:[NSString stringWithFormat:@"%@ %@",[dateFormatter stringFromDate:meetingDate],_ActualEndTime] forKey:@"ActualEndTime"];
    }
    else{
    [params setObject:[NSString stringWithFormat:@"%@ %@",[dateFormatter stringFromDate:meetingDate],[_meetingDetails objectForKey:@"ActualStartTime"]] forKey:@"ActualStartTime"];
    [params setObject:[NSString stringWithFormat:@"%@ %@",[dateFormatter stringFromDate:meetingDate],[_meetingDetails objectForKey:@"ActualEndTime"]] forKey:@"ActualEndTime"];
    }
    [dateFormatter setDateFormat:@"dd-mm-yyyy"];
    [params setObject:[NSString stringWithFormat:@"%@-%@(Adhoc)",[_meetingDetails objectForKey:@"PDFName"],[dateFormatter stringFromDate:meetingDate]] forKey:@"PDFName"];
    [params setObject:[_meetingDetails objectForKey:@"Mode"] forKey:@"Mode"];
    [params setObject:MomSendStatus forKey:@"MomSendStatus"];
    [params setObject:@"Publish" forKey:@"PublishStatus"];
    [params setObject:[_meetingDetails objectForKey:@"Duration"] forKey:@"Duration"];
    [params setObject:_subjectTF.text forKey:@"AddSubject"];
    [params setObject:@1 forKey:@"IncludeMeetingMember"];
    if (sharedManager.mailArray.count != 0) {
        [params setObject:[sharedManager.mailArray componentsJoinedByString:@";"] forKey:@"EmailString"];
    }
    else{
        [params setObject:@"" forKey:@"EmailString"];
    }
    
    [self showProgress];
    NSString* JsonString = [Utitlity JSONStringConv: params];
    NSLog(@"json----%@",JsonString);
    
    [[WebServices sharedInstance]apiAuthwithJSON:POSTFinishMeeting HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
        
        NSString *error=[json valueForKey:@"error"];
        [self hideProgress];
        
        if(error.length>0){
            [self showMsgAlert:[json valueForKey:@"error_description"]];
            return ;
        }else{
            if(json.count==0){
                [self showMsgAlert:@"Error , Try Again"];
            }else{
                NSLog(@"json-----%@",json);
                UIWindow* window = [[UIApplication sharedApplication] keyWindow];
                window.rootViewController = [window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
             [self.view.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
            }
        }
        
    }];
}
- (void)timerTick:(NSTimer *)timer {
    NSDate *now = [NSDate date];
    
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"E MMM dd yyyy hh:mm:ss a";  // very simple format  "8:47:22 AM"
    }
    NSString *countDownTimer ;
    if (decrement) {
        countDownTimer =  [self  timerFired];
    }
    else{
        countDownTimer = [self timerincrementer];
    }
    _timeLabel.text = [NSString stringWithFormat:@" %@ \nGMT+05:30(India Standard Time) %@",[dateFormatter stringFromDate:now],countDownTimer];
}
-(NSString*)timerFired
{
    if(currMinute == 0 && currSeconds == 0 && currHours == 0)
    {
        decrement = NO;
    }
    else{
        if(currMinute == 0 && currHours>0 && currSeconds==0)
        {
            currHours-=1;
            currMinute=59;
            mins = NO;
        }
        else if(currSeconds==0 && currMinute>0 )
        {
            if (mins) {
                currMinute-=1;
            }
            currSeconds=59;
        }
        else if(currSeconds>0 )
        {
            currSeconds-=1;
            mins = YES;
        }
    }
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds];
}
-(NSString*)timerincrementer
{
    if(currSeconds<59)
    {
        currSeconds+=1;
    }
    else if(currSeconds == 59 && currMinute < 59)
    {
        currMinute+=1;
        currSeconds=0;
    }
    else  if(currMinute == 59)
    {
        currHours+=1;
        currMinute=0;
        currSeconds=0;
    }
    
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds];
}

@end
