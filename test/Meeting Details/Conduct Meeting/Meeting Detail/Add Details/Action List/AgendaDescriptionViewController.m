//
//  AgendaDescriptionViewController.m
//  test
//
//  Created by ceaselez on 28/02/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "AgendaDescriptionViewController.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "MeetingDetailsTableViewCell.h"
#import "MySharedManager.h"
#import "MeetingActionListViewController.h"
#import "AddActionViewController.h"

@interface AgendaDescriptionViewController ()
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeLabelConstraint;
@end

@implementation AgendaDescriptionViewController
{
    MySharedManager *sharedManager;
    NSArray *agendaDiscussionArray;
    int agendaDiscussionIndex;
    NSString *textViewTextAgenda;

    NSTimer *timer;
    long currMinute;
    long currSeconds;
    long currHours;
    BOOL decrement;
    BOOL mins;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadAgendaDiscussionDataFromApi];
    sharedManager = [MySharedManager sharedManager];

    if (sharedManager.realTime) {
        if (sharedManager.currHour == 00 && sharedManager.currmins == 00 && sharedManager.currSecs == 00) {
            NSDateFormatter *dateFormatEnd = [[NSDateFormatter alloc] init];
            [dateFormatEnd setDateFormat:@"hh:mm a"];
            NSDate *endTime = [dateFormatEnd dateFromString:[_meetingDetails objectForKey:@"ActualEndTime"]];
            NSDate *realTime =[dateFormatEnd dateFromString:[_meetingDetails objectForKey:@"ActualStartTime"]];
            NSLog(@"realTime-----%@endTime------%@,",realTime,endTime);
            NSDateComponents *components;
            components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate: realTime toDate: endTime options: 0];
            currHours = [components hour];
            if ([components minute] != 0) {
                currMinute=[components minute]-1;
            }
            else{
                currMinute=[components minute];
            }
            currSeconds=00;
            decrement = YES;
        }
        else{
            decrement = sharedManager.decrement;
            currHours = sharedManager.currHour;
            currMinute=sharedManager.currmins;
            currSeconds=sharedManager.currSecs;
        }
        NSString *countDownTimer=[NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds];
        NSDate *now = [NSDate date];
        
        static NSDateFormatter *dateFormatter;
        if (!dateFormatter) {
            dateFormatter = [[NSDateFormatter alloc] init];
            dateFormatter.dateFormat = @"E MMM dd yyyy hh:mm:ss a";  // very simple format  "8:47:22 AM"
        }
        _timeLabel.text = [NSString stringWithFormat:@" %@ \nGMT+05:30(India Standard Time) %@",[dateFormatter stringFromDate:now],countDownTimer];
        
        [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTick:) userInfo:nil repeats:YES];
        
    }
    else{
        _timeLabel.hidden = YES;
        _timeLabelConstraint.constant = 20;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
            return  3;
  }
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MeetingDetailsTableViewCell *cell ;

    if (indexPath.row == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"title" forIndexPath:indexPath];
        if ([[agendaDiscussionArray objectAtIndex:agendaDiscussionIndex] objectForKey:@"DiscussionTitle"]) {
            cell.discussionTitleLabel.numberOfLines=0;
            cell.discussionTitleLabel.lineBreakMode=NSLineBreakByWordWrapping;
            cell.discussionTitleLabel.text = [self convertHTML:[[agendaDiscussionArray objectAtIndex:agendaDiscussionIndex] objectForKey:@"DiscussionTitle"]];
            
        }
    }
    if (indexPath.row == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"description" forIndexPath:indexPath];
        cell.discussionDescriptionTV.text = textViewTextAgenda;
    }
    if (indexPath.row == 2) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"buttons" forIndexPath:indexPath];
    }
    return cell;

}
- (CGFloat)tableView:(UITableView* )tableView heightForRowAtIndexPath:(NSIndexPath* )indexPath
{
    if (indexPath.row == 1) {
        if (textViewTextAgenda) {
            return [self heightForText:textViewTextAgenda withFont:[UIFont systemFontOfSize:14] andWidth:self.view.bounds.size.width-20]+60;
        }
    }
//    else if (indexPath.row==0)
//    {
//        _dataTableView.estimatedRowHeight=44;
//        return UITableViewAutomaticDimension;
//    }
        return 44;
}
- (IBAction)discussionPreBtn:(id)sender {
    if (agendaDiscussionIndex >0) {
        [self saveTheContent];
        -- agendaDiscussionIndex ;
        textViewTextAgenda =[[agendaDiscussionArray objectAtIndex:agendaDiscussionIndex] objectForKey:@"DiscussionDescription"];
        [_dataTableView reloadData];
    }
    else if (agendaDiscussionIndex == 0) {
        [self saveTheContent];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
- (IBAction)discussionSaveBtn:(id)sender {
    [self saveTheContent];
}
-(void)saveTheContent{
    if([Utitlity isConnectedTointernet]){
        NSDictionary *dict;
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/mm/yyyy"];
        NSDate *meetingDate =[dateFormatter dateFromString:[_meetingDetails objectForKey:@"meetingDate"]];
        [dateFormatter setDateFormat:@"yyyy/mm/dd"];
        dict= @{ @"MeetingDate" : [dateFormatter stringFromDate:meetingDate],@"MeetingTypeID" : [_meetingDetails objectForKey:@"meetingID"],@"DiscussionID" : [[agendaDiscussionArray objectAtIndex:agendaDiscussionIndex] objectForKey:@"DiscussionID"], @"DiscussionDescription" : textViewTextAgenda  };
        NSString* JsonString = [Utitlity JSONStringConv: dict];
        NSLog(@"dict-------%@",JsonString);
        [[WebServices sharedInstance]apiAuthwithJSON:PostUpdateAdhocMeetingDiscussion HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
            
            NSString *error=[json valueForKey:@"error"];
            [self hideProgress];
            
            if(error.length>0){
                [self showMsgAlert:[json valueForKey:@"error_description"]];
                return ;
            }else{
                if(json.count==0){
                    [self showMsgAlert:@"Error , Try Again"];
                }else{
//                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Conduct Meeting" message:[json valueForKey:@"UpdateAdhocMeetingDiscussionResult"] preferredStyle:UIAlertControllerStyleAlert];
//
//                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
//                                         {
                                             [self loadAgendaDiscussionDataFromApi];
//                                         }];
//                    [alert addAction:ok];
//                    [self presentViewController:alert animated:YES completion:nil];
                }
            }
            
        }];
        
    }
    else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)discussionNextBtn:(id)sender {
    if (agendaDiscussionIndex <agendaDiscussionArray.count-1) {
        [self saveTheContent];
        ++ agendaDiscussionIndex ;
        textViewTextAgenda =[[agendaDiscussionArray objectAtIndex:agendaDiscussionIndex] objectForKey:@"DiscussionDescription"];
        [_dataTableView reloadData];
        
    }
    else if(agendaDiscussionIndex == agendaDiscussionArray.count-1){
        [self saveTheContent];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MeetingActionListViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"MeetingActionListViewController"];
        myNavController.meetingDetails = _meetingDetails;
        sharedManager.decrement = decrement;
        sharedManager.currmins = currMinute;
        sharedManager.currHour = currHours;
        sharedManager.currSecs = currSeconds;
        [self presentViewController:myNavController animated:YES completion:nil];
    }
}
-(void)loadAgendaDiscussionDataFromApi{
    if([Utitlity isConnectedTointernet]){
        
        [self showProgress];
        
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/mm/yyyy"];
        NSDate *meetingDate =[dateFormatter dateFromString:[_meetingDetails objectForKey:@"meetingDate"]];
        [dateFormatter setDateFormat:@"yyyy/mm/dd"];
        NSString *targetUrl = [NSString stringWithFormat:@"%@GetAdhocMeetingDiscussions?MeetingID=%@&MeetingDate=%@", BASEURL,[_meetingDetails objectForKey:@"meetingID"],[dateFormatter stringFromDate: meetingDate]];
        NSLog(@"targetUrl=====%@",targetUrl);
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPMethod:@"GET"];
        [request setURL:[NSURL URLWithString:targetUrl]];
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
          ^(NSData * _Nullable data,
            NSURLResponse * _Nullable response,
            NSError * _Nullable error) {
              NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
              NSLog(@"%@",dataArray);
         
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self hideProgress];
                  agendaDiscussionArray = dataArray;
                  if ([self convertHTML:[[agendaDiscussionArray objectAtIndex:agendaDiscussionIndex] objectForKey:@"DiscussionDescription"]]) {
                      textViewTextAgenda = [self convertHTML:[[agendaDiscussionArray objectAtIndex:agendaDiscussionIndex] objectForKey:@"DiscussionDescription"]];
                      NSLog(@"textViewTextAgenda--------%@",textViewTextAgenda);

                  }
                  else{
                      textViewTextAgenda = @"";
                  }
                  [_dataTableView reloadData];
              });
              
          }] resume];
        
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)nextBtn:(id)sender {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        MeetingActionListViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"MeetingActionListViewController"];
        myNavController.meetingDetails = _meetingDetails;
        sharedManager.decrement = decrement;
        sharedManager.currmins = currMinute;
        sharedManager.currHour = currHours;
        sharedManager.currSecs = currSeconds;
        [self presentViewController:myNavController animated:YES completion:nil];
    
}
- (IBAction)previousBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)showProgress{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:_dataTableView animated:YES];
    
}

-(void)hideProgress{
    
    [MBProgressHUD hideHUDForView:_dataTableView animated:YES];
}
-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}
-(NSString *)convertHTML:(NSString *)html {
    
    NSScanner *myScanner;
    NSString *text = nil;
    myScanner = [NSScanner scannerWithString:html];
    
    while ([myScanner isAtEnd] == NO) {
        
        [myScanner scanUpToString:@"<" intoString:NULL] ;
        
        [myScanner scanUpToString:@">" intoString:&text] ;
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    //
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return html;
}
- (void)timerTick:(NSTimer *)timer {
    NSDate *now = [NSDate date];
    
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"E MMM dd yyyy hh:mm:ss a";  // very simple format  "8:47:22 AM"
    }
    NSString *countDownTimer ;
    if (decrement) {
        countDownTimer =  [self  timerFired];
    }
    else{
        countDownTimer = [self timerincrementer];
    }
    _timeLabel.text = [NSString stringWithFormat:@" %@ \nGMT+05:30(India Standard Time) %@",[dateFormatter stringFromDate:now],countDownTimer];
}
-(NSString*)timerFired
{
    if(currMinute == 0 && currSeconds == 0 && currHours == 0)
    {
        decrement = NO;
    }
    else{
        decrement = YES;
        
        if(currMinute == 0 && currHours>0 && currSeconds==0)
        {
            currHours-=1;
            currMinute=59;
            mins = NO;
        }
        else if(currSeconds==0 && currMinute>0 )
        {
            if (mins) {
                currMinute-=1;
            }
            currSeconds=59;
        }
        else if(currSeconds>0 )
        {
            currSeconds-=1;
            mins = YES;
        }
    }
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds];
}
-(NSString*)timerincrementer
{
    if(currSeconds<59)
    {
        currSeconds+=1;
    }
    else if(currSeconds == 59 && currMinute < 59)
    {
        currMinute+=1;
        currSeconds=0;
    }
    else  if(currMinute == 59)
    {
        currHours+=1;
        currMinute=0;
        currSeconds=0;
    }
    
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",currHours,currMinute,currSeconds];
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    NSString *newString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    [self updateTextLabelsWithText: newString];
    return YES;
}
-(void)updateTextLabelsWithText:(NSString *)string
{
    textViewTextAgenda = string;
}
-(CGFloat)heightForText:(NSString*)text withFont:(UIFont *)font andWidth:(CGFloat)width
{
    CGSize constrainedSize = CGSizeMake(width, MAXFLOAT);
    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:text attributes:attributesDictionary];
    CGRect requiredHeight = [string boundingRectWithSize:constrainedSize options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    if (requiredHeight.size.width > width) {
        requiredHeight = CGRectMake(0,0,width, requiredHeight.size.height);
    }
    return requiredHeight.size.height;
}

- (void)textViewDidChange:(UITextView *)textView
{
        CGFloat fixedWidth = textView.frame.size.width;
        CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
        CGRect newFrame = textView.frame;
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
        [_dataTableView beginUpdates];
        textView.frame = newFrame;
        [_dataTableView endUpdates];
}
- (IBAction)assignActionBtn:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    AddActionViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"AddActionViewController"];
    myNavController.update = NO;
    myNavController.meetingAction = YES;
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/mm/yyyy"];
    NSDate *meetingDate =[dateFormatter dateFromString:[_meetingDetails objectForKey:@"meetingDate"]];
    [dateFormatter setDateFormat:@"yyyy/mm/dd"];
    myNavController.meetingDate = [dateFormatter stringFromDate:meetingDate];
    NSLog(@"myNavController.meetingDate-----%@",myNavController.meetingDate);
    myNavController.meetingID = [_meetingDetails objectForKey:@"meetingID"];
    [self presentViewController:myNavController animated:YES completion:nil];
}
@end
