//
//  MeetingActionListViewController.h
//  test
//
//  Created by ceaselez on 18/01/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MeetingActionListViewController : UIViewController
@property(nonatomic,strong) NSDictionary *meetingDetails;

@end
