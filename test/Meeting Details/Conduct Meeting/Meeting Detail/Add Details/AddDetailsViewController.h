//
//  AddDetailsViewController.h
//  test
//
//  Created by ceaselez on 16/01/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddDetailsViewController : UIViewController
@property(nonatomic,strong) NSDictionary *meetingDetails;

//@property(nonatomic,weak) NSString *meetingID;
//@property(nonatomic,weak) NSString *meetingDate;
//@property(nonatomic,weak) NSString *meetingActualTime;

@end
