
#import "MeetingActionDetailsViewController.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "CustomTableViewCell.h"
#import "MySharedManager.h"
@interface MeetingActionDetailsViewController ()

@end

@implementation MeetingActionDetailsViewController
{
    NSArray *detailArray;
    MySharedManager *sharedManager;
    UIRefreshControl *refreshControl;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    _MeetingDetailsTV.rowHeight = UITableViewAutomaticDimension;
    _MeetingDetailsTV.estimatedRowHeight=92;
    sharedManager = [MySharedManager sharedManager];
    detailArray = [[NSArray alloc] init];
    [ self loadDataFromApi];
    refreshControl = [[UIRefreshControl alloc]init];
    [self.MeetingDetailsTV addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(loadDataFromApi) forControlEvents:UIControlEventValueChanged];
}
-(void) viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if([sharedManager.passingId isEqualToString:@"Update Meeting"])
    {
      return 7;
    }
    else
      return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomTableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    UILabel *Titlelbl=(UILabel*)[cell viewWithTag:1];
    UILabel *Desclbl=(UILabel*)[cell viewWithTag:2];
    if([sharedManager.passingId isEqualToString:@"Update Meeting"])
    {
        if (indexPath.row == 0) {
            Titlelbl.text=@"Meeting Name";
            Desclbl.text = [detailArray valueForKey:@"MeetingDescription"];
        }
        else if(indexPath.row==1)
        {
            Titlelbl.text=@"Status";
            Desclbl.text = [detailArray valueForKey:@"Status"];
        }
        else if(indexPath.row==2)
        {
            Titlelbl.text=@"Assigned To";
            Desclbl.text = [detailArray valueForKey:@"EmpName"];
        }
//        else if(indexPath.row==3)
//        {
//            Titlelbl.text=@"Actual Date";
//            Desclbl.text = [detailArray valueForKey:@"ActualDate"];
//        }
        else if(indexPath.row==3)
        {
            Titlelbl.text=@"Start Date";
            Desclbl.text = [detailArray valueForKey:@"MeetingDate"];
        }
        else if(indexPath.row==4)
        {
            Titlelbl.text=@"Target Date";
            Desclbl.text = [detailArray valueForKey:@"TargetDate"];
        }
        else if(indexPath.row==5)
        {
            Titlelbl.text=@"Action Type";
            Desclbl.text = [detailArray valueForKey:@"ActionType"];
        }
        else if(indexPath.row==6)
        {
            Titlelbl.text=@"Action Description";
            Desclbl.text = [detailArray valueForKey:@"ActionDescription"];
        }
    }
    else
    {
    if (indexPath.row == 0) {
        Titlelbl.text=@"Meeting Name";
        Desclbl.text = [detailArray valueForKey:@"MeetingDescription"];
    }
    else if(indexPath.row==1)
    {
        Titlelbl.text=@"Assigned To";
        Desclbl.text = [detailArray valueForKey:@"EmpName"];
    }
//    else if(indexPath.row==2)
//    {
//        Titlelbl.text=@"Actual Date";
//        Desclbl.text = [detailArray valueForKey:@"ActualDate"];
//    }
    else if(indexPath.row==2)
    {
        Titlelbl.text=@"Start Date";
        Desclbl.text = [detailArray valueForKey:@"MeetingDate"];
    }
    else if(indexPath.row==3)
    {
        Titlelbl.text=@"Target Date";
        Desclbl.text = [detailArray valueForKey:@"TargetDate"];
    }
    else if(indexPath.row==4)
    {
        Titlelbl.text=@"Action Type";
        Desclbl.text = [detailArray valueForKey:@"ActionType"];
    }
    else if(indexPath.row==5)
    {
        Titlelbl.text=@"Action Description";
        Desclbl.text = [detailArray valueForKey:@"ActionDescription"];
    }
    }
    return cell;
}
-(void)loadDataFromApi{
    detailArray=sharedManager.contactArray;
    [refreshControl endRefreshing];
    [_MeetingDetailsTV reloadData];
}
-(void)showProgress{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:_MeetingDetailsTV animated:YES];
    
}

-(void)hideProgress{
    [MBProgressHUD hideHUDForView:_MeetingDetailsTV animated:YES];
}
-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)Backbtn:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
