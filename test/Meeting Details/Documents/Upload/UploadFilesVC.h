//
//  UploadFilesVC.h
//  test
//
//  Created by ceaselez on 18/05/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UploadFilesVC : UIViewController
@property(nonatomic,strong)NSMutableDictionary *filesArray;
@property(nonatomic,strong)NSString *meetingId;
@property(nonatomic,strong)NSString *meetingDate;


@end
