//
//  AlertVC.m
//  test
//
//  Created by ceaselez on 26/06/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "AlertVC.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "MySharedManager.h"
#import "AlertTableViewCell.h"

@interface AlertVC ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerBtn1;
@property (weak, nonatomic) IBOutlet UIButton *headerBtn2;
@property (weak, nonatomic) IBOutlet UIButton *headerBtn3;
@property (weak, nonatomic) IBOutlet UIView *headerBtn1View;
@property (weak, nonatomic) IBOutlet UIView *headerBtn2View;
@property (weak, nonatomic) IBOutlet UIView *headerBtn3View;
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIView *popOverView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSArray *searchResult;
@property (weak, nonatomic) IBOutlet UIView *searchView;

@end

@implementation AlertVC
{
    NSArray *invoiceList;
    UIRefreshControl *refreshControl;
    MySharedManager *sharedManager;
    BOOL searchEnabled;
    NSString *dueString;
    NSInteger selectedIndex;
    NSArray *docsList;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    sharedManager = [MySharedManager sharedManager];
    invoiceList = [[NSArray alloc] init];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [_menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    refreshControl = [[UIRefreshControl alloc]init];
    [self.dataTableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(loadDataFromApi) forControlEvents:UIControlEventValueChanged];
}
-(void) viewWillAppear:(BOOL)animated{
    dueString = @"od";
    _headerLabel.text = @"Delayed Invoice List";
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self searchBarCancelButtonClicked:_searchBar];
    [self loadDataFromApi];
    _popOverView.hidden = YES;
}
-(void)viewDidAppear:(BOOL)animated{
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        sharedManager.slideMenuSlected = @"no";
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (invoiceList.count == 0) {
        return 1;
    }
    if (searchEnabled) {
        if (_searchResult.count == 0) {
            return 1;
        }
        return [self.searchResult count];
    }
    else{
        return invoiceList.count;
    }
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (selectedIndex != 5000) {
        if ( selectedIndex == section) {
            return  docsList.count;
        }
    }
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    AlertTableViewCell *cell;
     
    if (invoiceList.count == 0 || (searchEnabled && _searchResult.count == 0)) {
        UITableViewCell *cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SimpleTableItem"];
        cell1.textLabel.text = @"Data Not Found";
        cell1.textLabel.textAlignment = NSTextAlignmentCenter;
        cell1.backgroundColor = [UIColor clearColor];
        cell1.textLabel.textColor = [UIColor blackColor];
        cell1.userInteractionEnabled = NO;
        return cell1;
    }
    else if (searchEnabled) {
            cell= [tableView dequeueReusableCellWithIdentifier:@"header"];
            cell.clientLabel.text = [[_searchResult objectAtIndex: section] objectForKey:@"Category"];
            cell.companyLabel.text = [[_searchResult objectAtIndex: section] objectForKey:@"CompanyName"];
            cell.yearLabel.text = [[_searchResult objectAtIndex: section] objectForKey:@"FiscalYearName"];
            cell.headerButton.tag = section;
            return cell;
    }
    else{
        cell= [tableView dequeueReusableCellWithIdentifier:@"header"];
        cell.clientLabel.text = [[invoiceList objectAtIndex: section] objectForKey:@"Category"];
        cell.companyLabel.text = [[invoiceList objectAtIndex: section] objectForKey:@"CompanyName"];
        cell.yearLabel.text = [[invoiceList objectAtIndex: section] objectForKey:@"FiscalYearName"];
        cell.headerButton.tag = section;
        return cell;
        
    }
    return cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AlertTableViewCell *cell;
    cell= [tableView dequeueReusableCellWithIdentifier:@"row" forIndexPath:indexPath];
    cell.pdfLabel.text = [[docsList objectAtIndex:indexPath.row] objectForKey:@"InvoiceNo"];
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (invoiceList.count == 0 ||(searchEnabled && _searchResult.count == 0)) {
        return  self.view.bounds.size.height-100;
    }
    return 100;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([Utitlity isConnectedTointernet]){
        [MBProgressHUD showHUDAddedTo:_popOverView animated:YES];
        _popOverView.hidden = NO;
 
              NSString *_pdfUrl = [[docsList objectAtIndex: selectedIndex] objectForKey:@"InvoicePath"];
        NSLog(@"%@",_pdfUrl);
            _pdfUrl = [_pdfUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:_pdfUrl]];
        [_webView loadRequest:request];
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
    [MBProgressHUD hideHUDForView:_popOverView animated:YES];
}
-(void)loadDataFromApi{
    if([Utitlity isConnectedTointernet]){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [self showProgress];
        NSString *targetUrl = [NSString stringWithFormat:@"%@GetAllClientCompaniesList?userid=%@&status=pending&invstatus=%@", BASEURL,[defaults objectForKey:@"UserID"],dueString];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPMethod:@"GET"];
        [request setURL:[NSURL URLWithString:targetUrl]];
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
          ^(NSData * _Nullable data,
            NSURLResponse * _Nullable response,
            NSError * _Nullable error) {
              NSLog(@"targetUrl-----%@",targetUrl);

              invoiceList = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
              NSLog(@"%@",invoiceList);
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self hideProgress];
                  selectedIndex = 5000;
                  [refreshControl endRefreshing];
                  [_dataTableView reloadData];
//                  [_dataTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];

              });
              
          }] resume];
        
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
-(void)showProgress{
    [MBProgressHUD showHUDAddedTo:_dataTableView animated:YES];
    
}

-(void)hideProgress{
    [MBProgressHUD hideHUDForView:_dataTableView animated:YES];
}
-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)search:(id)sender {
    _searchView.hidden = NO;
    [_searchBar  becomeFirstResponder];
}
- (IBAction)searchBackBtn:(id)sender {
    [self searchBarCancelButtonClicked:_searchBar];
}
- (void)filterContentForSearchText:(NSString*)searchText
{
    _searchResult = [invoiceList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(MeetingDescription contains[c] %@) OR (CountryName contains[c] %@) OR (FrequencyDescription contains[c] %@) OR (MeetingDate contains[c] %@)", searchText, searchText, searchText, searchText]];
    [_dataTableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[searchBar.text stringByTrimmingCharactersInSet: set] length] == 0) {
        searchEnabled = NO;
        [_dataTableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[searchBar.text stringByTrimmingCharactersInSet: set] length] == 0) {
        searchEnabled = NO;
        [self.dataTableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    searchEnabled = NO;
    [_dataTableView reloadData];
    _searchView.hidden = YES;
    
}
- (IBAction)dueForNextWeekBtn:(id)sender {
    dueString = @"dfn";
    _headerLabel.text = @"Future Invoice List";
    _headerBtn1View.hidden = YES;
    _headerBtn2View.hidden = YES;
    _headerBtn3View.hidden = NO;
    _popOverView.hidden = YES;
    [self loadDataFromApi];
}
- (IBAction)dueForTodayButton:(id)sender {
    dueString = @"dft";
    _headerLabel.text = @"Today Invoice List";
    _headerBtn1View.hidden = YES;
    _headerBtn2View.hidden = NO;
    _headerBtn3View.hidden = YES;
    _popOverView.hidden = YES;
    [self loadDataFromApi];

}
- (IBAction)overDueButton:(id)sender {
    dueString = @"od";
    _headerLabel.text = @"Delayed Invoice List";
    _headerBtn1View.hidden = NO;
    _headerBtn2View.hidden = YES;
    _headerBtn3View.hidden = YES;
    _popOverView.hidden = YES;
    [self loadDataFromApi];
}

- (IBAction)headerBtn:(id)sender {
    UIButton *clickedButton = (UIButton*)sender;
    NSLog(@"section : %li",(long)clickedButton.tag);
    [self loadDataFromApiForDocs:clickedButton.tag];
}
-(void)loadDataFromApiForDocs:(NSInteger)indexSelected{
    if([Utitlity isConnectedTointernet]){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [self showProgress];
        NSString *targetUrl = [NSString stringWithFormat:@"%@GetAllInvoiceListByUserid?externalid=%@&fiscalyearid=%@&userid=%@&status=%@", BASEURL,[[invoiceList objectAtIndex:indexSelected] objectForKey:@"ExternalStakeholderID"],[[invoiceList objectAtIndex:indexSelected] objectForKey:@"FiscalYearId"],[defaults objectForKey:@"UserID"],dueString];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPMethod:@"GET"];
        [request setURL:[NSURL URLWithString:targetUrl]];
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
          ^(NSData * _Nullable data,
            NSURLResponse * _Nullable response,
            NSError * _Nullable error) {
              NSLog(@"targetUrl-----%@",targetUrl);
              
              docsList = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
              NSLog(@"%@",docsList);
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self hideProgress];
                  selectedIndex = indexSelected;
                  [refreshControl endRefreshing];
                  [_dataTableView reloadData];
                  //                  [_dataTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
                  
              });
              
          }] resume];
        
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)popOverViewOkButton:(id)sender {
    _popOverView.hidden = YES;
}
@end
