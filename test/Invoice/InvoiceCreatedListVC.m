//
//  InvoiceCreatedListVC.m
//  test
//
//  Created by ceaselez on 24/07/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "InvoiceCreatedListVC.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "MySharedManager.h"
#import "SearchViewController.h"
#import "ReceiptTableViewCell.h"
#import "InvoiceDetailVC.h"
@interface InvoiceCreatedListVC ()
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) NSArray *searchResult;
@property (weak, nonatomic) IBOutlet UIView *allBtnView;
@property (weak, nonatomic) IBOutlet UIView *pendingBtnView;
@property (weak, nonatomic) IBOutlet UIView *overDueBtnView;
@property (weak, nonatomic) IBOutlet UIView *paidBtnView;
@property (weak, nonatomic) IBOutlet UIView *closedBtnView;
@property (weak, nonatomic) IBOutlet UIButton *fiscalYearBtn;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@end

@implementation InvoiceCreatedListVC
{
    UIRefreshControl *refreshControl;
    MySharedManager *sharedManager;
    NSArray *receiptList;
    BOOL searchEnabled;
    NSString *fiscalId;
    NSString *statusString;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    sharedManager = [MySharedManager sharedManager];
    receiptList = [[NSArray alloc] init];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [_menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    refreshControl = [[UIRefreshControl alloc]init];
    [self.dataTableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(loadDataFromApi) forControlEvents:UIControlEventValueChanged];
}
-(void) viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        [self searchBarCancelButtonClicked:_searchBar];
        self.searchView.hidden = YES;
        fiscalId = @"NA";
        statusString = @"all";
        _headerLabel.text = @"All Invoice";
        [self buttonClicked:NO pendindView:YES overDueView:YES paidView:YES closedView:YES];
        [_fiscalYearBtn setTitle:@"----Select----" forState:UIControlStateNormal];
    }
    else{
        [self fillData];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        sharedManager.slideMenuSlected = @"no";
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (searchEnabled) {
        if (_searchResult.count == 0) {
            return 1;
        }
        return [self.searchResult count];
    }
    else{
        return receiptList.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReceiptTableViewCell *cell;
    if (receiptList.count == 0 || (searchEnabled && _searchResult.count == 0)) {
        UITableViewCell *cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SimpleTableItem"];
        cell1.textLabel.text = @"Data not Found";
        cell1.textLabel.textAlignment = NSTextAlignmentCenter;
        cell1.backgroundColor = [UIColor clearColor];
        cell1.textLabel.textColor = [UIColor blackColor];
        cell1.userInteractionEnabled = NO;
        return cell1;
    }
    if (searchEnabled) {
            cell= [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            cell.receiptNoLabel.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"InvoiceNo"];
            cell.companyNameLabel.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"CompanyName"];
            cell.totalAmountLabel.text = [Utitlity currencyFormate:[[[_searchResult objectAtIndex: indexPath.row] objectForKey:@"TotalAmount"] intValue]];
            cell.statusLabel.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"ApprovalStatus"];
            cell.receiptDateLabel.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"InvoiceStatus"];
            cell.receiptDateLabel.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"InvoiceDate"];
            return cell;
        }
    else{
        cell= [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.receiptNoLabel.text = [[receiptList objectAtIndex: indexPath.row] objectForKey:@"InvoiceNo"];
        cell.companyNameLabel.text = [[receiptList objectAtIndex: indexPath.row] objectForKey:@"CompanyName"];
        cell.totalAmountLabel.text = [Utitlity currencyFormate:[[[receiptList objectAtIndex: indexPath.row] objectForKey:@"TotalAmount"] intValue]];
        cell.statusLabel.text = [[receiptList objectAtIndex: indexPath.row] objectForKey:@"ApprovalStatus"];
        cell.invoiceStauts.text = [[receiptList objectAtIndex: indexPath.row] objectForKey:@"InvoiceStatus"];
        cell.receiptDateLabel.text = [[receiptList objectAtIndex: indexPath.row] objectForKey:@"InvoiceDate"];
        return cell;
        
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (receiptList.count == 0 || (searchEnabled && _searchResult.count == 0)) {
        return  self.view.bounds.size.height -100 ;
    }
    return 170;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    InvoiceDetailVC *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"InvoiceDetailVC"];
    if (searchEnabled) {
        myNavController.ServiceInvoiceID = [[_searchResult objectAtIndex:indexPath.row] objectForKey:@"ServiceInvoiceID"];
    }
    else{
        myNavController.ServiceInvoiceID = [[receiptList objectAtIndex:indexPath.row] objectForKey:@"ServiceInvoiceID"];
    }
    [self presentViewController:myNavController animated:YES completion:nil];
}
-(void)loadDataFromApi{
    if([Utitlity isConnectedTointernet]){
        [self showProgress];
        NSString *targetUrl = [NSString stringWithFormat:@"%@GetInvoiceListforDetails?externalid=NA&fiscalyearid=%@&userid=%@&status=%@", BASEURL,fiscalId,[[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"],statusString];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPMethod:@"GET"];
        [request setURL:[NSURL URLWithString:targetUrl]];
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
          ^(NSData * _Nullable data,
            NSURLResponse * _Nullable response,
            NSError * _Nullable error) {
              receiptList = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
              NSLog(@"%@",receiptList);
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self hideProgress];
                  if (receiptList.count == 0) {
                      _dataTableView.hidden = YES;
                  }
                  else{
                      _dataTableView.hidden = NO;
                  }
                  [refreshControl endRefreshing];
                  
                  [_dataTableView reloadData];
              });
              
          }] resume];
        
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
-(void)showProgress{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:_dataTableView animated:YES];
    
}

-(void)hideProgress{
    [MBProgressHUD hideHUDForView:_dataTableView animated:YES];
}
-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)search:(id)sender {
    _searchView.hidden = NO;
    [_searchBar  becomeFirstResponder];
}
- (IBAction)searchBackBtn:(id)sender {
    [self searchBarCancelButtonClicked:_searchBar];
}
- (void)filterContentForSearchText:(NSString*)searchText
{
    _searchResult = [receiptList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(CompanyName contains[c] %@) OR (InvoiceNo contains[c] %@)  OR (ApprovalStatus contains[c] %@) OR (InvoiceStatus contains[c] %@) OR (InvoiceDate contains[c] %@)" , searchText, searchText, searchText, searchText, searchText]];
   
    [_dataTableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[searchBar.text stringByTrimmingCharactersInSet: set] length] == 0) {
        searchEnabled = NO;
        [_dataTableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[searchBar.text stringByTrimmingCharactersInSet: set] length] == 0) {
        searchEnabled = NO;
        [self.dataTableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    searchEnabled = NO;
    [_dataTableView reloadData];
    _searchView.hidden = YES;
    
}
- (IBAction)allBtn:(id)sender {
    statusString = @"all";
    _headerLabel.text = @"All Invoice";
    [self buttonClicked:NO pendindView:YES overDueView:YES paidView:YES closedView:YES];
}
- (IBAction)pendingBtn:(id)sender {
    statusString = @"pending";
    _headerLabel.text = @"Pending Invoice";
    [self buttonClicked:YES pendindView:NO overDueView:YES paidView:YES closedView:YES];
}
- (IBAction)overDueBtn:(id)sender {
    statusString = @"overdue";
    _headerLabel.text = @"Overdue Invoice";
    [self buttonClicked:YES pendindView:YES overDueView:NO paidView:YES closedView:YES];
}
- (IBAction)paidBtn:(id)sender {
    statusString = @"paid";
    _headerLabel.text = @"Paid Invoice";
    [self buttonClicked:YES pendindView:YES overDueView:YES paidView:NO closedView:YES];
}
- (IBAction)closedBtn:(id)sender {
    statusString = @"closed";
    _headerLabel.text = @"Closed Invoice";
    [self buttonClicked:YES pendindView:YES overDueView:YES paidView:YES closedView:NO];
}
-(void)buttonClicked:(BOOL)allView pendindView:(BOOL)pendindView overDueView:(BOOL)overDueView paidView:(BOOL)paidView closedView:(BOOL)closedView {
    self.allBtnView.hidden =allView;
    self.pendingBtnView.hidden = pendindView;
    self.overDueBtnView.hidden = overDueView;
    self.paidBtnView.hidden = paidView;
    self.closedBtnView.hidden = closedView;
    [self loadDataFromApi];
}
- (IBAction)ppysicalYearBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        sharedManager.passingMode = @"physicalYear";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
        [self presentViewController:myNavController animated:YES completion:nil];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
-(void)fillData{
    if (sharedManager.passingString.length != 0) {
        
        if ([sharedManager.passingMode isEqualToString: @"physicalYear"]) {
            [_fiscalYearBtn setTitle:sharedManager.passingString forState:UIControlStateNormal];
            fiscalId = sharedManager.passingId;
            [_fiscalYearBtn setTitle:sharedManager.passingString forState:UIControlStateNormal];
            if ([fiscalId isEqualToString: @"00000000-0000-0000-0000-000000000000"]) {
                fiscalId = @"NA";
            }
            [self loadDataFromApi];
        }
    }
}
@end
