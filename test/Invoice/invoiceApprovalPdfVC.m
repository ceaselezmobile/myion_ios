//
//  invoiceApprovalPdfVC.m
//  test
//
//  Created by ceaselez on 19/07/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "invoiceApprovalPdfVC.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "MySharedManager.h"
#import "AlertTableViewCell.h"

@interface invoiceApprovalPdfVC ()
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIView *popOverView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSArray *searchResult;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;
@property (weak, nonatomic) IBOutlet UIButton *pendingStatusBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dataTableViewYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cancelBtnXContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *popoverButtonViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UITextView *commentsTV;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@end

@implementation invoiceApprovalPdfVC
{
    NSArray *invoiceList;
    UIRefreshControl *refreshControl;
    MySharedManager *sharedManager;
    BOOL searchEnabled;
    NSString *statusString;
    NSArray *docsList;
    NSIndexPath *selectedIndex;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    sharedManager = [MySharedManager sharedManager];

    invoiceList = [[NSArray alloc] init];
    statusString=@"all";
    _headerLabel.text  = @"All Invoice List";
    _dataTableViewYConstraint.constant = 0;
    _cancelBtnXContraint.constant = 0;
    _popOverView.hidden = YES;
    [self loadDataFromApiForDocs];
    [self searchBarCancelButtonClicked:_searchBar];

}
- (IBAction)allBtn:(id)sender {
    statusString=@"all";
    _headerLabel.text  = @"All Invoice List";
    _dataTableViewYConstraint.constant = 0;
    [self loadDataFromApiForDocs];
}
- (IBAction)approvedBtn:(id)sender {
    statusString=@"Approved";
    _headerLabel.text  = @"Approved Invoice List";
    _dataTableViewYConstraint.constant = 0;
    [self loadDataFromApiForDocs];
}
- (IBAction)pendingBtn:(id)sender {
    _dataTableViewYConstraint.constant = 80;
    _headerLabel.text  = @"Pending Invoice List";
    statusString=@"od";
    [self loadDataFromApiForDocs];
}
- (IBAction)rejectedBtn:(id)sender {
    _dataTableViewYConstraint.constant = 0;
    _headerLabel.text  = @"Rejected Invoice List";
    statusString=@"Rejected";
    [self loadDataFromApiForDocs];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (docsList.count == 0) {
        return 1;
    }
            return  docsList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AlertTableViewCell *cell;
    if (docsList.count == 0) {
        UITableViewCell *cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SimpleTableItem"];
        cell1.textLabel.text = @"Data Not Found";
        cell1.textLabel.textAlignment = NSTextAlignmentCenter;
        cell1.backgroundColor = [UIColor clearColor];
        cell1.textLabel.textColor = [UIColor blackColor];
        cell1.userInteractionEnabled = NO;
        return cell1;
    }

    cell= [tableView dequeueReusableCellWithIdentifier:@"row" forIndexPath:indexPath];
    cell.invoiceNoLabel.text = [[docsList objectAtIndex:indexPath.row] objectForKey:@"InvoiceNo"];
    cell.totalLabel.text = [Utitlity currencyFormate:[[[docsList objectAtIndex:indexPath.row] objectForKey:@"TotalAmount"] intValue]];
    cell.statusLabel.text = [[docsList objectAtIndex:indexPath.row] objectForKey:@"Status"];

    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (docsList.count == 0 ) {
        return  self.view.bounds.size.height-100;
    }

    return 100;
}
- (IBAction)actionStatusBtn:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Select Action Status :" message:nil preferredStyle:UIAlertControllerStyleAlert];
    // create the actions handled by each button
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Overdue" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        statusString = @"od";
        [_pendingStatusBtn setTitle:@"Overdue" forState:UIControlStateNormal];
        [self loadDataFromApiForDocs];
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Due for today" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        statusString = @"dft";
        [_pendingStatusBtn setTitle:@"Due for today" forState:UIControlStateNormal];
        [self loadDataFromApiForDocs];
    }];
    
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"Due for next Week" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        statusString = @"dfn";
        [_pendingStatusBtn setTitle:@"Due for next Week" forState:UIControlStateNormal];
        [self loadDataFromApiForDocs];
        
    }];
    
    // add actions to our sheet
    [actionSheet addAction:action1];
    [actionSheet addAction:action2];
    [actionSheet addAction:action3];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        // Called when user taps outside
    }]];
    // bring up the action sheet
    [self presentViewController:actionSheet animated:YES completion:nil];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([Utitlity isConnectedTointernet]){
        [MBProgressHUD showHUDAddedTo:_popOverView animated:YES];
        _popOverView.hidden = NO;
        selectedIndex = indexPath;
        NSString *_pdfUrl = [[docsList objectAtIndex: indexPath.row] objectForKey:@"InvoicePath"];
        if ([[[docsList objectAtIndex: indexPath.row] objectForKey:@"Status"] isEqualToString:@"Pending"]) {
            _popoverButtonViewHeightConstraint.constant = 150;
            _cancelBtnXContraint.constant = 220;
            _commentsTV.text = @"";
        }
        else{
            _cancelBtnXContraint.constant = 0;
            _popoverButtonViewHeightConstraint.constant = 40;
        }
        NSLog(@"%@",_pdfUrl);
        _pdfUrl = [_pdfUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:_pdfUrl]];
        [_webView loadRequest:request];
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (void)webViewDidFinishLoad:(UIWebView *)webView;
{
    [MBProgressHUD hideHUDForView:_popOverView animated:YES];
}
-(void)showProgress{
    [MBProgressHUD showHUDAddedTo:_dataTableView animated:YES];
    
}

-(void)hideProgress{
    [MBProgressHUD hideHUDForView:_dataTableView animated:YES];
}
-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)search:(id)sender {
    _searchView.hidden = NO;
    [_searchBar  becomeFirstResponder];
}
- (IBAction)searchBackBtn:(id)sender {
    [self searchBarCancelButtonClicked:_searchBar];
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    searchEnabled = NO;
    [_dataTableView reloadData];
    _searchView.hidden = YES;
    
}
- (void)filterContentForSearchText:(NSString*)searchText
{
    _searchResult = [invoiceList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(InvoiceNo contains[c] %@) OR (Status contains[c] %@)", searchText, searchText ]];
    [_dataTableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[searchBar.text stringByTrimmingCharactersInSet: set] length] == 0) {
        searchEnabled = NO;
        [_dataTableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[searchBar.text stringByTrimmingCharactersInSet: set] length] == 0) {
        searchEnabled = NO;
        [self.dataTableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}
-(void)loadDataFromApiForDocs{
    if([Utitlity isConnectedTointernet]){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [self showProgress];
        NSString *targetUrl = [NSString stringWithFormat:@"%@GetAllInvoiceListByUserid?externalid=%@&fiscalyearid=%@&userid=%@&status=%@", BASEURL,_companyId,_physicalId,[defaults objectForKey:@"UserID"],statusString];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPMethod:@"GET"];
        [request setURL:[NSURL URLWithString:targetUrl]];
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
          ^(NSData * _Nullable data,
            NSURLResponse * _Nullable response,
            NSError * _Nullable error) {
              NSLog(@"targetUrl-----%@",targetUrl);
              
              docsList = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
              NSLog(@"%@",docsList);
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self hideProgress];
                  [refreshControl endRefreshing];
                  [_dataTableView reloadData];
                  //                  [_dataTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
                  
              });
              
          }] resume];
        
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)backBtn:(id)sender {
    sharedManager.slideMenuSlected = @"yes";
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)popOverViewCancelBtn:(id)sender {
    _popOverView.hidden = YES;

}
- (IBAction)approveBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

        NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
        [params setObject:[[docsList objectAtIndex:selectedIndex.row] objectForKey:@"ServiceInvoiceID"]  forKey:@"ServiceInvoiceID"];
        [params setObject: _commentsTV.text  forKey:@"ApproverComments"];
        [params setObject:@"YES"  forKey:@"isApproved"];
        [params setObject:@"NO"  forKey:@"isRejected"];
        [params setObject:[defaults objectForKey:@"UserID"]  forKey:@"ModifiedBy"];

        [self showProgress];
        NSString* JsonString = [Utitlity JSONStringConv: params];
        [[WebServices sharedInstance]apiAuthwithJSON:@"POSTApprovalofInvoice" HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
            
            NSString *error=[json valueForKey:@"error"];
            [self hideProgress];
            
            if(error.length>0){
                [self showMsgAlert:[json valueForKey:@"error_description"]];
                return ;
            }else{
                [self showMsgAlert:@"Invoice Approved successfully."];
                NSLog(@"json-----%@",json);
                _popOverView.hidden = YES;
                [self loadDataFromApiForDocs];
            }
        }];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }

}
- (IBAction)RejectBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
        [params setObject:[[docsList objectAtIndex:selectedIndex.row] objectForKey:@"ServiceInvoiceID"]  forKey:@"ServiceInvoiceID"];
        [params setObject: _commentsTV.text  forKey:@"ApproverComments"];
        [params setObject:@"NO"  forKey:@"isApproved"];
        [params setObject:@"YES"  forKey:@"isRejected"];
        [params setObject:[defaults objectForKey:@"UserID"]  forKey:@"ModifiedBy"];
        
        [self showProgress];
        NSString* JsonString = [Utitlity JSONStringConv: params];
        [[WebServices sharedInstance]apiAuthwithJSON:@"POSTApprovalofInvoice" HTTPmethod:@"POST" forparameters:JsonString ContentType:APICONTENTTYPE apiKey:nil onCompletion:^(NSDictionary *json, NSURLResponse * headerResponse) {
            
            NSString *error=[json valueForKey:@"error"];
            [self hideProgress];
            
            if(error.length>0){
                [self showMsgAlert:[json valueForKey:@"error_description"]];
                return ;
            }else{
                [self showMsgAlert:@"Invoice Rejected successfully."];
                NSLog(@"json-----%@",json);
                _popOverView.hidden = YES;
                [self loadDataFromApiForDocs];
            }
        }];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }

}

@end
