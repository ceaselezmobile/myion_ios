//
//  PaymentRecieptListVC.m
//  test
//
//  Created by ceaselez on 24/07/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "PaymentRecieptListVC.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "MySharedManager.h"
#import "SearchViewController.h"
#import "ReceiptTableViewCell.h"
#import "PaymentReceiptVC.h"
@interface PaymentRecieptListVC ()
@property (weak, nonatomic) IBOutlet UITableView *dataTableView;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) NSArray *searchResult;
@property (weak, nonatomic) IBOutlet UIButton *categoryBtn;
@property (weak, nonatomic) IBOutlet UIButton *companyBtn;
@end

@implementation PaymentRecieptListVC
{
    UIRefreshControl *refreshControl;
    MySharedManager *sharedManager;
    NSArray *receiptList;
    BOOL searchEnabled;
    NSString *companyID;
    NSString *categoryId;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    sharedManager = [MySharedManager sharedManager];
    receiptList = [[NSArray alloc] init];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [_menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    refreshControl = [[UIRefreshControl alloc]init];
    [self.dataTableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(loadDataFromApi) forControlEvents:UIControlEventValueChanged];
}
-(void) viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self searchBarCancelButtonClicked:_searchBar];
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
//        categoryId = @"NA";
        companyID = @"NA";
        [_categoryBtn setTitle:@"----select-----" forState:UIControlStateNormal];
        [_companyBtn setTitle:@"----select-----" forState:UIControlStateNormal];
    }
    else{
        [self fillData];
    }

    [self loadDataFromApi];
}
-(void)viewDidAppear:(BOOL)animated{
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        sharedManager.slideMenuSlected = @"no";
    }
}
- (IBAction)categoryBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        [_companyBtn setTitle:@"----select-----" forState:UIControlStateNormal];
        companyID = @"";
        
        sharedManager.passingMode = @"categoryContact";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
        [self presentViewController:myNavController animated:YES completion:nil];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)companyBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        if ([categoryId isEqualToString:@""]) {
            [self showMsgAlert:@"Please select Category"];
        }
        else{
            sharedManager.passingMode = @"companyContact";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            myNavController.categoryID = categoryId;
            sharedManager.passingId = @"00000000-0000-0000-0000-000000000000";
            [self presentViewController:myNavController animated:YES completion:nil];
        }
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (searchEnabled) {
        if (_searchResult.count == 0) {
            return 1;
        }
        return [self.searchResult count];
    }
    else{
        return receiptList.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReceiptTableViewCell *cell;
        if (receiptList.count == 0 || (searchEnabled && _searchResult.count == 0)) {
            UITableViewCell *cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SimpleTableItem"];
            cell1.textLabel.text = @"Data not Found";
            cell1.textLabel.textAlignment = NSTextAlignmentCenter;
            cell1.backgroundColor = [UIColor clearColor];
            cell1.textLabel.textColor = [UIColor blackColor];
            cell1.userInteractionEnabled = NO;
            return cell1;
        }
    else if (searchEnabled) {
    
            cell= [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
            cell.receiptNoLabel.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"ReceiptNo"];
            cell.companyNameLabel.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"CompanyName"];
            cell.totalAmountLabel.text = [Utitlity currencyFormate:[[[_searchResult objectAtIndex: indexPath.row] objectForKey:@"ReceivedAmount"] intValue]];
            cell.statusLabel.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"Status"];
            cell.receiptDateLabel.text = [[_searchResult objectAtIndex: indexPath.row] objectForKey:@"ReceiptDate"];
            return cell;
        }
    else{
        cell= [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.receiptNoLabel.text = [[receiptList objectAtIndex: indexPath.row] objectForKey:@"ReceiptNo"];
        cell.companyNameLabel.text = [[receiptList objectAtIndex: indexPath.row] objectForKey:@"CompanyName"];
        cell.totalAmountLabel.text = [Utitlity currencyFormate:[[[receiptList objectAtIndex: indexPath.row] objectForKey:@"ReceivedAmount"] intValue]];
        cell.statusLabel.text = [[receiptList objectAtIndex: indexPath.row] objectForKey:@"Status"];
        cell.receiptDateLabel.text = [[receiptList objectAtIndex: indexPath.row] objectForKey:@"ReceiptDate"];
        return cell;
        
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (receiptList.count == 0 ||(searchEnabled && _searchResult.count == 0)) {
        return  self.view.bounds.size.height-100;
    }
    return 150;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PaymentReceiptVC *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"PaymentReceiptVC"];
    if (searchEnabled) {
        myNavController.receiptId = [[_searchResult objectAtIndex:indexPath.row] objectForKey:@"ReceiptID"];
    }
    else{
        myNavController.receiptId = [[receiptList objectAtIndex:indexPath.row] objectForKey:@"ReceiptID"];
    }
    [self presentViewController:myNavController animated:YES completion:nil];
}
-(void)loadDataFromApi{
    if([Utitlity isConnectedTointernet]){
        [self showProgress];
        NSString *targetUrl = [NSString stringWithFormat:@"%@GetPaymentReceiptList?externalid=%@", BASEURL,companyID];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setHTTPMethod:@"GET"];
        [request setURL:[NSURL URLWithString:targetUrl]];
        
        [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
          ^(NSData * _Nullable data,
            NSURLResponse * _Nullable response,
            NSError * _Nullable error) {
              receiptList = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
              NSLog(@"%@",receiptList);
              
              dispatch_async(dispatch_get_main_queue(), ^{
                  [self hideProgress];
                  if (receiptList.count == 0) {
                      _dataTableView.hidden = YES;
                  }
                  else{
                      _dataTableView.hidden = NO;
                  }
                  [refreshControl endRefreshing];
                  
                  [_dataTableView reloadData];
              });
              
          }] resume];
        
        
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
-(void)showProgress{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:_dataTableView animated:YES];
    
}

-(void)hideProgress{
    [MBProgressHUD hideHUDForView:_dataTableView animated:YES];
}
-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)search:(id)sender {
    _searchView.hidden = NO;
    [_searchBar  becomeFirstResponder];
}
- (IBAction)searchBackBtn:(id)sender {
    [self searchBarCancelButtonClicked:_searchBar];
}
- (void)filterContentForSearchText:(NSString*)searchText
{
    _searchResult = [receiptList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(CompanyName contains[c] %@) OR (ReceiptDate contains[c] %@) OR (ReceiptNo contains[c] %@) OR (Status contains[c] %@)", searchText, searchText, searchText, searchText]];
    [_dataTableView reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[searchBar.text stringByTrimmingCharactersInSet: set] length] == 0) {
        searchEnabled = NO;
        [_dataTableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[searchBar.text stringByTrimmingCharactersInSet: set] length] == 0) {
        searchEnabled = NO;
        [self.dataTableView reloadData];
    }
    else {
        searchEnabled = YES;
        [self filterContentForSearchText:searchBar.text];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    searchEnabled = NO;
    [_dataTableView reloadData];
    _searchView.hidden = YES;
    
}
-(void)fillData{
    if (sharedManager.passingString.length != 0) {
        if ([sharedManager.passingMode isEqualToString: @"categoryContact"]) {
            [_categoryBtn setTitle:sharedManager.passingString forState:UIControlStateNormal];
            categoryId = sharedManager.passingId;
            
//            if ([companyID isEqualToString: @"00000000-0000-0000-0000-000000000000"]) {
//                companyID = @"NA";
//            }
        }
        if ([sharedManager.passingMode isEqualToString: @"companyContact"]) {
            [_companyBtn setTitle:sharedManager.passingString forState:UIControlStateNormal];
            companyID = sharedManager.passingId;
            if ([companyID isEqualToString: @"00000000-0000-0000-0000-000000000000"]) {
                companyID = @"NA";
            }
        }
    }
}
@end
