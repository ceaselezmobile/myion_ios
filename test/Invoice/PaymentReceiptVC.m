//
//  PaymentReceiptVC.m
//  test
//
//  Created by ceaselez on 24/07/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import "PaymentReceiptVC.h"
#import "MyMeetingTableViewCell.h"
#import "MySharedManager.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "ReceiptTableViewCell.h"

@interface PaymentReceiptVC ()
@property (weak, nonatomic) IBOutlet UITableView *ReceiptTableView;
@property (weak, nonatomic) IBOutlet UITableView *InvoiceTableView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;

@end

@implementation PaymentReceiptVC
{
    NSMutableArray *dataArray;
    long tableIndex;
    int textFieldNO;
    long lenght;
    int range;
    NSArray *headerArray;
    NSArray *contentArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    dataArray = [[NSMutableArray alloc] init];
    headerArray = [NSArray arrayWithObjects:@"Category :",@"Company :",@"Receipt No :",@"Received Date :",@"Total Amount :",@"Payment Mode :",@"Remarks :", nil];
    lenght = 0;
    range = 0;
    _scrollView.hidden = YES;
    _headerLabel.text  = @"Payment Receipt";
}
-(void)viewWillAppear:(BOOL)animated{
    [self viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self apiCall];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ( tableView == _ReceiptTableView) {
        return 7;
    }
    else{
        if (dataArray.count == 0) {
            return  0;
        }
        if ([[[dataArray objectAtIndex:0] objectForKey:@"lstreceipt"] count] == 0) {
            return 1;
        }
        return [[[dataArray objectAtIndex:0] objectForKey:@"lstreceipt"] count];
    }
   
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == _ReceiptTableView) {
        static NSString *simpleTableIdentifier = @"cell";
        ReceiptTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        cell.detailTopicLabel.text = [headerArray objectAtIndex:indexPath.row];
        cell.detailContentTF.text = [contentArray objectAtIndex:indexPath.row];
        return  cell;
    }
    else{
        if ([[[dataArray objectAtIndex:0] objectForKey:@"lstreceipt"] count] == 0) {
            UITableViewCell *cell1 = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SimpleTableItem"];
            cell1.textLabel.text = @"Receipts are not found";
            cell1.textLabel .textAlignment = NSTextAlignmentCenter;
            cell1.backgroundColor = [UIColor clearColor];
            cell1.textLabel.textColor = [UIColor blackColor];
            cell1.userInteractionEnabled  = NO;
            return cell1;
        }
        else{
    static NSString *simpleTableIdentifier = @"dataCell";
    MyMeetingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.data1.text = [NSString stringWithFormat:@"%@",[[[[dataArray objectAtIndex:0] objectForKey:@"lstreceipt"] objectAtIndex:indexPath.row] objectForKey:@"InvoiceNo"]];
    cell.data2.text = [NSString stringWithFormat:@"%@",[[[[dataArray objectAtIndex:0] objectForKey:@"lstreceipt"] objectAtIndex:indexPath.row] objectForKey:@"InvoiceDate"]];
    cell.data3.text = [Utitlity currencyFormate:[[[[[dataArray objectAtIndex:0] objectForKey:@"lstreceipt"] objectAtIndex:indexPath.row] objectForKey:@"InvoiceTotalAmount"] intValue]];
    cell.data4.text = [Utitlity currencyFormate:[[[[[dataArray objectAtIndex:0] objectForKey:@"lstreceipt"] objectAtIndex:indexPath.row] objectForKey:@"ReceivedAmount"] intValue]];
    cell.data5.text = [Utitlity currencyFormate:[[[[[dataArray objectAtIndex:0] objectForKey:@"lstreceipt"] objectAtIndex:indexPath.row] objectForKey:@"BalanceAmount"] intValue]];
    cell.data6.text = [Utitlity currencyFormate:[[[[[dataArray objectAtIndex:0] objectForKey:@"lstreceipt"] objectAtIndex:indexPath.row] objectForKey:@"CurrentTotalAmount"] intValue]];
        return  cell;
        }
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    static NSString *simpleTableIdentifier = @"headerCell";
    MyMeetingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.header1.text = @"Invoice No";
    cell.header2.text = @"Invoice Date";
    cell.header3.text = @"Invoive Amount";
    cell.header4.text = @"Received Amount";
    cell.header5.text = @"Balance Amount";
    cell.header6.text = @"Current Amount";
    return  cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (dataArray.count != 0 && tableView == _InvoiceTableView) {
        return 44;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _ReceiptTableView) {
        return 80;
    }
    if ([[[dataArray objectAtIndex:0] objectForKey:@"lstreceipt"] count] == 0) {
        return  self.view.bounds.size.height - 100;
    }
    return 44;
}
-(void)apiCall{
    if([Utitlity isConnectedTointernet]){
        
        [self showProgress];
        NSMutableURLRequest *urlRequest;        
        urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@GetPaymentReceiptDetails?receiptid=%@",BASEURL,_receiptId]]];
        
        //create the Method "GET"
        [urlRequest setHTTPMethod:@"GET"];
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                          {
                                              NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                              if(httpResponse.statusCode == 200)
                                              {
                                                  NSError *parseError = nil;
                                                  dataArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                                                  NSLog(@"urlRequest-----%@",dataArray);

                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [self hideProgress];
                                                     
                                                      contentArray = [NSArray arrayWithObjects:[[dataArray objectAtIndex:0] objectForKey:@"Category"],[[dataArray objectAtIndex:0] objectForKey:@"Company"],[[dataArray objectAtIndex:0] objectForKey:@"ReceiptNo"],[[dataArray objectAtIndex:0] objectForKey:@"ReceivedDate"],[Utitlity currencyFormate:[[[dataArray objectAtIndex:0] objectForKey:@"TotalAmount"] intValue]],[[dataArray objectAtIndex:0] objectForKey:@"PaymentMode"],[[dataArray objectAtIndex:0] objectForKey:@"Remarks"], nil];
                                                      [_ReceiptTableView reloadData];
                                                      [_InvoiceTableView reloadData];
                                                  });
                                              }
                                              else
                                              {
                                                  NSLog(@"Error");
                                              }
                                          }];
        [dataTask resume];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
    
}
-(void)showProgress{
    [self hideProgress];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}

-(void)hideProgress{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}
- (IBAction)receiptButton:(id)sender {
    _headerLabel.text  = @"Payment Receipt";
    _scrollView.hidden = YES;
}
- (IBAction)invoiceButton:(id)sender {
    _headerLabel.text  = @"Invoice List";
    _scrollView.hidden = NO;
}
- (IBAction)backBtn:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
