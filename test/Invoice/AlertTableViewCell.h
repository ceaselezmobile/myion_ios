//
//  AlertTableViewCell.h
//  test
//
//  Created by ceaselez on 26/06/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *clientLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyLabel;
@property (weak, nonatomic) IBOutlet UILabel *pdfLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UIButton *headerButton;
@property (weak, nonatomic) IBOutlet UILabel *invoiceNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *receivedamountLabel;
@property (weak, nonatomic) IBOutlet UILabel *invoiceStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *invoiceDateLabel;

@property (weak, nonatomic) IBOutlet UILabel *CloseInvoiceNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *closeInvoiceDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *closeDueDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *closeGSTINLabel;
@property (weak, nonatomic) IBOutlet UILabel *closeTaxableAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *closeTotalTaxLabel;
@property (weak, nonatomic) IBOutlet UILabel *closeTotalAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *CloseCloseDate;
@property (weak, nonatomic) IBOutlet UILabel *closeRamarkLabel;
@property (weak, nonatomic) IBOutlet UITextView *closeCommentsTV;
@property (weak, nonatomic) IBOutlet UILabel *closeInvoiceAmount;
@property (weak, nonatomic) IBOutlet UILabel *closeReceivedAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *closeBalanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *closeCompany;
@property (weak, nonatomic) IBOutlet UILabel *colseInvoiceNumber;
@property (weak, nonatomic) IBOutlet UILabel *closeInoviceTotalAmount;
@property (weak, nonatomic) IBOutlet UILabel *closeReceiptNo;
@property (weak, nonatomic) IBOutlet UILabel *closeReceiptDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *closeTDSAmountLabeel;
@property (weak, nonatomic) IBOutlet UILabel *receiptTaxaleAmount;
@property (weak, nonatomic) IBOutlet UILabel *receiptTotalTaxLabel;
@property (weak, nonatomic) IBOutlet UILabel *receiptTotalAmount;
@end
