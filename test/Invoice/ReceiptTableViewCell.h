//
//  ReceiptTableViewCell.h
//  test
//
//  Created by ceaselez on 24/07/18.
//  Copyright © 2018 ceaselez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReceiptTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *receiptNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *receiptDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *invoiceStauts;

@property (weak, nonatomic) IBOutlet UILabel *detailTopicLabel;
@property (weak, nonatomic) IBOutlet UITextField *detailContentTF;
@property (weak, nonatomic) IBOutlet UIButton *statusBtn;
@end
