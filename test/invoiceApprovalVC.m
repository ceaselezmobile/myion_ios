//
//  invoiceApprovalVC.m
//  
//
//  Created by ceaselez on 25/06/18.
//

#import "invoiceApprovalVC.h"
#import "Utitlity.h"
#import "Reachability.h"
#import "WebServices.h"
#import "GlobalURL.h"
#import "MBProgressHUD.h"
#import "SWRevealViewController.h"
#import "MySharedManager.h"
#import "SearchViewController.h"
#import "invoiceApprovalPdfVC.h"


@interface invoiceApprovalVC ()
@property (weak, nonatomic) IBOutlet UIButton *categoryBtn;
@property (weak, nonatomic) IBOutlet UIButton *companyBtn;
@property (weak, nonatomic) IBOutlet UIButton *physicalYearBtn;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;

@end

@implementation invoiceApprovalVC
{
    MySharedManager *sharedManager;
    NSString *companyID;
    NSString *categoryId;
    NSString *physicalYearId;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    SWRevealViewController *revealViewController = self.revealViewController;
    sharedManager = [MySharedManager sharedManager];
    if ( revealViewController )
    {
        [_menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}
-(void) viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        categoryId = @"";
        companyID = @"";
        physicalYearId = @"";
        [_categoryBtn setTitle:@"----select-----" forState:UIControlStateNormal];
        [_companyBtn setTitle:@"----select-----" forState:UIControlStateNormal];
        [_physicalYearBtn setTitle:@"----select-----" forState:UIControlStateNormal];
    }
    else{
        [self fillData];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    if ([sharedManager.slideMenuSlected isEqualToString:@"yes"]) {
        sharedManager.slideMenuSlected = @"no";
    }
}
- (IBAction)categoryBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        [_companyBtn setTitle:@"----select-----" forState:UIControlStateNormal];
        [_physicalYearBtn setTitle:@"----select-----" forState:UIControlStateNormal];
        physicalYearId = @"";
        companyID = @"";

        sharedManager.passingMode = @"categoryContact";
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
        [self presentViewController:myNavController animated:YES completion:nil];
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)companyBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        if ([categoryId isEqualToString:@""]) {
            [self showMsgAlert:@"Please select Category"];
        }
        else{
            physicalYearId = @"";
            [_physicalYearBtn setTitle:@"----select-----" forState:UIControlStateNormal];
            sharedManager.passingMode = @"companyContact";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            myNavController.categoryID = categoryId;
            sharedManager.passingId = @"00000000-0000-0000-0000-000000000000";
            [self presentViewController:myNavController animated:YES completion:nil];
        }
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)ppysicalYearBtn:(id)sender {
    if([Utitlity isConnectedTointernet]){
        if ([companyID isEqualToString:@""]) {
            [self showMsgAlert:@"Please select Category"];
        }
        else{
            sharedManager.passingMode = @"physicalYear";
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SearchViewController *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
            [self presentViewController:myNavController animated:YES completion:nil];
        }
    }else{
        [self showMsgAlert:NOInternetMessage];
    }
}
- (IBAction)SubmitBtn:(id)sender {
    if([categoryId isEqualToString:@""] && [companyID isEqualToString:@""] && [physicalYearId isEqualToString:@""]){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        invoiceApprovalPdfVC *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"invoiceApprovalPdfVC"];
        myNavController.companyId = @"NA";
        myNavController.physicalId = @"NA";
        [self presentViewController:myNavController animated:YES completion:nil];
    }
    else if([categoryId isEqualToString:@""] && ![companyID isEqualToString:@""] && ![physicalYearId isEqualToString:@""]){
        [self showMsgAlert:@"Please select all the fields"];
    }
    else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        invoiceApprovalPdfVC *myNavController = [storyboard instantiateViewControllerWithIdentifier:@"invoiceApprovalPdfVC"];
        myNavController.companyId = companyID;
        myNavController.physicalId = physicalYearId;
        [self presentViewController:myNavController animated:YES completion:nil];
    }
}

-(void)showMsgAlert:(NSString *)msg{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:nil
                                                                  message:msg
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:ok];
    
    
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)fillData{
    if (sharedManager.passingString.length != 0) {
        if ([sharedManager.passingMode isEqualToString: @"categoryContact"]) {
            [_categoryBtn setTitle:sharedManager.passingString forState:UIControlStateNormal];
            categoryId = sharedManager.passingId;
        }
        if ([sharedManager.passingMode isEqualToString: @"companyContact"]) {
            [_companyBtn setTitle:sharedManager.passingString forState:UIControlStateNormal];
            companyID = sharedManager.passingId;
            if ([companyID isEqualToString: @"00000000-0000-0000-0000-000000000000"]) {
                companyID = @"NA";
            }
        }
        if ([sharedManager.passingMode isEqualToString: @"physicalYear"]) {
            [_physicalYearBtn setTitle:sharedManager.passingString forState:UIControlStateNormal];
            physicalYearId = sharedManager.passingId;
            if ([physicalYearId isEqualToString: @"00000000-0000-0000-0000-000000000000"]) {
                physicalYearId = @"NA";
            }

        }
  }
}
@end
